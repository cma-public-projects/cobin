#!/usr/bin/env python
from networkcontagion import postlib

if __name__ == "__main__":
    postlib.processing.run_postprocessing(
        columns=postlib.metrics.columns,
        timeseries=postlib.metrics.timeseries,
        plots=postlib.metrics.plots,
        doc=__doc__,
    )

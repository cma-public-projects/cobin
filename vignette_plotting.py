#! /usr/bin/env python

import polars as pl
import pandas as pd
import numpy
import sys
from matplotlib import pyplot as plt

from networkcontagion.postlib import utils 
from networkcontagion.postlib.processing import import_file


vaccination_npz = sys.argv[3]
vax_dict_data, _ = import_file(vaccination_npz)

# ------------ Plotting outputs by different demographic factors ------------------ #
demographic_npz = sys.argv[1]
demographic_dict_data, _ = import_file(demographic_npz)
indiv_data = pl.read_csv('/your/path/to/networkcontagion/examples_networks/IH_nodelist_0_doses.csv')

# make dataframes and rename variables
demo_df = pl.DataFrame(demographic_dict_data)
demo_df = demo_df.rename({'enode':'node'}).join(indiv_data,on='node',how='left')

# get infection events and add descriptive columns
infect_events = demo_df.filter(
    pl.col("efrom").is_in(utils.safe_keys) & ~pl.col("eto").is_in(utils.safe_keys)
).with_columns([
    pl.col('t').cast(pl.Int16).alias('day'),
    pl.lit("normal").alias('beta'),
    pl.lit("0 doses").alias('vax')
])

# timeseries by ethnicity
ts_demo = infect_events.groupby(['ethnicity','day'], maintain_order=True).count().groupby('ethnicity')

# total infections by ethnicity
totals = infect_events.groupby('ethnicity', maintain_order=True).count()

# plotting timeseries for each ethnicity and saving
fig,ax = plt.subplots(figsize=(12, 4), dpi=80)
for t in ts_demo:
    eth = t[0]
    ax.plot(t[1]['day'], t[1]['count'], label=eth)
plt.legend()
plt.savefig("/your/path/to/tutorial_sim/outputs/demographic_ts.png", dpi='figure', pad_inches=0.5,  bbox_inches='tight')

fig,ax = plt.subplots(figsize=(12, 4), dpi=80)
ax.bar(totals['ethnicity'], totals['count'])
plt.savefig("/your/path/to/tutorial_sim/outputs/demographic_bar.png", dpi='figure', pad_inches=0.5,  bbox_inches='tight')


# --------------- Investigating the effects of doubling beta --------------------- #
double_beta_npz = sys.argv[2]
double_beta_dict_data, _ = import_file(double_beta_npz)
# make dataframes and rename variables
double_df = pl.DataFrame(double_beta_dict_data)
double_df = double_df.rename({'enode':'node'})

# get infection events and add descriptive columns
infect_events_double = double_df.filter(
    pl.col("efrom").is_in(utils.safe_keys) & ~pl.col("eto").is_in(utils.safe_keys)
).with_columns([
    pl.col('t').cast(pl.Int16).alias('day'), 
    pl.lit("double").alias('beta')
])

# turn into timeseries by day
ts_normal = infect_events.groupby(['day'], maintain_order=True).count()
ts_double = infect_events_double.groupby(['day'], maintain_order=True).count()

# plotting each timeseries and saving
fig,ax = plt.subplots(figsize=(12, 4), dpi=80)
ax.plot(ts_normal['day'], ts_normal['count'], label='Normal')
ax.plot(ts_double['day'], ts_double['count'], label='Double')
plt.legend()
plt.savefig("/your/path/to/tutorial_sim/outputs/double_beta/beta_ts.png", dpi='figure', pad_inches=0.5,  bbox_inches='tight')


# ----- Investigating the effects of adding vaccination and waning immunity ----- #

# make dataframes and rename variables
vax_df = pl.DataFrame(vax_dict_data)
vax_df = vax_df.rename({'enode':'node'})

# get infection events and add descriptive columns
infect_events_vax = vax_df.filter(
    pl.col("efrom").is_in(utils.safe_keys) & ~pl.col("eto").is_in(utils.safe_keys)
).with_columns([
    pl.col('t').cast(pl.Int16).alias('day'), 
    pl.lit("vax_and_waning").alias('vax')
])

# turn into timeseries by day
ts_vax = infect_events_vax.groupby('day', maintain_order=True).count()

# plotting each timeseries and saving
fig,ax = plt.subplots(figsize=(12, 4), dpi=80)
ax.plot(ts_normal['day'], ts_normal['count'], label='0 Doses, No Waning')
ax.plot(ts_vax['day'], ts_vax['count'], label='Multiple Doses, Waning')
plt.legend()
plt.savefig("/your/path/to/tutorial_sim/outputs/vaccination/vax_ts.png", dpi='figure', pad_inches=0.5,  bbox_inches='tight')
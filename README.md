# Network Contagion
`networkcontagion` is a `Python` library for simulating spreading processes on bipartite networks, with dynamics defined by reactions between individual agents on the network. It uses the contagion on a bipartite interaction network (`cobin`) model employed by COVID-19 Modelling Aotearoa for predicting and modelling the spread of COVID-19 in New Zealand.

## Documentation
HTML documentation on how to install and run the package, as well as how the model works can be found in the directory `docs/build/html` the landing page is `index.html` [here](docs/build/html/index.html)

Details on development and building of the documentation is [here](docs/README.md)

## Root directory files
### Run files
- run_simulation.py: A script to construct and run a `networkcontagion` cobin simulation. The `networkcontagion` documentation tutorial has step by step instructions for how to do this.
- postprocessing_cobin.py: A script to run the `networkcontagion` postprocessing on a cobin simulation output (`.npz` files). The `networkcontagion` documentation tutorial has step by step instructions for how to do this.
- make_experiments.py: A script to build a set of config.yaml files to grid over different specifed simulation parameters.
- vignette_plotting: An example script that demonstrates how the structure of the simulation results `.npz` files allow for fine-grained aggregation of certain statistics (such as infections by context, age, ethnicity, etc). Requires Pola.rs library. The `networkcontagion` documentation tutorial has step by step instructions for how to use this script.

### Dependency files
- setup.cfg
- environment.yaml
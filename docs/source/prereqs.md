# Prerequisites

`networkcontagion` is written in Python. If you are comfortable with Python and the management of your Python environment, you may proceed to the {doc}`install`. If not, we will describe here some methods of managing your Python environment.

## Python Environments

Python exists in many versions at the same time. Although only a small number of these versions are officially supported by the core Python team, the *packages* built on top of Python often support or require older versions of Python. Often, the required versions of both Python and other packages required by the packages we are interested in using can conflict with each other between different projects. The reasons for these versioning conflicts and differences are widely varied, but as end-users, we must be aware of potential versioning conflicts and deal with these before they become critical problems. For \*nix systems, like Linux and MacOS, this is critical as there is an instance of Python present in order to run the system, and messing with the 'system Python' can cause problems with the operating system itself.

The standard solution is to separate the environments -- the Python interpreter/version and packages -- for each project. There are a number of ways to manage these, ranging from the in-built `virtualenv` to more-than-just-Python management systems like `conda`.

## The 'easy' way: Conda

Arguably the easiest way to manage environments in Python is by using Conda. Conda manages packages, dependencies and environments for multiple inter-dependent languages, including Python.
For scientific applications, especially if you intend to use numerically-heavy libraries, conda can also manage underlying C(++) and Fortran dependencies.

```{important}
Conda is the recommended way to manage Python for scientific uses, since it can also manage non-Python dependencies (like underlying Fortran or C code). It is also well-supported, has a large community, and is relatively easy to use correctly.
```

### Flavours of conda

Conda is available in at least two 'flavours': Anaconda and miniconda.

[Anaconda](https://www.anaconda.com/) is a fully-fledged ecosystem, which comes with a base set of packages for scientific applications. It also installs utilities like Jupyter (for interactive notebook sessions) and Spyder (a graphical MatLab-like IDE for Python), as well a GUI interface for environment management, Anaconda Navigator.

[miniconda](https://docs.conda.io/en/latest/miniconda.html) is a minimal utility that provides conda. It is a stripped-down version of the Anaconda distribution, with only conda, Python, and their dependencies. For space-constrained systems, this could be a good choice, but requires more command-line interaction.

### Environments in conda

Both 'flavours' of conda provide a base environment. It can be tempting to work directly on this environment, but this can be perilous for the same reasons as not managing environments in the first place. This base environment is used by conda itself, and is assumed by the program to be 'stable'. Messing with the versions of packages in the base environment could cause package conflicts for us later, if we need to update packages for other projects. It can also cause confusion wherein executables/packages can be accessed from other projects that should not have access.

#### Creating New Environments

We can create a new conda environment with the name `contagion` and install the required packages like so:

```bash
conda create --name contagion python=3.10 networkx numpy>=1.18 scipy pyyaml matplotlib
```

**N.B. ZSH Shell Users:** The `zsh` shell tries to treat `>=` as a command, and so you should instead run:

```zsh
conda create --name contagion python=3.10 networkx 'numpy>=1.18' scipy pyyaml matplotlib
```

Here we have:

- Created a conda environment with the name `contagion`
- Installed the Python 3.10 interpreter into that environment
- Installed the networkx library into that environment
- Installed the numpy library that is at least version 1.18 into that environment

To successfully check the install of your new conda environment run:

```bash
conda activate contagion
python --version
pip list
```

You should see the `(base)` flag on the lefthand side of your terminal line change to `(contagion)`. The terminal should also have a printed line with `Python 3.10.9` (or another version more up to date than this).
In the final lines you should see a list of all the packages that are installed in the contagion environment, the ones that are required to run the CNM (although these will also be installed if missing during the `networkcontagion` install) are:
- numpy
- scipy
- networkx
- pyyaml
- matplotlib

#### Working on and customising environments (extra information only if you did not follow the above steps)

To work on an environment (for example, `contagion`), we invoke the command:

```bash
conda activate contagion
```

To stop working on an environment, we invoke the command:

```bash
conda deactivate
```

Note that you can activate one environment from inside another, which will swap you completely over to that activated environment (no nesting of environments).

To add packages to an environment, we can invoke:

```bash
conda install <package_name>
```

For example

```bash
conda install ipython scipy
```

Following the [installation instructions](install.md) for the networkcontagion package will also make sure that all the package dependencies are installed at the same time. 

For further information, seek out the conda documentation, or use the help functionality:

```bash
conda help

conda install --help
```

[Back to installation](install.md)

## The standard library way: venv

`venv` is a way of constructing virtual environments that comes with the standard library (the default Python installation).

You can use venv to create virtual environments with no additional setup. The tool is well documented in the [Python docs](https://docs.python.org/3/library/venv.html)

If you want to progress this way, ensure that you are consistent with where you are creating virtual environments, and make sure to reuse as much as possible, since each new invocation of venv will create a brand new virtual environment with its own set of packages.

[Back to installation](install.md)

## Managing Python versions: pyenv

If we are not using conda, we will need to ensure that the Python version we want is available on our system. Managing multiple Python interpreters manually is a hassle. Luckily, `pyenv` provides a utility for doing this.

If you're on MacOS, you're in luck. `pyenv` is provided by [Homebrew](https://brew.sh).
```bash
brew update
brew install pyenv
```

Other *nix users will have to clone the [GitHub repository](https://github.com/pyenv/pyenv) and insert the following into their `~/.bashrc` (Ubuntu/Fedora), `~/.bash_profile` (other distros) or equivalent (e.g. for zsh users) to modify the system path.
```bash
export PYENV_ROOT="$HOME/.pyenv"
export PATH="$PYENV_ROOT:$PATH"
```

<hr />

To proceed with installation, further modify the `~/.bash_profile`-equivalent file to include the following to enable shims and autocompletion:
```bash
if command -v pyenv 1>/dev/null 2>&1; then
  eval "$(pyenv init -)"
fi
```

Then, reload the shell with `source ~/.bash_profile` or equivalent to set the path and initialise `pyenv`.

For installation, the `pyenv` wiki writers have also obscured away the dependency install step: follow [these instructions](https://github.com/pyenv/pyenv/wiki#suggested-build-environment) for your platform **before proceeding**.

Then, we can finally install our version of Python. For this example, we will use `3.11.2`, the latest stable release at time of writing
```bash
pyenv install 3.11.2
pyenv rehash
```

To "apply" a `pyenv` Python version, you can set the entire shell to use the specified version:
```bash
pyenv shell 3.11.2
```
which you can handily leave in your `~/.bashrc`, after the initialisation snippet above. This means you don't have to think about my Python version until you need to upgrade for some reason.

### `virtualenv`: Managing libraries
To manage libraries, and conflicting library requirements from different applications (i.e. non-maintained, old applications), I use `virtualenv`. This allows the separation of different sets of Python libraries that can be switched between at will.

For example, I have a default virtual environment that my terminal is hard set to (via my `~/.bashrc`), which contains the standard scientific libraries, and then I have a particular virtual environment for doing work related to teaching, which contains libraries specific to the teaching material. Keeping the two environments separate ensures I know exactly which libraries students require to run their lab exercises, for example.

#### Installing `virtualenv`
There are many ways to install and use `virtualenv`. I prefer to use the [`pyenv-virtualenvwrapper` plugin](https://github.com/pyenv/pyenv-virtualenvwrapper) for `pyenv`. The installation is relatively simple, running the following in the terminal:

```bash
git clone https://github.com/pyenv/pyenv-virtualenvwrapper.git $(pyenv root)/plugins/pyenv-virtualenvwrapper
cd "$(pyenv root)/plugins/pyenv-virtualenvwrapper"
git pull
```

Then add to your `~/.bash_profile` or equivalent, under the `pyenv shell X.X.X` or equivalent command:
```bash
pyenv virtualenvwrapper
export PYENV_VIRTUALENVWRAPPER_PREFER_PYENV="true"
```

and apply the changes
```bash
source ~/.bash_profile
```

#### Creating and using virtual environments
Basic creation:
```bash
mkvirtualenv MY_VENV_NAME_HERE
```

Activation:
```bash
workon MY_VENV_NAME_HERE
```

Destruction:
```bash
rmvirtualenv MY_VENV_NAME_HERE
```

#### Managing libraries
Use `pip` to manage your Python libraries.

A useful snippet to auto-upgrade all libraries in an environment
```bash
pip list --outdated --format=freeze | grep -v '^\-e' | cut -d = -f 1  | xargs -n1 pip install -U
```

#### *nix Snippet

A `.bashrc` snippet for *nix users:
```bash
export PYENV_ROOT="$HOME/.pyenv"
export PATH="$PYENV_ROOT/bin:$PATH:"
if command -v pyenv 1>/dev/null 2>&1; then
  eval "$(pyenv init -)"
fi
pyenv shell __PYTHON_VERSION_HERE__
pyenv virtualenvwrapper
export PYENV_VIRTUALENVWRAPPER_PREFER_PYENV="true"
workon __DEFAULT_VIRTUAL_ENV_HERE__ 
```
Remember to replace the double-underscore variables!

[Back to installation](install.md)

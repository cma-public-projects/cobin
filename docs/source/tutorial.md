# networkcontagion Tutorial
## Overview
The [first tutorial section](#basic-tutorial) of this page intends to briefly guide the user through the process of setting up and running simulations on the network contagion model. It also demonstrates how the user can take the output from the model and produce two simple time-series plots (of daily infections and hospitalisations) and some summary metrics (the time to detection of the first case and the total number of ICU visits).

[Following sections](#tutorial-extensions) in this page extend the tutorial and show how to use other capabilities of the `networkcontagion` library, and how these affect simulation results.

```{note}
This tutorial follows the process to run the `cobin` simulation locally, with inputs appropriate to local use. 
However longer, more complex, and batch runs of simulations, on larger networks, may require more memory and processing than usually available locally. For running simulations on large scale and complex networks the COVID-19 Modelling Aotearoa team uses a High Performance Supercomputer.
```

## Brief overview of the required input files
The standard basic files to configure and run a `cobin` simulation are:
- `config.yaml`
  This contains the configuration settings for this simulation run. This yaml specifies the inputs to the `cobin` simulation code. It specifies which network object to run on; initial conditions for seeding infections; how many days to run the simulation for; disease and behaviour parameters; when contagion parameter-change events occur and the parameters associated with them; and so on. More detailed documentation of `config.yaml` parameters can be found in the [sample values page](sample_values_refs.md) and we also [provide explanations for all parameters](input_parameters_dict.md).
  
- `bipartite_network.full.gpickle`
  This is the bipartite network object that the contagion simulation process runs on.
  We have included some example networks in this package. For more information on network objects used by this package see [Networks](inputs.md#bipartite-network-object)

## Basic tutorial
This tutorial runs through the basic steps for running a `cobin` simulation using `networkcontagion`.

```{caution}
Make sure that you have first completed the steps in [Prerequisites](prereqs.md) and [Installation](install.md) before starting this tutorial.
```

### 1. Set up
#### 1.1. Create a directory for your simulation
Create a directory locally somewhere you would like to store the inputs and outputs of this simulation.
For the purposes of this tutorial we will name this directory `tutorial_sim`.
Within this directory create two subdirectories, `inputs` and `outputs`.
Your folder structure should now look like this:
```
.
|__tutorial_sim
|____inputs
|____outputs

```
This is the structure that we will be using for this tutorial, however it is not a requirement of the `networkcontagion` library that you follow this structure. When setting up your own simulations you can structure your directories as you wish.

#### 1.2. Select a network object
As mentioned in the [Overview](#brief-overview-of-the-required-input-files) section, we need a network object to run the `cobin` model code on. We have provided a selection of example network objects in the `network_contagion` package in the `/example_networks` folder.

In this tutorial we will be using the example network provided at `/example_networks/no_doses_TA50_network.gpickle`

#### 1.3. Set up a config.yaml
Copy the `vignette_config_0_doses.yaml` from `/configs` into your `tutorial_sim/inputs` directory.
This yaml specifies input parameters to the cobin model.

We now need to edit the tutorial `yaml` to ensure that our sims can be run using it as an input. Open the `yaml` for editing in your preferred editor.

This file is broken into 7 sections; 
- INITIALISATION, 
- CONTAGION PARAMETERS, 
- VACCINATION, 
- TESTING,
- TRACING/ISOLATING, 
- CLOSE CONTACTS, and 
- INTERVENTIONS.

The majority of `vignette_config_0_doses.yaml` is already set up as required to run a simulation on a small network, but we need to make some small changes. 

In the INITIALISATION section change the line

```
  graph: "/absolute/path/to/network.gpickle"
```
to

```
  graph: /path/to/cobin/example_networks/no_doses_TA50_network.gpickle
```
where `/path/to/cobin/` is an absolute path to where you have installed the `networkcontagion` library.

Next change

```
  output_dir: "/absolute/path/to/output_directory"
```

to

```
  output_dir: "/your/local/path/tutorial_sim/outputs"
```
where `/your/local/path/tutorial_sim/` is an absolute path to the directory you created in step 1.1. 

In the input `yaml` the `output_dir` parameter determines where the output from the simulations run using that input `yaml` will be stored. 

We will also change the number of days we want to simulate. Again in the INITIALISATION 
section, change the line

```
  tmax: 30
```

to

```
  tmax: 60
```

Our simulations will now simulate 60 days instead of 30. 

Next, scroll down the yaml to the section entitled VACCINATION.
Note the line
```
   p_asympt_list: !include '/cobin/configs/parameters_by_TA/p_asympt.csv'
```
It is important to check that any `!include` paths like these in the `yaml` point to the intended files. 
There are a number of `!include` statements in this tutorial `yaml` file, all reference sample infection probability files that have been included in the `networkcontagion` library. 

For your simulation to work in this tutorial, you will need to update these `!include` references.
Change all `!include` references in your copy of `vignette_config_0_doses.yaml` from

```
...: !include '/cobin/configs/...
```
To
```
...: !include '/path/to/cobin/configs/...
```
where `/path/to/cobin/` is an absolute path to where you have installed the `networkcontagion` library.
 
For more information on the vaccination files included in this tutorial `yaml`, see [Sample input files](sample_values_refs.md)
 
For more information about how to use `!include` and other tags in the `config.yaml` see [The Config Loader](config_loader.md)

To ensure reproducibility, and to help with file versioning, it is worth making a copy of any files (such as `yamls` and `csvs`) that you are pointing to within the experiment `yaml` in the simulation experiment directory (and then adjusting the `!include` file paths accordingly). This ensures that a copy of the input files used in an experiment always sits in that experiment directory and so can allow for having different versions of the input files for different contagions/COVID variants without worrying about overwriting the input files used by other experiments. This also makes it easier to determine exactly what files were used with what experiment when calibrating or reporting on simulations. However, this ease-of-access adjustment will need to be balanced with the storage cost of saving potentially duplicate files.

### 2. Run simulation
We can now set our set of simulations running. To do this, from terminal run

```
python  /your/path/to/cobin/run_simulation.py /your/local/path/to/tutorial_sim/inputs/vignette_config_0_doses.yaml --verbose
```

The `--verbose` argument to `run_simulation.py` means some useful information will be printed to terminal, including:
- the random seed that was used for each simulation run (if you haven't specified the seed in your input `yaml`, which we didn't for this tutorial).
- high-level updates on the progress of the code as it is run.
- how long parts of the code took to run.

Each simulation job should take less than a minute or two to complete, depending on your computer specs.

### 2.1 Check that your simulations have produced some output
Once our simulation has finished running, check that it has produced some output. There should be one `npz` file in your `tutorial_sim/outputs` directory (or wherever you specified for the `output_dir` parameter in your input `yaml`.)

For more information on the type and structure of the output files from running `cobin` simulations, see [here](outputs.md).


### 3. Postprocess outputs
The `networkcontagion` package includes a number of scripts for processing the `npz` outputs of simulations in the `postlib` submodule. These are documented [here](postlib.md).

We will use the `postlib` to produce a simple csv output of two daily timeseries (one for new infections and one for new hospitalisations) and then another csv output containing two numerical metrics (time till the detection of the first case and the total number of patients that had to visit the ICU).

#### 3.1 Check the desired metrics are selected

Open `networkcontagion/postlib/metrics.py` and make sure that at least the following lines are uncommented:

```python
""" Metrics library for post-processing """

from functools import partial
from . import behaviour, tracing, detection, plot

# Comment/uncomment desired timeseries to be returned
# Dictionary of timeseries metrics to return. Key:value == Timseries name:function

timeseries = {
  # Cases
  'New Infections': behaviour.byday_new_infections,
  #...,
  'New Hospitalisations': behaviour.byday_new_hosp,
  #...
}

#...

columns = {
  'Time to Detection': detection.time_of,
  # ...
  'Total Critical Hospitalisations': behaviour.total_hospitalisations,
  # ...
}
# ...
```

An explanation of `metrics.py` and the other statements found within is located [here](postlib.md)

We can also control the set of output metrics, timeseries, and plots we want postprocessing to produce by passing them as args to the postprocessing function. But it's more readable to do this way - you just need to remember to check that the ones you want are uncommented every time you're going to run postprocessing.

#### 3.2 Run postprocessing
Once you have the timeseries and plots you want, we can execute the postprocessing run script, 
From terminal run `postprocessing_cobin.py` with the following arguments. Further information for the different arguments you can pass to this function can be found [here](postlib.md).

```
python  /your/path/to/cobin/postprocessing_cobin.py \
/your/local/path/to/tutorial_sim/outputs/ --csv \
-o /your/local/path/to/tutorial_sim/outputs/output_metrics.csv \
-t /your/local/path/to/tutorial_sim/outputs/time_series.csv \
-p -d /your/local/path/to/tutorial_sim/outputs/
-n pdf
```

Once this has finished running your `outputs` directory should be filled with:
- `output_metrics.csv`
- `time_series.csv`
- one pdf per time series specified in `metrics.py`

Looking at the timeseries we desired, the `pdfs` produced for `plot_with_quantile_New Infections.pdf` and `plot_with_quantile_New Hospitalisations.pdf` should look something like:

:::{figure} img/new_inf_single.*
New Infections
:::

:::{figure} img/new_hosp_single.*
New Hospitalisations
:::

And in the `output_metrics.csv` you should see a column corresponding to time to detection (something similar to 4.70) and a column for total hospitalisations (something similar to 600).

We note that for the timeseries plots that `postlib` is also trying to overlay values for quartiles and the median. Due to stochasticity, simulating the same config input to `cobin` will produce different outputs (unless the same random seed is used). 

#### 3.3 Running 
Let's see the difference in both simulation and plotting output that happens if we were to rerun `run_simulation.py` multiple times for the same config yaml. First, rerun the model four more times using the same terminal code as mentioned previously (wait for each job to finish and then run the code again and again):

```
python  /your/path/to/cobin/run_simulation.py /your/local/path/to/tutorial_sim/inputs/vignette_config_0_doses.yaml --verbose
```

We note that it is also possible to set-up a shell script to sequentially run the simulations, so that you do not have to manually set the model running multiple times. An example script is provided called `run_seq_sims.sh`

Then, after making sure that there are now five `npz` files in your equivalent directory to `tutorial_sim/outputs`, rerun the `postlib` call:

```
python  /your/path/to/cobin/postprocessing_cobin.py \
/your/local/path/to/tutorial_sim/outputs/ --csv \
-o /your/local/path/to/tutorial_sim/outputs/output_metrics.csv \
-t /your/local/path/to/tutorial_sim/outputs/time_series.csv \
-p -d /your/local/path/to/tutorial_sim/outputs/
```

Looking at the `pdfs` this time, you should see multiple simulations plotted and the central tendency measures overlayed like this:
:::{figure} img/new_inf_multi.*
New Infections
:::

:::{figure} img/new_hosp_multi.*
New Hospitalisations
:::

And in the `output_metrics.csv` you should see multiple rows (each corresponding to a different `npz`).

## Tutorial Extensions
As part of the tutorial, we also include some 'vignette-style' examples of what else is possible to model using the parameters available in the configuration yamls and also how the structure of the `npz` files allows for fine-grained aggregation of certain statistics (such as infections by context, age, ethnicity, etc.).
We note that any postprocessing of the `npz` files in this section makes use of the [Pola.rs](https://www.pola.rs/) library which should be installed in your Python environment before running the included `vignette_plotting.py` file.

NB: You will have to edit `vignette_plotting.py` to correct the paths to your desired file outputs as directed in the vignette/file. The file takes three arguments, and is designed to only work if all three are given (i.e. all three vignettes are to be plotted), though please edit this if you wish: 
1. Path to an `npz` run using the tutorial
2. Path to an `npz` run using the `vignette_config_double_beta.yaml`
3. Path to an `npz` run using the `vignette_config_vaccination.yaml`

Then, to run the vignette plotting, run the following (edited) command:

```
python vignette_plotting.py /your/path/to/tutorial_sim/outputs/file.npz /your/path/to/tutorial_sim/outputs/double_beta/file_npz /your/path/to/tutorial_sim/outputs/vaccination/file_npz
```

We also note that the parameters changed in the vignettes and summarised results are not indicative of any modelling efforts conducted by the `networkcontagion` team and are just meant as a showcase of the effects of some commonly change parameters and of some commonly undertaken analyses.

### Adding additional fine-grained aggregation
We begin this section by looking at how the `npz` files and network information can be combined to provide demographic-dependent statistics. The `example_networks/IH_nodelist_0_doses.csv` contains demographic information for every individual node within the simulation run. By joining this data onto the IDs of either the 
enode or the anode, we can gain information as to the demographics of the infected/infector individuals within the simulation. 

The section of `vignette_plotting.py` labelled **Plotting outputs by different demographic factors** contains code that plots the ethnicity of infected individuals over time. To run this section of the code, edit the paths of the outputted plots to your desired directory (found in lines 46 and 50). The first argument when you run the file should be your 

The resultant timeseries and bar chart produced should look like similar to (where we can see the breakdown in infections by ethnicity):

:::{figure} img/demographic_ts.*
Timeseries of New Infections
:::

:::{figure} img/demographic_bar.*
Bar chart of Total Infections
:::

### Effect of doubling beta
To display the nonlinear effect that changing even a single parameter in the model has on observed outputs, we also show the effects of doubling `beta` for all demographics and contexts. The config with this doubled `beta` can be found in `configs/vignette_config_double_beta.yaml`. The paths within the `yaml` should be updated like in Step 1.3 of the tutorial and you should also copy this yaml to `/your/local/path/to/tutorial_sim/inputs/vignette_config_double_beta.yaml`, we also suggest that you make a new directory for the outputs of this config (e.g. at `tutorial_sim/outputs/double_beta/`). We can then run the experiment by running the following code from the terminal:

```
python  /your/path/to/cobin/run_simulation.py /your/local/path/to/tutorial_sim/inputs/vignette_config_double_beta.yaml --verbose
```

Running the section of `vignette_plotting.py` labelled **Investigating the effects of doubling beta** you should see a timeseries that shows that doubling `beta` increases the peak of the infection curve and causes it to occur much earlier:

:::{figure} img/beta_ts.*
New Infections
:::

### Effect of adding in vaccination and waning immunity
To show the effects of vaccination and waning immunity within the model we also include an additional configuration yaml: `configs/vignette_config_vaccination.yaml`. The paths within the `yaml` should again be updated like in Step 1.3 of the tutorial (**NB: the network path should instead be to `/path/to/cobin/example_networks/TA50_network.gpickle`**). We also suggest that you make a new directory for the outputs of this config (e.g. at `tutorial_sim/outputs/vaccination/`). We can then run the experiment by running the following code from the terminal:

```
python  /your/path/to/cobin/run_simulation.py /your/local/path/to/tutorial_sim/inputs/vignette_config_vaccination.yaml --verbose
```

The transmission-reduction effects of vaccination should be noticeable in your timeseries. If we had run on a larger network, or for a longer timeperiod, we would have also seen the results of reinfection due to waning immunity (the infections curve would show more than one peak):

:::{figure} img/vax_ts.*
New Infections
:::

### Effect of reseeding
To show the effects of having scheduled reseeding events to the simulation we also include an additional configuration yaml: `configs/vignette_config_reseed.yaml`. The paths within the `yaml` should again be updated like in Step 1.3 of the tutorial. We also suggest that you make a new directory for the outputs of this config (e.g. at `tutorial_sim/outputs/reseed/`). We can then run the experiment by running the following code from the terminal:

```
python  /your/path/to/cobin/run_simulation.py /your/local/path/to/tutorial_sim/inputs/vignette_config_vaccination.yaml --verbose
```

The added peaks in the infection curve as a result of the reseeding should be noticeable in your timeseries. We can also look at the timeseries for Cumulative and New Reseeds to check that we are indeed getting around 40 reseeds every 5 days.

:::{figure} img/reseed_infections.*
New Infections
:::

:::{figure} img/reseed_nreseeds.*
New Reseeds
:::

:::{figure} img/reseed_creseeds.*
Cumulative Reseeds
:::
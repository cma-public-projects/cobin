networkcontagion package
========================

.. automodule:: networkcontagion
   :members:
   :undoc-members:
   :show-inheritance:

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   networkcontagion.lib
   networkcontagion.postlib

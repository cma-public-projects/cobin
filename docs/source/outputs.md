# Cobin Model Outputs
Each run of the `cobin` code outputs an `npz` file. This is a zipped archive of numpy arrays (`npy` files), with each array corresponding to a column in the `networkcontagion.lib.simulation.simulation.return_states` list.
These include the time of each event (infection, lockdown, tracing, etc.), the enode: _primary node_ (node ID of individual associated with the event, e.g. from changing Susceptible to Exposed), the anode: _auxiliary nodes_ (node ID of another individual associated with the event, e.g. Infectious individual that infected the enode), the group (node ID of group that caused the change state, e.g. a dwelling node if an infection occurred in a dwelling), efrom and eto (state that the enode transitions from and to, e.g. SU and EU) and finally, the rest of the columns are the counts of individuals in that particular
state at time t (i.e. SU, EU, AU, PU, ..., SQ, EQ, AQ, PQ...).

These npz's can be read in and converted to different formats for visualisation and analysis, such as a pandas dataframe, dictionary, list of lists and more. An example of a few rows of a pandas dataframe displaying the information stored in the sim outputs is shown below:

|   t   |   enode  |   anode  |  group  | efrom | eto  | SU  | EU  | ... |
|-------|----------|----------|---------|-------|------|-----|-----| --- |
| 1.234 | 'IH1234' | 'IH5678' | 'DW123' | 'SU'  | 'EU' | 100 | 200 | ... |
| 1.345 | 'IH5678' |          |         | 'EU'  | 'PU' | 100 | 199 | ... |

## Output usage
These outputs are designed to be used with the `postlib` submodule to produce basic output metrics that you would expect from the average epidemics compartment model (counts within each compartment over time) as well as some overall simulation numbers (such as total isolations).

For a more indepth look at how simulation outputs can be plotted and analysed see the latter sections of the [Tutorial](tutorial.md).

# Postlib Submodule Outputs
The `postlib` submodule is designed to produce basic output metrics for simulation bugfixing, calibration and reporting. For more information on the submodule, and how to run it, see [here](postlib.md).

There are two main outputs from this submodule, both `csv` files. One is named `<name>_time_series_<i>.csv`, where the experiment name (`<name>`) and number (`<i>`) are given by the user. Every row in the csv corresponds to a different timeseries for a certain run/npz. The columns (excluding the first two) are then the values for the timeseries on different days. Example timeseries could be new infections, active infections and new hospitalisations. An example of this file is shown below:

| run |        metric        |  0  |  1  |  2  |  3  |   4  | ... |
|-----|----------------------|-----|-----|-----|-----|------| --- |
|  1  |    New Infections    | 100 | 250 | 500 | 700 | 1500 | ... |
|  1  | New Hospitalisations |  0  | 50  | 25  | 68  | 17   | ... |
| ... | .................... | ... | ... | ... | ... | .... | ... |
|  2  |    New Infections    | 101 | 350 | 495 | 765 | 1630 | ... |
|  2  | New Hospitalisations |  2  | 350 | 495 | 765 | 1630 | ... |

The other main output from this submodule is named `<name>_output_metrics_<i>.csv`, where the values are also given by the user. Every row in this csv instead corresponds to a certain run/npz. The columns (excluding the first one) are then the values for different 'metrics'. An example of this file is shown below:

| run | Day of Peak Individuals Isolated in a Day | Peak Number of Individuals Isolated in a Day | Time to Detection | Total Infections | ... |
| --- | ----------------------------------------- | -------------------------------------------- | ----------------- | ---------------- | --- |
|  1  |                  6                        |                      3671                    |      1.186347     |      90396       | ... |
|  2  |                  6                        |                      3652                    |      0.985234     |      90405       | ... |

These csv's are then able to be read in and plotted in a software package of choice. The submodule does include some basic plotting functionality too, allowing for auto-plotting of the timeseries contained in the csv as well (see [here](tutorial.md) for example plots).
# Input files

The `cobin` model requires two inputs: 
1. a bipartite network, and 
2. a simulation config.yaml file.

## Bipartite network object
`cobin` takes a network.gpickle object to run the contagion process on. This package includes some example network files to run the code on. These have been created by the COVID-19 Modelling Aotearoa team.

We intend to publish the code that generated these networks in the future, but for the purposes of publishing the `networkcontagion` library we have included some sample networks in the `example_networks` directory.

For more example networks see these stored networks [on Figshare](https://doi.org/10.6084/m9.figshare.23557506.v1).

Each bipartite network object contains two types of nodes: one representing the individuals of Aotearoa, and the other representing interaction contexts/group nodes that individuals interact and possible infect others in. The individuals are connected to the group nodes via edges in the network, and each node can have different attributes (e.g. ethnicity, vaccination dates, age, sex, etc. for individuals and lockdown status, etc. for group nodes). The bipartite networks are stored in `gpickle` files, which are relabelled `pickle` files, and can be read-in using the `pickle` library in Python and converted to a `networkx.Graph` object (assuming the `networkx` library is also loaded). These graphs then have the attributes of the nodes, and other values important to the contagion simulation that can be accessed as dictionaries. For more information on the `networkx.Graph` objects, and other information related to functions in `networkx` that we may take advantage of, see [here](https://networkx.org/documentation/stable/index.html).

## Simulation config settings
A simulation's configuration is specified in a .yaml file which is then passed to the `cobin` run script.
This file must contain parameters to specify:
- Simulation initialisation and run parameters
- Networkcontagion process/modelling parameters
  - Disease trajectory parameters
  - Transmission parameters
  - Vaccination and reinfection parameters
  - TTIQ parameters
  - Behaviour modification parameters

{doc}`input_parameters_dict` provides an exhaustive list of the possible parameters that can be used in a config.yaml. In practice we do not usually use all of these options (some parameters are also mutually exclusive).

{doc}`config_loader` contains info about the mechanics of how the config file can be constructed, and some directives/functions that can be used within it

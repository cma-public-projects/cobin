# Flow diagrams of events in the `cobin` model

```{contents}
```

Having outlined the different components within the model, it is worth highlighting that the main difficulties with a complex model like this is how to efficiently and accurately model the rates and changes of each jump process. Here, we describe the actions taken to realise particular events in the `cobin` model.

Employing the Gillespie Max Method (outlined [here](algorithm.md)), we simulate all events at a maximal rate, and then employ rejection based sampling to decide whether an event actually occurs. The probability of an event occurring depends on things like immunity from vaccinations, NPI and other policies implemented. The rejection sampling can also be used to adjust for non-Markovian processes such as a set isolation period for cases.

We will endeavour to explain some of the parameters/values that are used/sampled as they appear in the event descriptions, but note that $\tau$ is used to indicate a sampled time (e.g. $\tau_\text{test}$ is the time till an individual seeks a test) and that $\tau \sim \text{distribution}$ indicates that the time is drawn from the given distribution. 

For clarity, we also note that that "∗X" indicates the lockdown state of an individual (either Q, C, D, W, T or U) and that "X∗" indicates the disease state of the individual (S, E, A, P, I, R, D or H). Bracket notation such as "$[SEA]$∗" or "∗$[CQ]$" is used to indicate that the step applies to any individual within that selection of states (i.e. for the given examples, that the disease state can be in any of S, E, A and that the quarantine state is either C or Q, respectively). See the [model explanation](modelling_individuals_and_processes.md) for more information on the different states.

## Spontaneous Event
A spontaneous event is an event where only one individual changes state and does not impact any other individual.
An example would be an exposed individual becoming pre-sympotomatic (`EU-->PU`)
:::{figure} img/flow_spontaneous.*
Spontaneous Event
:::



## Removal Event
A removal event is where an individual is removed from the population (e.g. through recovery or death).
An example is when a previously infected individual has died (`CQ-->DX`)
:::{figure} img/flow_removal.*
Removal Event
:::



## Infection Event
An infection event is where a susceptible individual has beome exposed to an infectious individual.
This could lead to an individual(s) becoming infected (`SU-->EU`) or the infection attempt may be rejected.
:::{figure} img/flow_infection.*
Infection Event
:::



## Test Seeking Event
A test seeking event corresponds to an individual seeking a test (either randomly or due to tracing policy).
This could lead to an infectious individual then waiting for a test (e.g. `IU-->IW`) and for contacts to then become distanced (e.g. `EU-->ED`) or leading to no change in status. We note that the time to test return ($\tau$) is drawn from an exponential distribution with rate parameter $\theta_\text{lab}$.
:::{figure} img/flow_test_seek.*
Test Seeking Event
:::



## Test Return Event
A test return event corresponds to a waiting individual receiving their test back.
If individuals have already received a result, or already have known disease state then their status is not changed. Else they will transition into a known state (e.g. `IW-->IC`).
:::{figure} img/flow_test_return.*
Test Return Event
:::



## Confirmation Event
A test confirmation event corresponds to an individual confirming/reaffirming their disease state.
This event causes the lockdown triggers to be checked and contacts traced/tested as set by policy.
We note that the subscripts $w_i,\tau_i$ indicate the weight and trace delay for each individual $i$.
:::{figure} img/flow_confirm.*
Confirmation Event
:::



## Lockdown Trigger Check Event
A lockdown trigger check event corresponds to checking whether trigger for each lockdown has been switched on, and whether the triggers need to be reactivated.
:::{figure} img/flow_lockdown_trigger.*
Lockdown Trigger Check Event
:::



## Contact Tracing Event
A contact tracing event corresponds to attempting to trace a contact of a confirmed case.
If individuals are unsuccessfully traced, then another trace event is scheduled. Otherwise, contacts may seek a test or enter quarantine (e.g. `EU-->EQ`).
We note that $\tau$ is to indicate the time delay for either testing, quarantine exit or tracing and that $\tau$ is drawn from an exponential distribution with rate parameters $\theta_\text{q}, \lambda_\text{Q}$ and $\lambda_\text{trace}$ respectively.
:::{figure} img/flow_trace.*
Contact Tracing Event
:::



## Isolation Escape Event
An isolation escape event corresponds to either a distanced individual leaving quarantine or a quarantined individual escaping quarantine (e.g. `SD-->SU`).
:::{figure} img/flow_escape.*
Isolation Escape Event
:::



## Lockdown Event
A lockdown event corresponds to checking whether a certain lockdown level will be turned on (and its trigger may or may not be reactivated too). 
:::{figure} img/flow_lockdown.*
Lockdown Event
:::



## Initial Case Seeding Event
An initial case seeding event corresponds to selecting the initial/reseeding individuals in the simulation to set as exposed.
If an individual is selected, they become exposed (e.g. `SU-->EU`).
:::{figure} img/flow_seeding.*
Initial Case Seeding Event
:::

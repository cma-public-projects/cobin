# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Fixing pathing to look for the module in the correct relative directory ------
import os
import sys
sys.path.insert(0, os.path.abspath(os.path.join('..','..')))

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'networkcontagion'
copyright = '2023, COVID-19 Modelling Aotearoa Team'
author = 'COVID-19 Modelling Aotearoa Team'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    'myst_parser',
    'sphinx.ext.napoleon',
    'sphinx.ext.mathjax',
    'sphinx.ext.todo',
    'sphinxcontrib.mermaid',
    'sphinxcontrib.pseudocode',
    "sphinx_design",
    # 'sphinxcontrib.plantuml',
]

templates_path = ['_templates']
exclude_patterns = []

autodoc_member_order = 'groupwise'

default_role = 'any'

myst_enable_extensions = [
    'colon_fence',
    'dollarmath',
]
myst_heading_anchors = 2

numfig = True

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'sphinx_rtd_theme'
html_static_path = ['_static']
html_sidebars = { '**': [
    'globaltoc.html', 
    'relations.html', 
    'sourcelink.html', 
    'searchbox.html',
    ]}

html_css_files = [
    "css/widetable.css"
]
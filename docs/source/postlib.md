# Postlib
The postlib library is a separate submodule within `networkcontagion` that is designed to work on the outputted `npz` files from the main `networkcontagion.lib` run-library. It outputs timeseries counting the number of jump events and number in state for certain counts important for reporting (e.g. number of new infections, number of individuals isolated, etc.). It also outputs a selection of column/numerical metrics that can be used for both calibration and reporting (e.g. total number of infections, peak number of daily isolations, etc.). Finally, it also allows for the plotting of the timeseries metrics overlayed with pointwise centrality measures.

## Inputs
The postlib library takes in a dictionary of `npz` files relating to a certain experiment configuration (or multiple configurations in the same directory) and processes them into important timeseries and simulation metrics that can be used for reporting and calibrating the simulation results. 

The main (control flow) input to `postlib` is the `metrics.py` file. It contains three dictionaries that dictate the outputs produced by the library. Each dictionary contains `key: value` tuples, where the key is the name of the timeseries or metric to output and the value is the associated function by which to calculate the timeseries or metric. Looking at the timeseries included in `postlib` they are:

- New Infections: the daily count of new infections
- New Reseeds: the daily count of newly seeded individuals
- New Confirmed Cases: the daily count of newly confirmed cases
- New Hospitalisations: the daily count of newly hospitalised individuals
- New Critical: the daily count of new visits to the ICU
- New Deaths: the daily count of new fatalities
- Newly Contact Traced: the daily count of newly contact traced individuals
- Active Infections: the daily count of individuals who have the contagion (and can spread it)
- Active Confirmed Cases: the daily count of individuals who are confirmed
- Active Critical: the daily count of individuals who are in ICU
- Cumulative Infections: the cumulative count of daily new infections
- Cumulative Reseeds: the cumulative count of daily new reseeds
- Cumulative Confirmed Cases: the cumulative count of daily new confirmed cases
- Cumulative Hospitalisations: the cumulative count of daily new hospitalisations
- Cumulative Critical: the cumulative count of daily new ICU patients
- Cumulative Deaths: the cumulative count of daily new fatalities

There is an option to disaggregate the count of daily new infections by infection group (by dwelling, school, workplace, etc.) which is the line `timeseries.update(...)` found below the timeseries dictionary.

Looking at the numerical metrics included:

- Time at End of Simulation: the time of the final event at the end of the simulation
- Time to Detection: the time that the first case was confirmed
- Cumulative Infections at Detection: the total number of individuals infected by the detection of the first case
- Active Infections at Detection: the number of infected individuals at the detection of the first case
- Total Infections: the total number of individuals infected during the simulation
- Total Deaths: the total number of fatalities during the simulation
- Total Hospitalisations: the total number of individuals hospitalised during the simulation
- Total Critical Hospitalisations: the total number of patients that visited the ICU during the simulation
- Peak Number of Individuals Isolated in a Day: the maximum number of individuals in isolation during one day
- Day of Peak Individuals Isolated in a Day: the day that the above metric occurred on
- Total Number of Individuals Isolated: the total number of individuals in the simulation that were in isolation
- Mean Time to Trace Contacts: the average time to trace a contact of a confirmed case

## Postlib outputs
The main outputs of the library are two `csv` files. One contains the count of certain statistics (such as new infections, active infections, new hospitalisations, etc.) over time, and the other contains calibration and reporting metrics. See the section on [Outputs](outputs.md) for more information.

The library also allows for the plotting of the timeseries within the csv. By default these are saved as `pdf` files.

## Library Structure
The main files within the library are `metrics.py` and `processing.py`. `metrics.py` contains three dictionaries corresponding to the different possible outputs of the `postlib` library (one for timeseries, one for metrics and one for plots). Within the dictionaries, desired/undesired outputs can be uncommented/uncommented so that they appear/do not appear in the outputted csv files (or plots). If extra timeseries, metrics and plots are desired, first add their definitions to the files listed below and then add a line in a similar `key: function` tuple to the existing metrics in the corresponding dictionary. `processing.py` contains the functions that actually run through the `npz` directory and read-in and process the files. 

The `behaviour.py`, `tracing.py` and `detection.py` files contain the function definitions used to get different outputs. `behaviour.py` contains the function definitions for different timeseries, and is where extra timeseries metrics should be defined as desired. Each function (for all three files) takes in the same three arguments: `data`: a `dict` that contains the `npz` files in dictionary form, `df`: a `pandas.DataFrame` that contains the data from the `npz` files as a dataframe and `outputs`: is a `dict` of the outputs for each `npz` file. `tracing.py` contains functions defining metrics related to contact tracing statistics (number of trace events per day, number of peak isolations, etc.). `detection.py` contains functions that define metrics about the number of infections at the time that the first confirmed case is detected.

The `plot.py` file contains the function definitions for plotting the outputs of the postlib library. Currently, we only provide the capability to plot the timeseries outputs, with the values for all `npz` files in the input directory plotted, as well as the IQR and median taken pointwise from the whole directory. Further plotting functions may be added here and then to the `plots` dictionary in `metrics.py` as desired.

The other files (`meta.py`, `parse_yaml.py`, `record_table.py`, `utils.py`) contain utility and I/O functions used throughout the rest of the library. 

## Running the Code
The main file that runs all the postprocessing scripts is `postprocessing_cobin.py`, which takes in the directory of `npz` files to process. The full list of arguments that can be passed to the function is available by running:

```bash
python postprocessing_cobin.py -h
```

Note that some flags should be followed by the corresponding argument, e.g. -t "path_to_outputs/time_series.csv", tells `postlib` to output the timeseries csv at that path.
All the flags available (and their corresponding arguments if required) are:

- Directory: first argument, the directory of npzs to process
- -f (requires path and filename): specifies the yaml config used for sims (default.yaml is used if flag and path not passed)
- -c (requires number): number of cores to use if processing the npzs in parallel
- -csv: whether to create the csvs for timeseries and numerical metrics
- -o (requires path and filename): "path/output_metrics.csv" to save the numerical metrics csv to
- -t (requires path and filename): "path/time_series.csv" to save the timeseries csv to
- -x: whether to conduct a 'dry-run' of the library, outputting no results but printing argument values
- -p: whether to save plots
- -n: type of file to save plot as e.g. pdf
- -d: (requires path): directory to save plots into
- -w: whether or not to see warnings
- -C: if "*" will produce all numerical metrics, if "help" will print available metrics, if a list will only produce those metrics, default is all
- -T: if "*" will produce all timeseries, if "help" will print available metrics, if a list will only produce those metrics, default is all
- -P: if "*" will produce all plots, if "help" will print available plots, if a list will only produce those plots, default is all
- -s: whether or not to shift the timeseries with respect to detection time
- -F: whether to force the numerical metrics to be recomputed or use existing outputted values
- --fix-rq: whether to use the RQ/RT fix. NB: development only, only use if you have read the cobin issue on it

See the [Tutorial](tutorial.md) for more information and an example of running the library on a simulation output directory.
# `networkcontagion` dependencies

The `networkcontagion` package has the following dependencies (package name followed by minimum version). These will be installed/updated by default during installation of the package.

python >= 3.10  
numpy >= 1.16.2  
sortedcontainers >= 2.1.0  
networkx >= 2.2  
PyYAML >= 5.1  
matplotlib >= 3.0.3  
pandas >= 1.5  
scipy >= 1.5.2  
# The Config Loader

In order to allow for more flexible specification of the configuration dictionary, we allow for loading in a YAML file. As part of this process, we implement some additional custom tags in order to extend the functionality of the base PyYAML loader.

## Available Custom Tags

### `!math`

We implement a basic one-operator arithmetic expression parser withe the `!math` tag. This accepts a string that can be split into two floats separated by one of 4 elementary arithmetic operators (`+`, `-`, `*`, `/`).

There is an alias `!+` for this tag.

(yaml_tag_include)=
### `!include`

We can arbitrarily include other compliant YAML files and CSV files as values in our YAML files using the `!include` tag.

These paths are to be either absolute paths, or relative to the file that `!include` is invoked in.

For example, in the directory structure:

```
.
|____start.yml
|____alpha
| |____beta
| | |____beta.yml
| | |____gamma
| | | |____gamma.yml
| |____alpha.yml
```
Where we have the file contents:

:::::::{grid} 1 1 2 2 
:gutter: 1

::::::{grid-item}

:::::{grid} 1 1 1 1
:gutter: 1

::::{grid-item} 
:::{dropdown} start.yml 
```{code} yaml
alpha: !include alpha/alpha.yml
```
::: 
::::

::::{grid-item} 
:::{dropdown} alpha/alpha.yml 
```{code} yaml
!include beta/beta.yml
```
::: 
::::

:::::

::::::

::::::{grid-item}

:::::{grid} 1 1 1 1
:gutter: 1

::::{grid-item} 
:::{dropdown} alpha/beta/beta.yml 
```{code} yaml
gamma: !include gamma/gamma.yml
```
::: 
::::

::::{grid-item} 
:::{dropdown} alpha/beta/gamma/gamma.yml 
```{code} yaml
values:
  - a
  - b
  - c
  - d
values2:
  - 1
  - 2
  - 3
  - 4
```
::: 
::::

:::::

::::::

:::::::

This generates the nested dictionary

```{code} python
{'alpha': {'gamma': {'values':['a','b','c','d'], 'values2':[1,2,3,4]}}} 
```

If the included file is a CSV file, the contents are converted into a {any}`ParameterTable` instead. If constructed this way, the {any}`ParameterTable` will record the filename in the `.extname` attribute.

### `!paramtable`

A {any}`ParameterTable` can be constructed explicitly with the `!paramtable` tag. Typically, this is not done, and is instead done with a CSV using {ref}`include <yaml_tag_include>`.

A {any}`ParameterTable` is a dictionary which parses the attributes of a node in order to generate the lookup key. Internally, the keys exist as tuples, ordered according to the `._keytype` attribute of the {any}`ParameterTable`. We have implemented the ability for hard-coded casts of particular attribute types; for example, the `TA` attribute is cast to `int`.

:::{dropdown} Example `!paramtable` specification

```{code} yaml
content: !paramtable
    keytype: ['TA', 'ethnicity']
    !!python/tuple [76, 'Other']: 1
    !!python/tuple [76, 'Maori']: 2
    !!python/tuple [76, 'Pacific']: 3
    !!python/tuple [76, 'MaoriPacific']: 4
    !!python/tuple [50, 'Other']: 5
    !!python/tuple [50, 'Maori']: 6
    !!python/tuple [50, 'Pacific']: 7
    !!python/tuple [50, 'MaoriPacific']: 8
```
:::

### `!()` shorthand

We implement `!()` as a shorthand for `!!python/tuple`.

(yaml-tag-random)=
### `!random`

The `!random` tag converts a list-like YAML node to a {any}`randomfloat.RandomFloat`. The syntax for the list is:

```{code} yaml
!random [<name>, <distribution argument>, <distribution argument>, ...]
```

where the `name` is any distribution available in Scipy's `stats` module. The `distribution_argument`s are passed to the constructor of the distribution.

For example, we can define a [chi-squared](https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.chi2.html) random variable with 5 degrees of freedom as

```{code} yaml
!random [chi2, 5]
```

or

```{code} yaml
!random
  - "chi2"
  - 5
```


## Config Writing

Configuration dictionaries can be written to disk using the {any}`config_loader.dump` function.
The `frozen` keyword option writes out a _frozen_ version of the parameters, where random variables (created with the `!random` tag) are replaced with their realisation values.

```{admonition} Coming Soon

We will implement freezing of included files.
```

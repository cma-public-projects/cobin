# Model Conceptual Overview

## Epidemic Model Features 
While developed specifically for the context of COVID-19 in Aotearoa, `cobin` models the spread of relatively generic processes that others can use to model infectious disease spread in other contexts.
The processes modelled include:

- non-symptomatic infection (pre-symptomatic and fully asymptomatic pathways), as well as symptomatic infection
- severe outcomes (hospitalisation, critical care, and death)
- spontaneous (symptom-driven) testing
- self-isolation, contact tracing, and induced testing
- non-pharmaceutical interventions (NPIs), i.e. lockdowns and restrictions
- multi-dose vaccination
- reinfection 
- waning immunity
- individual and context heterogeneity

## Model details
```{toctree}
:maxdepth: 1

How individuals, infections, and interventions are modelled <modelling_individuals_and_processes>
Transmission modelling/implementation <transmission_modelling>
Bipartite networks <cobin_bipartite>
Event flow diagrams <cobin_event_flows>
Simulating discrete jumps in time <algorithm>
Assumptions, limitations, and quirks <assumptions_limitations>
Generalising concepts specific to Aotearoa <generalising_aotearoa_specific>
```

## Target audience and package usage
Originally developed for use by CMA in providing modelling advice to the New Zealand Government, this package is now designed to be usable by anyone with an interest in epidemics modelling (from amateur to academic) or by individuals who want to see/recreate/investigate the modelling efforts of CMA on behalf of Aotearoa.
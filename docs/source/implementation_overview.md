# Model Implementation Overview

The ``networkcontagion`` package is written for generic jump process models.
It is currently implemented as a Python library, which requires user-defined modules and calls in a script.

The top-level object is the `Simulation` from `networkcontagion.simulation`, which assembles and passes the associated functions from the user-defined modules to the underlying simulation algorithm. It also handles writing out the results.

## Inputs
```{toctree}
:maxdepth: 2

Input files required <inputs>
Dictionary of config file input parameters <input_parameters_dict>
More information on specifying initial seed cases <seeding_initial_cases>
More information on the config loader <config_loader>
```


## Internal processes
```{toctree}
:maxdepth: 1

Overview of simulation steps <sim_steps_overview>
How an NPI is simulated <simulating_NPIs>
event_queue_delayed_events
Continuing simulations <restarting_sims>
```

## Outputs
```{toctree}
:maxdepth: 1

outputs
```

## Parallelisation and parameter gridding
```{toctree}
:maxdepth: 1

parallelisation_and_gridding
```

## Postlib library
```{toctree}
:maxdepth: 1

postlib
```

## Library module and functions documentation
```{toctree}
:maxdepth: 1

modules
```
# Dictionary of networkcontagion config parameters
```{contents}
```
This is an exhaustive list of the possible parameters that can be used in a config.yaml. In practice we do not usually use all of these options (some parameters are also mutually exclusive). The page/list is broken into sections that reflect the parameter's position in the yaml (e.g. run parameters are found under the `run_options:` dictionary heading) or their effect on the model (e.g. disease trajectory includes parameters like the time spent in each disease state and the likelihood of symptom onset).

## Simulation set up, run, and initial condition parameters
### Simulation configuration parameters
The following parameters are specified at the top of the config.yaml, they are not nested under a YAML key heading.
::::{table} Simulation overall parameters
:align: left
|Parameter|Description|Structure/Value Type|Example Value|
|:----|:----|:----|:----|
|graph|File path to bipartite network object to run contagion process on|str|/example_networks/no_doses_TA50_network.gpickle|
|num_runs|The number of simulation runs to set running. WARNING: Currently this parameter is not functional, if set to a value other than 1 the simulation will break with an error message.|int|1|

::::

### Run parameters
The following parameters can be specified under the `run_options:` YAML dictionary heading.
More info on restarting sims can be found [here](restarting_sims.md)
::::{table} Simulation run parameters
:align: left
|Parameter|Description|Structure/Value Type|Example Value|
|:----|:----|:----|:----|
|run_options: num_runs|The number of simulation runs to set running. WARNING: Currently this parameter is not functional, if set to a value other than 1 the simulation will break with an error message.|int|1|
|run_options: dump_internals|If True, dumps out the ‘internals’ of the simulation to be used in continuing a simulation (see restart_from)|bool [default = false]|false|
|run_options: tmin|(Absolute) Time to start simulation at. If running chains of re-start sims 'relative' time refers to the start of the restart sim, 'absolute' time refers to the overarching timeline of the chained sims|int [default = 0]|10|
|run_options: tmax|Maximum absolute time that the simulation runs to. No events past that time will be recorded|int [default = 300]|60|
|run_options: full_return:|If True, returns additional information in the npz about the details of the events (involved nodes and contexts, status of involved nodes), if False only returns time and counts of return_states|bool [default = false]|false|
|run_options: output_dir:|Directory to output resultant npz files to|str [default = ./output]|./simulation_output|
|run_options: max_infections:|Maximum number of infections, after which the simulation immediately terminates|int|1000|
|run_option: print_params:|If True, dumps out a frozen copy of the parameters as a .yaml file with the same name stem as the associated npz file|bool [default = false]|false|
|run_options: random_seed:|Random seed to set for the MT pseudo-random number generators (random and numpy.random)|int|123456987|
|run_options: restart_from:|File path to a .internals.pkl file output from a previous sim to restart from (see [here](restarting_sims))|str|./prev_simulation_output/example.internals.pkl|
::::

### Initial conditions
The following parameters can be specified under the `initial_conditions:` YAML dictionary heading. For more information on the New Zealand specific regional definitions (and how they can be generalised), see [here](generalising_aotearoa_specific).


::::{table} Initial condition parameters
:align: left
|Parameter|Description|Structure/Value Type|Example Value|
|:----|:----|:----|:----|
|initial_conditions: num_seeds:|Number of random seed individuals to generate|int|10|
|initial_conditions: whole_groups:|Whether or not to seed entire groups (for random seeding or group_list seeding)|bool|True|
|initial_conditions: whole_expose_type|Types of groups to seed the entirety of, if seeding entire groups|(iterable | None)|['DW']|
|initial_conditions: random:|Whether or not to sample randomly. (If False, then one of ``seed_all``, ``seed_individual_id``, or ``group_lists`` must be given.)|bool|True|
|initial_conditions: TA:|TA to restrict random seeding to. For more information on TA's see [here](generalising_aotearoa_specific)|int|50|
|initial_conditions: SA2:|SA2 to restrict random seeding to. Ignored if TA is provided. For more information on TA's see [here](generalising_aotearoa_specific)|int|257300|
|initial_conditions: seed_individual_id:|A single individual, or a list of individuals that are the seed cases|str | list[str]: <br>```-IH760686769``` <br> ```-IH760686780```| 
::::


## networkcontagion simulation parameters
The following parameters are all specified in the config.yaml under the YAML dictionary heading `parameters`.

### Disease trajectory parameters
Once an individual is infected/exposed (E state in our model), there are a number of parameters that determine the progression of their infection pathway.

::::{table} Disease trajectory parameters
:align: left
|Parameter|Description|Structure/Value Type|Example Value|
|:----|:----|:----|:----|
|parameters: p_asympt_list|Age structured proportions of infectious cases that are asymptomatic, can be specified further by other node attributes like ethnicity|list, can also include refer to csv's|p_asympt_list:<br>```- 0217```<br>```- 0.196```<br>```- 0.142```<br>```- 0.108```<br> OR <br>p_asympt_list: include '/networkcontagion/configs/parameters_by_TA/p_asympt.csv'|
|parameters: p_mild_list|Age structured proportions of infectious cases that are mild, can be specified further by other node attributes like ethnicity|list, can also !include refer to csv's|p_mild_list:<br>```- 0.768```<br>```- 0.778```<br>```- 0.74175```<br>```- 0.6253```<br>OR<br>p_mild_list: !include '/networkcontagion/configs/parameters_by_TA/p_mild.csv'|
|parameters: gamma|Parameters to specify the gamma distribution of the incubation period (from noninfectious to infectious eg E -> P)|`dict` str -> float|gamma:<br>```scale: 0.25```<br>```shape: 8```|
|parameters: delta_bm|Rate of Symptom Progression of mild infections (B -> M)|float|0.85|
|parameters: delta_pi|Rate of symptom progression of serious infections (P -> I)|float|0.85|
|parameters: delta_ih|Rate of symptom progression for hospitalisation (I -> H)|float|0.2|
|parameters: delta_hc|Rate of symptom progression for critical hospitalisation (H -> C)|float|0.5|
|parameters: alpha_ar|Rate of recovery for asymptomatic infections (A -> R)|float|0.3|
|parameters: alpha_mr|Rate of recovery for mild infections (M -> R)|float|0.1|
|parameters: alpha_md|Rate of Fatal Termination Rate for mild infections (M -> D)|float|0.1111|
|parameters: alpha_ir|Rate of recovery for serious infections (I -> R)|float|0.3|
|parameters: alpha_id|Rate of Fatal Termination for serious infections (I -> D)|float|0.1111|
|parameters: alpha_hr|Rate of recovery for hospitalisations (H -> R)|float|0.2083|
|parameters: alpha_cr|Rate of recovery for critical hospitalisations (C -> R)|float|0.0555|
|parameters: alpha_cd|Rate of Fatal Termination for critical hospitalisations (C -> D)|float|0.1111|
|parameters: mu_m|Mortality probability for mild infection (NB: CMA has not used this functionality, instead using only mu_c_list - meaning deaths only occur once an individual has been hospitalised and in a critical condition)|float|0|
|parameters: mu_i|Mortality probability for serious infection (NB: CMA has not used this functionality, instead using only mu_c_list - meaning deaths only occur once an individual has been hospitalised and in a critical condition)|float|0|
|parameters: mu_c_list|Age structured mortality probabilities for critically hospitalised individuals (state "C"), can be specified further by other node attributes like ethnicity|list, can also !include refer to csv's|mu_c_list:<br>```- 0.3```<br>```- 0.4```<br>```- 0.5```<br>```- 0.8```<br>OR<br>mu_c_list: !include 'parameters_by_TA/mu_c.csv'|
|parameters: eta_h_list|Age structured probability of hospitalisation (transitioning to state "H") given individual is 'seriously' infected (in state "I"). Can be specified further by other node attributes like ethnicity|list, can also !include refer to csv's|eta_h_list:<br>```- 0.002```<br>```- 0.015```<br>```- 0.2```<br>```- 0.4```<br>OR<br>ea_h_list: !include '/networkcontagion/configs/parameters_by_TA/eta_h.csv'|
|eta_c_list|Age structured probability of entering critical care (transitioning to state "C") given individual is hospitalised  (in state "H"). Can be specified further by other node attributes like ethnicity|list, can also !include refer to csv's|eta_c_list:<br>```- 0.05```<br>```- 0.05```<br>```- 0.1```<br>```- 0.5```<br>OR<br>ea_c_list: !include '/networkcontagion/configs/parameters_by_TA/eta_c.csv'|
|parameters: alpha_sr|Rate at which recovered individuals become susceptible again (R -> S). Previously infected individuals in S have different immunity to individuals in S who have not been infected. Their immunity starts waning from the time they transition from R to S.|float|alpha_sr: 0.14|
::::

### Transmission parameters
Parameters relating to the spread of disease within different contexts and for different demographic groups.
::::{table} Transmission parameters
:align: left
|Parameter|Description|Structure/Value Type|Example Value|
|:----|:----|:----|:----|
|parameters: beta_0|Transmission rate to casual contacts for an infected individual (age structured)|ParameterTable. Non-negative float, 1 value for each age band|beta_0: <br>```- 0.18```<br>```- 0.18```<br>```- 0.18```<br>```- 0.18```|
|parameters: beta_c|Transmission rate to close contacts for an infected individual (age structured)|ParameterTable. Non-negative float, 1 value for each age band|beta_c:<br>```- 3.0```<br>```- 3.0```<br>```- 3.0```<br>```- 3.0```|
|parameters: beta_h|Density dependent transmission rate for an infected individual to density-dependent groups|ParameterTable. Non-negative float, 1 value for each age band|beta_h:<br>```- 1.0```<br>```- 1.0```<br>```-1.0```<br>```- 1.0```|
|parameters: density|Density dependent contagion coefficient. density = 0.0 represents pure frequency dependence of the beta_h process, density = 1.0 represents pure density dependence of the beta_h process|non-negative float|0.8|
|parameters: density_groups|List of groups that use density-dependent contagion instead of weighted frequency dependence|str | list[str]|density_groups: ['DG', 'DZ']|
|parameters: beta_weights|Age structured susceptibility to infection (acts as a weighting on the transition likelihood) - values below 1 decrease the likelihood of being infected for that age group|ParameterTable.Non-negative float, 1 value for each age band|beta_weights:<br>```- 1.0```<br>```- 1.0```<br>```- 1.0```<br>```- 1.0```|
|parameters: group_type_weights|Likelihood weights on transmission contexts (groups). Can be thought of as the proportion of time spent in each context supertype for casual contact interactions. Contexts are defined in contact_tracing.py `_groups`|`dict` context supertype -> float|group_type_weights:<br>```HOME: 1```<br>```WORK: 2```<br>```COMM: 1.33```|
|parameters: eps_a|Relative infection pressure of Asymptomatic individuals (compared to Infected individuals)|float|0.7|
|parameters: eps_b|Relative infection pressure of Presymptomatic Mild (infection)  individuals (compared to Infected individuals)|float|0.7|
|parameters: eps_m|Relative infection pressure of Mild (infection)  individuals (compared to Infected individuals)|float|0.7|
|parameters: eps_p|Relative infection pressure of Presymptomatic individuals (compared to Infected individuals)|float|0.7|
|parameters: eps_h|Relative infection pressure of Hospitalised individuals (compared to Infected individuals)|float|0|
|parameters: eps_c|Relative infection pressure of Critically hospitalised individuals (compared to Infected individuals)|float|0|
|parameters: close_contact_weights|Likelihood weights on transmission contexts (groups). Can be thought of as the proportion of time spent in each context supertype for close contact interactions. Contexts are defined in contact_tracing.py `_groups`|`dict` context supertype -> float|close_contact_weights:<br>```HOME: 2.5```<br>```WORK: 1.0```<br>```COMM: 4.0```|
|parameters: group_size_reject|Threshold group size over which transmission attempt is rejected. This can be set at different values for different adaptive lockdown levels, essentially controlling the size of groups that are 'open' as transmission contexts during different adaptive lockdown levels|`dict` default -> float; adaptive lockdown level -> float|group_size_reject:<br>```default: 1000```<br>```limit_gatherings_100: 100```|
|parameters: transmission_event_parameters|Dictionary of parameters related to the size of a community transmission event - the number of infections that occur in a single infection event. This number of infections is drawn from a roughly log-normal distribution with the underlying exponential distribution having a mean of log(max(log10(N),1)) where N is the event size. The `transmission_event_parameters` (`exponent`, `beta`, `alpha`, `sigma`) specify this distribution|`dict` parameter name -> float|transmission_event_parameters:<br>```exponent: 0.2```<br>```sigma: 0.1```<br>```alpha: 1.1```<br>```beta: 8.5```|

::::

### Vaccination and reinfection parameters
Parameters for specifying vaccination levels in individuals. There are a number of sub-parameters to be specified under the vaccination parameter. NB: in the config yamls supplied in this package, we use !() as a shorthand for !!python/tuple. For more information see the detail provided on the [config loader](config_loader.md).

::::{table} Vaccination and reinfection parameters
|Parameter|Description|Structure/Value Type|Example Value|
|:----|:----|:----|:----|
|parameters: vaccination: date_offset|Offset between start date of vaccinations and start date of simulation.|int|date_offset: 50|
|parameters: vaccination: (n doses, prev infection) tuple|Parameters nested below these (int, bool) tuples specify the immunity status for people with different levels of vaccination and previous infection. Tuples have two entries (n, bool) is someone with n doses of vaccination and bool = true if they have been infected previously, otherwise false|(n, bool) tuple|? !()<br>```- 2```<br>```- true```|
|parameters: vaccination: tuple: waning|Specify the immunity parameters of logistic functions for 6 different outcomes: acquisition; critical; hospitalisation mortality; onwards; symptoms. For each outcome, specify three-parameter tuples for a logistic function of the form Immunity(t) = arg1 / (1 + exp( - arg2 * (t - arg3))).These logistic values are multipliers (less than 1) that are multiplied by the conditional probabilities of acquisition (S --> E) per exposure event, symptoms (E -> S), hospitalisation (I -> H), critical care (H -> C) and death (C-> D). If you do not want dynamic immunity, set these values at ```-2 -0 -0``` and use the static vaccination parameters.|outcome: (float, float, float)|From '/networkcontagion/configs/vaccine_efficacy/omicron/with_infection/pfizer_one_dose.yaml' which is !include-ed in configs/vignette_config_vaccination.yaml: <br>```acquisition:```<br>```  - 0.2903254952600825```<br>```  - 0.011652053267960122```<br>```  - 427.915835515277```<br>```critical:```<br>```  - 2.0```<br>```  - 0.0```<br>```  - 0.0```<br>```hospitalisation:```<br>```  - 0.12296228883984998```<br>```  - 0.024903758760558385```<br>```  - 5.7152240787198645```<br>```mortality:```<br>```  - 2.0```<br>```  - 0.0```<br>```  - 0.0```<br>```onwards:```<br>```  - 2.0```<br>```  - 0.0```<br>```  - 0.0```<br>```symptoms:```<br>```  - 12.750372846747291```<br>```  - 0.0013145370973778```<br>```  - 2459.464420691619```|
|parameters: vaccination: tuple: txn_weight|Static multipliers for reduction in transmission due to the infector's immunity status. These values in [0,1] are multiplied by the dynamic logistic values in parameters: vaccination: tuple: waning: onwards to give the protection against onwards transmission.|list(float)|txn_weight:<br>```- 0.95```<br>```- 0.95```<br>```- 0.95```<br>```- 0.95```|
|parameters: vaccination: tuple: rcv_weight|Static multipliers for protection against infection per exposure due to the susceptible's immunity status. These values in [0,1] are multiplied by the dynamic logistic values in parameters: vaccination: tuple: waning: acquisition to give the protection against infection per exposure.|list(float)|rcv_weight:<br>```- 0.35```<br>```- 0.5```<br>```- 0.5```<br>```- 0.5```|
|parameters: vaccination: tuple: p_sympt|Static multipliers for protection against symptomatic infection. These values in [0,1] get multiplied by the dynamic logistic values in parameters: vaccination: tuple: waning: symptoms to give the protection against symptomatic disease. |list(float)|p_sympt:<br>```- 0.75```<br>```- 0.75```<br>```- 0.75```<br>```- 0.75```|
|parameters: vaccination: tuple: p_mild|Static multipliers for the protection against mild disease. These values in [0,1] get multiplied by the dynamic logistic values in parameters: vaccination: tuple: waning: symptoms to give the protection against mild disease.|list[float]|p_mild:<br>```- 1.0```<br>```- 1.0```<br>```- 1.0```<br>```- 1.0```|
|parameters: vaccination: tuple: testing: |Static multipliers for testing parameters dependent on the individuals immunity status. For more information on testing parameters see the TTIQ parameter table below.|list[parameter (str), float]|testing: <br>```p_test_0: 1```<br>```p_test_m: 1```<br>```p_test_i: 1```<br>```theta_0: 1```<br>```theta_m: 1```<br>```theta_i: 1```|
|parameters: vaccination: tuple: hospitalisation: eta_h: |Static multipliers [baseline] * [age: ] for protection against hospitalisation due to the infected's immunity status. The baseline value will be multiplied by an age-dependent value or default to get a product in [0,1] which is then multiplied by the dynamic logistic values in parameters: vaccination: tuple: waning: hospitalisation to give the protection against hospitalisation. |list[parameter (str),  float]|eta_h: <br>```age:```<br>```  default: 1```<br>```baseline: 1```|
|parameters: vaccination: tuple: hospitalisation: eta_c: |Static multipliers [baseline] * [age: ] for protection against critical care due to the hospitalised's immunity status. The baseline value will be multiplied by an age-dependent value or default to get a product in [0,1] which is then multiplied by the dynamic logistic values in parameters: vaccination: tuple: waning: critical to give the protection against critical care. |list[parameter (str),  float]|eta_c: <br>```age:```<br>```  default: 1```<br>```baseline: 1```|
|parameters: vaccination: tuple: mortality: mu_i|Static multiplier for mu_i dependent on the individual's immune status. This value is in [0,1] and is NOT multiplied by a dynamic factor.|float|mu_i: 1|
|parameters: vaccination: tuple: mortality: mu_m|Static multiplier for mu_m dependent on the individual's immune status. This value is in [0,1] and is NOT multiplied by a dynamic factor.|float|mu_m: 1|
|parameters: vaccination: tuple: mortality: mu_c:|Static multipliers [baseline] * [age: ] for protection against death due to the critical-care's immunity status. The baseline value will be multiplied by an age-dependent value or default to get a product in [0,1] which is then multiplied by the dynamic logistic values in parameters: vaccination: tuple: waning: mortality to give the protection against death. |list[parameter (str),  float]|mu_c:<br>```age:```<br>```  default: 1```<br>```baseline: 1```|
::::


### TTIQ and related behaviour parameters
Parameters related to TTIQ policies and related behaviour (such as an individual's likelihood of seeking a test based on their vaccination and symptom status).
::::{table} Behaviour modification parameters
:align: left
|Parameter|Description|Structure/Value Type|Example Value|
|:----|:----|:----|:----|
|parameters: p_test_0|Probability that asymptomatics/presymptomatic infections get tested|float|0|
|parameters: p_test_m|Probability that mild infections get tested|float|0|
|parameters: p_test_i|Age structured probability that serious infections get tested|float|p_test_i:<br>```- 0.6```<br>```- 0.6```<br>```- 0.6```<br>```- 0.6```|
|parameters: theta_0|Rate at which asymptomatics/presymptomatics seek tests|float|0|
|parameters: theta_m|Rate at which mild cases seek tests|float|0.5|
|parameters: theta_i|Rate at which serious cases seek tests|float|0.8|
|parameters: theta_lab|Rate parameter for exponential distribution of delay for lab results to be returned|float|0.5|
|parameters: quarantine|Testing probabilities and rates while individual is in isolation. Can specify values for p_test_0, p_test_m, p_test_i, theta_0, theta_m, theta_i|`dict` parameter -> float|quarantine:<br>```p_test_0: 0.9```<br>```p_test_m: 0.9```<br>```p_test_i: 0.9```<br>```theta_0: 0.5```<br>```theta_m: 0.5```<br>```theta_i: 0.5```|
|parameters: test_also_iso|Transmission context groups that distance (XD state) if someone within them seeks a test. Select any of ["HOME", "WORK", "COMM"]|list[str]|test_also_iso:<br>```- HOME```|
|parameters: kappa_0|Shape and scale parameters for weibull distribution of trace times|kappa_0:<br>```scale: 3```<br>```shape: 2```|
|parameters: kappa_c|Parameter for exponential distribution of re-trace time (followup trace if unsuccessful trace attempt)|float|4|
|trace_weights|Priority weights for contact tracing. For each super context, a weight needs to be specified - these can be split for casual and close contacts within that supercontext - for prioritising contact tracing through contexts|super context : float|trace_weights:<br>```HOME:```<br>```  close: 0```<br>```  casual: 0.25```<br>```WORK:```<br>```  close: 2```<br>```  casual: 0.25```<br>```COMM:```<br>```  close: 2```<br>```  casual: 0.25```|
|parameters: escape_time|Duration of minimum quarantine (days)|vaccine dosage: float|escape_time: <br>```default: 5```<br>```0: 5```<br>```1: 5```<br>```2: 5```<br>```3: 5```|
|parameters: notrace|Probability that an event cannot be traced (by group type)|context type: float|notrace:<br>```dw: 0```<br>```dg: 0```<br>```dz: 0```<br>```we: 1```<br>```wp: 1```<br>```wg: 1```<br>```wz: 1```<br>```sc: 1```<br>```sg: 1```<br>```sz: 1```<br>```pm: 1```<br>```pg: 1```<br>```pz: 1```<br>```ev: 1```<br>```ex: 1```|
|parameters: trace_drop|Probability that a contact attempt fails (by group type)|context type: float|trace_drop:<br>```dw: 0.01```<br>```dg: 0```<br>```dz: 0```<br>```we: 0.1```<br>```wp: 0.1```<br>```wg: 0.1```<br>```wz: 0.1```<br>```sc: 0.1```<br>```sg: 0.1```<br>```sz: 0.1```<br>```pm: 0.1```<br>```pg: 0.1```<br>```pz: 0.1```<br>```ev: 0.3```<br>```ex: 0.1```|
|parameters: max_trace_attempts|Maximum number of contact trace attempts for any given contact|int|3|
|parameters: casual_test_prob|Probabilities that casual contacts of a known case get tested|context type: float|casual_test_prob:<br>```default: 0.1```<br>```dw: 0.5```<br>```dz: 0.5```<br>```we: 0.5```<br>```wp: 0.5```<br>```wz: 0.5```<br>```sc: 0.4```<br>```sz: 0.4```<br>```pm: 0.2```<br>```pz: 0.2```<br>```ev: 0.1```|
|parameters: casual_test_delay|Parameters for the distribution of delay for casual contact to get back a positive test. Delay = betavariate([core][alpha], [core][beta]) * [scale] + [loc]|parameter (str): float|casual_test_delay:<br>```core:```<br>```  alpha: 3```<br>```  beta: 5```<br>```scale: 4.5```<br>```loc: 2.5```|
::::

### Behaviour modification parameters
Parameters relating to a change in an individual's behaviour as a result of NPI 
[Simulating Non Pharmaceutical Interventions (NPIs)](simulating_NPIs.md) gives more information on how to implement `lockdown_shutdown`events
::::{table} Behaviour modification parameters
:align: left
|Parameter|Description|Structure/Value Type|Example Value|
|:----|:----|:----|:----|
|parameters: omega|Infection leakage/relative contact between different TTIQ states ("CQWD") and unrestricted state ("U")|`dict` policy; control state -> float | omega: <br>```policy: home```<br>```C: 0.01```<br>```Q: 0.05```<br>```W: 0.05```<br>```D: 0.5```|
|parameters: adaptive_lockdown|Dictionary of triggers for a single adaptive lockdown. For more info see [here for NPI implementation details](simulating_NPIs.md) and the TTIQ parameters table above for parameter explanations. Key options are:<ul><li>name: name of the adaptive_lockdown.</li><li>level: name of the level of lockdown - this is the attribute that is referred to when specifying different parameter values that are in place during this adaptive lockdown level.</li><li>ncumul: cumulative number of known cases trigger.</li><li>nactive: current number of known active cases trigger.</li>delay: amount of time between the trigger being satisfied and the adaptive_lockdown being enacted. Must be specified. If ncumul not set then the adaptive_lockdown is enacted at t= delay. This is ALWAYS RELATIVE to the start of the sim that it is queued in - if you are running a restart sim and specifying an adaptive_lockdown delay in the restart sim's config the delay is relative to the restart sim's t=0, NOT the absolute timeline of your chained restart sims.</li><li>exhausted: whether the adaptive_lockdown can be triggered or note.</li></ul>
|`dict`: name: str; level: str; nactive: int; ncumul: int; delay: int; exhausted: bool;|adaptive_lockdown:<br>```- name: choose_name```<br>```ncumul: 500```<br>```nactive: 80```<br>```delay: 10```<br>```level: default_lockdown```<br>```exhausted: True/False```|
|parameters: lockdown_weights|Dictionary of lockdown weights per group on beta during a specified adaptive_lockdown level. During this lockdown, transmission within each context is reduced to the factor listed for that context in lockdown_weights|`dict`: context code: float|lockdown_weights:<br>```RED:```<br>```  dw: 1.0```<br>```  dg: 1.0```<br>```  dz: 1.0```<br>```  sc: 0.2```<br>```  sg: 0.3```<br>```  sz: 0.3```<br>```  pm: 0.2```<br>```  pg: 0.3```<br>```  pz: 0.3```<br>```  we: 0.5```<br>```  wp: 0.5```<br>```  wg: 0.8```<br>```  wz: 0.8```<br>```  ev: 0.5```<br>```  ex: 0.9```|
|parameters: lockdown_testing|Dictionary of upweights on testing parameters (likelihoods, rates) during specified adaptive_lockdown level, can specify weightings to p_test_ parameters, theta_ parameters.|`dict`: testing parameter: float|  lockdown_testing:<br>```RED:```<br>```  p_test_0: 0.0```<br>```  p_test_m: 0.5```<br>```  p_test_i:```<br>```  - 0.4```<br>```  - 0.4```<br>```  - 0.4```<br>```  - 0.4```<br>```  theta_0: 0.33```<br>```  theta_m: 0.5```<br>```  theta_i: 0.5```<br>```  theta_lab: 0.5```|
|parameters: lockdown_shutdown|Dictionary that contains information about what proportion of different interaction nodes to close during a specified adaptive_lockdown level. Values take a float between 0.0 and 1.0. It is also possible to use ‘RED, AMBER, GREEN’ instead of a proportion which will instead only shutdown nodes that were shutdown in that level of the New Zealand COVID-19 Protection Framework|`dict`: adaptive_lockdown_level; group_node_type: float [0,1] | str [‘RED, AMBER, GREEN’]|lockdown_shutdown:<br>```_by_map_:```<br>```  OPEN: '00'```<br>```  CLOSED: 0X```<br>```lockdown_level_name:```<br>```  we: RED```<br>```  wp: RED```<br>```  wg: RED```<br>```  wz: RED```<br>```  ev: 0.5```<br>```  ex: 0.2```|
|parameters: lockdown_state|Tracks what adaptive_lockdown level the simulation is in as it runs. To start simulation with no adaptive_lockdown in place, set to 0|int|str|lockdown_state: 0|
|parameters: trace_policy|What to do for close and casual contacts at each alert level. Possible actions are: "immediate" - immediately attempt to trace contacts to place them into isolation (only sensible to use for household members), "trace" - after a random delay attempt to contact trace and place contacts into isolation (state "Q"), "test" - schedule a test for contacts after a random delay, "skip" - do not attempt to contact trace.|`dict`: group type: str|trace_policy: <br>```default:```<br>```  close: trace```<br>```  casual: test```<br>```  EV: skip```<br>```  DG: immediate```<br>```  DZ: immediate```|
::::

### Simulation reseeding
Parameters relating to seeding individuals at a time after the start of the simulation (reseeding).
::::{table} Behaviour modification parameters
:align: left
|Parameter|Description|Structure/Value Type|Example Value|
|:----|:----|:----|:----|
|parameters: reseed|Parameters for seeding infections dynamically. Reseeding takes similar options to the initial condition options, and has a few additional fields. Most importantly: `time` (when the reseeding occurs - required parameter) and `replay` (how often it repeats - optional parameter).|Multiple parameters - see initial conditions|See `configs/vignette_config_reseed.yaml` for an example|
|parameters: reseed: name|Name of the reseeding event|str|reseed_100|
|parameters: reseed: name: time|At what simulation time the reseed occurs|int|time: 10|
|parameters: reseed: name: replay|How often the reseeding repeats (period in days)|int|replay: 7 # Reseed again weekly|
|parameters: reseed: name: only_if|Which states to seed into|str, list[str]|['SU','SQ','SW']
|parameters: reseed: name: max_attempts|Maximum number of attempts at randomly drawing valid individuals to seed|int|1000000|
::::
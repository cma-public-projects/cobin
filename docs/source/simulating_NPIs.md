# Simulating Non Pharmaceutical Interventions (NPIs)
Non pharmaceutical interventions (NPIs) are implemented as modifications to:

- possibility of transmission through contexts (e.g. school closure)
- probability of transmission through contexts, if transmission is possible (e.g. improved hygiene practices)
- probability and rate of spontaneous testing

## Triggering an NPI in the simulation
The actioning of these NPIs is dependent on _triggers_ defined in the `adaptive_lockdown` parameter.

Each trigger has a unique identifier, or `name`, which is used to track triggers as they queue up.

Triggers have a `delay` field that specifies the amount of time between the trigger being satisfied (see below) and the NPI being enacted, representing the turnaround time for legal and political procedures.

A trigger must satisfy all its conditions in order to cause the NPI to be enacted. These depend on two global counters that track both the current number of known active (infectious) cases (through test confirmations) and the cumulative number of known cases. Trigger conditions that rely on these two counters are defined in the `nactive` and `ncumul` fields of the trigger, respectively. The trigger conditions are listed in the table below based on the specification of these fields:

:::{table} Lockdown Trigger Conditions
:align: center

| Spec | `nactive` not specified | `nactive` positive (>=0) | `nactive` negative (<0) |
| :--: | :---------------------: | :----------------------: | :---------------------: |
| **`ncumul` not specified** | Always satisfied (trigger immediately) | When known active cases are above given `nactive` | When known active cases are below (absolute value of) given `nactive` |
| **`ncumul` specified** | When total known cases above given `ncumul` | When known active cases are above given `nactive`, *and* total known cases above given `ncumul` | When known active cases are below (absolute value of) given `nactive`, *but* total known cases above given `ncumul` |
:::

After a trigger is satisfied, and the NPI is enacted, it becomes `exhausted`, and cannot trigger again unless it is no longer `exhausted`. A trigger can become non-`exhausted` by being reactivated by another specific NPI being enacted. This is defined by the `reactivated_by` field. This field is list-valued, and any NPI being enacted that matches the name of an element of that list will cause that trigger to become non-`exhausted`. This is particularly useful for modelling 'opening-up' scenarios, where a reduction in restrictions must follow an earlier increase of restrictions.

## How an NPI (`lockdown_shutdown`) plays out in the simulation
To understand the effects of the provided lockdown parameters, it is best to look at an example. In the example configuration that we provide (see [tutorial](tutorial.md) for more information), we set one adaptive lockdown called high_lock to trigger once a cumulative number (`ncumul`) of cases of 50000 is reached. 

Within this adaptive lockdown, we set the time `delay` to '0' so that the lockdown starts as soon as the cumulative case number is reached. The `level` is set to 'RED', meaning that values tagged by 'RED' are used for the `lockdown_weights, lockdown_testing, lockdown_shutdown` parameters during this lockdown. 

During this lockdown, transmission within each context is reduced to the factor listed for that context in `lockdown_weights`. For example, transmission rate within a dwelling (`dw,dg,gz`) is not reduced at all ('1.0','1.0','1.0') whereas transmission within a close community event is reduced to 90% transmission (`ex`: '0.9'). 

This lockdown also causes a random proportion of interaction nodes to close (`lockdown_shutdown`). For example, we can see that 50% of both close and casual workplaces are closed due to the lockdown (`we,wp,wg`: '0.5'). It is also possible to use 'RED, AMBER, GREEN' instead of a proportion which will instead only shutdown nodes that were shutdown in that level of the New Zealand COVID-19 Protection Framework (e.g. `we,wp,wg`: 'RED' would only allow transmission through workplaces that were open during the Red Level).

We also recognise that if this lockdown is triggered that individuals may change their testing habits (in this case they reduce their testing maybe because of a decreased availability of tests/less desire if they are only at home anyways). The `lockdown_testing` at this level shows a decrease to a '0.4' likelihood an infectious individual of any age seeking a test (`p_test_i`), as well as a change in the rates of infectious individuals seeking a test (`theta_i`: '0.5').

Finally, due to the lockdown, the contact tracing policy employed has changed from the `default` level to the `RED` level. This coincides with no change in that `close` contacts are  'traced', but we now 'test' `casual` contacts and `EV` contacts. There is no change as to the 'immediate' tracing of dwelling contacts.
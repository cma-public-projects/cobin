# Installation

Installation of this library is relatively straightforward. Before proceeding, if you are relatively new to Python, it may be useful to consider the {doc}`prereqs`.

## `pip` installation

You can install the package with `pip` directly like so (the following commands on this page require you to have navigated to the networkcontagion directory):

```bash
pip install .
```

If you are using conda, you can preinstall [dependencies](dependencies.md) with

```bash
conda env update --file environment.yml
```

before running the pip install. 

environment.yml lists the `networkcontagion` package dependencies.

## Development Installation

To develop on the package, you can install with `pip` using the `--editable` option:

```bash
pip install --editable .
```

To additionally build the documentation, you can install the package with the `docs` extras:

```bash
pip install .[docs]
```

**Note:** if you are using the `zsh` shell, '[]' are used to denote wildcards and thus the above command should be edited to instead include: `'.[docs]'`

These two install alternatives can also be combined:
```bash
pip install --editable .[docs]
```

NB: The documentation requires the `GNU Make` tool, as it uses a `makefile` when generating the docs. To check if you currently have `GNU Make` installed, simply run `make` within a terminal. If it is installed, you should either get an error that there is no targets specified and no makefile found, or some help information from make. If you do not then you will have to install make (if you want to generate the docs).

If you are using conda, you can install the tool using:

For MacOS or other *NIX system:

```bash
conda install -c anaconda make
```

For Windows users:

```bash
conda install -c conda-forge m2w64-make
```

For non-Conda users, `GNU Make` can be downloaded through various package managers such as `homebrew` for *NIX and `chocolatey` for Windows.

To build the docs, first navigate in the terminal to the `docs` directory, and then use `make` to build the documentation. **Note:** if you are using the `zsh` shell, '[]' are used to denote wildcards and thus the below command should be edited to instead include: `'[docs]'`

```bash
cd docs
make docs
```

For more information on the building process see the README in the `docs` directory.

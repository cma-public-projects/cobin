# This is to test the random_get that Peter Maxwell suggested.

import networkx as nx
import random
from itertools import chain, islice
import timeit
from collections import Counter
from lib.graph_handling import load_from_pickle

def neighbours(G, node):
    return (i for x in G[node] for i in G[x] if i != node)

# B = nx.bipartite.random_graph(100, 5000, 0.05)
B = load_from_pickle(r"data\TA50_cmScEmDwNetwork_k3d5x12pl.gpickle")

for nd in B.nodes:
    if B.nodes[nd]['bipartite'] == 1:
        B.nodes[nd]['n'] = len(list(neighbours(B, nd)))

def get_random_neighbour_new(G, node):
    N = (len(G[g]) -1 for g in G[node])
    random_index = random.randint(0, G.nodes[node]['n']-1)
    offset = 0
    for ni,g in zip(N, G[node]):
        if random_index < offset + ni:
            return [i for i in G[g] if i != node][random_index-offset]
        offset += ni
    raise RuntimeError

def get_random_neighbour_old(G, node):
    random_index = random.randint(0, G.nodes[node]['n']-1)
    return generator_get(neighbours(G, node), random_index)

def generator_get(generator, index):
    return next(islice(generator, index, None))

Inds = [n for n in B.nodes if B.nodes[n]['bipartite'] == 1]

print("Old")
x, y = [], []
start = timeit.default_timer()
for _ in range(10000):
    nd0 = random.choice(Inds)
    try:
        x.append(get_random_neighbour_old(B, nd0))
    except ValueError as ve:
        print(ve)
        continue
print(f"t: {timeit.default_timer() - start}")
print("new")
start = timeit.default_timer()
for _ in range(10000):
    nd0 = random.choice(Inds)
    try:
        y.append(get_random_neighbour_new(B, nd0))
    except ValueError as ve:
        print(ve)
        continue
print(f"t: {timeit.default_timer() - start}")

# print(Counter(x))
# print(Counter(y))
from matplotlib import pyplot as plt

# fig, axs = plt.subplots(2, 1)

# X, Y = Counter(x), Counter(y)

# axs[0].bar(X.keys(), X.values())
# axs[1].bar(Y.keys(), Y.values())

Gnds = [n for n in B.nodes if B.nodes[n]['bipartite'] == 0]
plt.figure()
CC = Counter((len(B[g]) -1 for g in Gnds))
plt.bar(CC.keys(), CC.values())

plt.show()
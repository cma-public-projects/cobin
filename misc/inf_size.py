""" This script generates a plot of the pdf of the
multi-infection event size as it varies with group size
"""

import math
import random

def lognorm_obj(size, parameters):    
    from scipy import stats
    G = size - 1
    sigma = max(parameters['exponent'] * math.log(G), parameters['sigma'])
    exp_mu = max(math.log10(G), 1)
    return stats.lognorm(s=sigma, scale=exp_mu)

def explore(sizes, cmap='viridis', lims=(0, 7), p_exp=0.2, p_sig=0.1, scale='linear'):
    import numpy as np
    from matplotlib import pyplot as plt
    import matplotlib.cm as mplcm
    import matplotlib.colors as colors

    if scale == 'log':
        cNorm = colors.SymLogNorm(linthresh=1, base=10, vmin=sizes[0], vmax=sizes[-1])
    else:
        cNorm = colors.Normalize(vmin=sizes[0], vmax=sizes[-1])
    cm = plt.get_cmap(cmap)
    scalarMap = mplcm.ScalarMappable(norm=cNorm, cmap=cm)

    xs = np.linspace(*lims, num=10000)
    parameters = {"exponent": p_exp, "sigma": p_sig}
    for sz in sizes:
        lpdf = lognorm_obj(sz, parameters).pdf(xs)
        plt.plot(xs, lpdf, color=scalarMap.to_rgba(sz), alpha=0.5)
    clbr = plt.colorbar(scalarMap)
    plt.xlabel("Number of Infections per Event")
    plt.ylabel("Probability Distribution Function")
    plt.title("Distribution of N as a function of Group Size")
    clbr.set_label("Group Size", rotation=270)

if __name__ == "__main__":
    import numpy as np
    from matplotlib import pyplot as plt

    szs = np.linspace(2, 101, num=101)
    explore(szs, cmap='viridis', scale='log')
    plt.show()
#!/bin/bash -e

# Written 2022 Feb 14

PYVER="3.7"

echo "Installing into $1"
echo "Assuming Python version $PYVER"
mkdir -p "$1/lib/python$PYVER/site-packages"
CURR_DIR=$(pwd -P)
PARENT_DIR="${CURR_DIR%nesi}"
LNK_TARGET="$1/lib/python$PYVER/site-packages/networkcontagion"
if [ -L "$LNK_TARGET" ]; then
    echo "Existing version found, replacing..."
    rm "$LNK_TARGET"
fi
ln -s "$PARENT_DIR/networkcontagion" "$LNK_TARGET"
echo "Symlink installation complete"
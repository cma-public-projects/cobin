We provide three example networks for cobin: one large network represents the whole of Aotearoa New Zealand (about 5 million individuals), one medium subnetwork represents TA76 (Auckland, about 1.5 million individuals) , and one small subnetwork represents TA50 (South Wairarapa, about 10000 individuals). For these networks we have taken the following steps to anonymise the vaccination data: 
1. We have changed the dates of each vaccination by up to one week. 
2. For the large NZ network we have assigned vaccinations to individuals based on their TA (location) and age only. There is no ethnic or SA2-level location information in the assignment of vaccinations.
3. For the small and medium subnetworks we assigned vaccinations based on their age only. 

For the sake of storage we only provide the small TA50 network in the repo. The other networks can be accessed on Figshare at 
https://figshare.com/articles/dataset/Network_representations_of_populations_in_Aotearoa_NZ/23557506



""" Functions related to tracing """

from . import utils

do_nothing = utils.do_nothing

def byday_traces(data, df, output):
    """ Returns the number of new contact tracing events per day
    
    Args:
      data (dict): npz data as a dictionary
      df (pandas.DataFrame): npz data as a dataframe
      output (dict): temporary and final variables used (e.g. timeseries and config yamls) 
    """
    # if we have already found trace events get them else find them
    if '_ARE_TRACE_' not in output:
        are_trace_event(data, df, output)
    trace_events = output['_ARE_TRACE_']
    # turn events into a summable item and then count the number
    df = df.assign(trace_events=trace_events.astype(int))
    trace_nums = df.groupby('day')['trace_events'].agg(lambda x: sum(x, 0))
    # fill days without contact tracing events
    return utils.byday_reindex(trace_nums)

def peak_trace(data, df, output):
    """ Returns the peak number of traces in a day
    
    Args:
      data (dict): npz data as a dictionary
      df (pandas.DataFrame): npz data as a dataframe
      output (dict): temporary and final variables used (e.g. timeseries and config yamls) 
    """    
    # if byday_traces has been run get the timeseries of contact tracing events else compute it
    if 'Newly Self-Isolated Cases by Tracing' not in output:
        output['Newly Self-Isolated Cases by Tracing'] = byday_traces(data, df, output)
    # return maximum of the timeseries
    return output['Newly Self-Isolated Cases by Tracing'].max()

def time_peak_trace(data, df, output):
    """ Returns the day on which the peak number of trace events occurred
    
    Args:
      data (dict): npz data as a dictionary
      df (pandas.DataFrame): npz data as a dataframe
      output (dict): temporary and final variables used (e.g. timeseries and config yamls) 
    """    
    # if byday_traces has been run get the timeseries of contact tracing events else compute it
    if 'Newly Self-Isolated Cases by Tracing' not in output:
        output['Newly Self-Isolated Cases by Tracing'] = byday_traces(data, df, output)
    # if timeseries is not none return the day of the maximum of the timeseries
    if output['Newly Self-Isolated Cases by Tracing'].any():
        return output['Newly Self-Isolated Cases by Tracing'].argmax()
    else:
        return None

def total_trace(data, df, output):
    """ Returns the total number of contract tracing events
    
    Args:
      data (dict): npz data as a dictionary
      df (pandas.DataFrame): npz data as a dataframe
      output (dict): temporary and final variables used (e.g. timeseries and config yamls) 
    """    
    # if byday_traces has been run get the timeseres of contact tracing events else compute it
    if 'Newly Self-Isolated Cases by Tracing' not in output:
        output['Newly Self-Isolated Cases by Tracing'] = byday_traces(data, df, output)
    # return the total sum of events
    return output['Newly Self-Isolated Cases by Tracing'].sum()

def are_trace_event(data, df, output):
    """ Inplace adds a pandas.DataFrame of contact tracing events to output
    
    Args:
      data (dict): npz data as a dictionary
      df (pandas.DataFrame): npz data as a dataframe
      output (dict): temporary and final variables used (e.g. timeseries and config yamls) 
    """    
    output['_ARE_TRACE_'] = (~ df.efrom.isin(utils.selfiso_keys)) & df.eto.isin(utils.selfiso_keys)

def parse_trace_events(data, df, output):
    """ Parses the trace events into different values in output
    
    Args:
      data (dict): npz data as a dictionary
      df (pandas.DataFrame): npz data as a dataframe
      output (dict): temporary and final variables used (e.g. timeseries and config yamls) 
    """    
    # makes a dictionary of confirmation event times to indices in df
    if '_CONF_TMAP_' not in output:
        output['_CONF_TMAP_'] = df[(~df.efrom.isin(utils.known_keys)) & df.eto.isin(utils.known_keys)][['t', 'enode']].set_index('enode').t.to_dict()
    conf_time_map = output['_CONF_TMAP_']
    # if we have already found trace events get them else find them
    if '_ARE_TRACE_' not in output:
        are_trace_event(data, df, output)
    # maps times to trace events and gets delay between case confirmation and tracing contacts
    trace_events = df[output['_ARE_TRACE_']][['t', 'enode', 'anode', 'group']]
    trace_events = trace_events.assign(conf_time=trace_events.anode.apply(conf_time_map.get))
    output['_TRACE_EVENTS_'] = trace_events.assign(trace_time = trace_events.t - trace_events.conf_time)

def mean_time_to_trace(data, df, output):
    """ Returns the time at the end of the simulation
    
    Args:
      data (dict): npz data as a dictionary
      df (pandas.DataFrame): npz data as a dataframe
      output (dict): temporary and final variables used (e.g. timeseries and config yamls) 
    """
    # if parse_trace_events has not been run then run it
    if '_TRACE_EVENTS_' not in output:
        parse_trace_events(data, df, output)
    # return the mean time to trace delay
    trace_times = output['_TRACE_EVENTS_'].trace_time
    return trace_times.mean()

def byday_quarantined(data, df, output):
    """ Returns the max number of individuals in isolation per day
    
    Args:
      data (dict): npz data as a dictionary
      df (pandas.DataFrame): npz data as a dataframe
      output (dict): temporary and final variables used (e.g. timeseries and config yamls) 
    """    
    quarantined = df[utils.selfiso_keys].sum(axis=1)
    df = df.assign(quarantined=quarantined)
    qnum = df.groupby('day').quarantined.agg(max)
    return utils.byday_reindex(qnum)
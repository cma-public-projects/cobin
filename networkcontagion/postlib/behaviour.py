""" Functions related to overall behaviour """
import numpy as np
import pandas as pd
from collections import Counter
from functools import partial
from . import utils

do_nothing = utils.do_nothing

def time_end(data, df, outputs):
    """ Returns the time at the end of the simulation
    
    Args:
      data (dict): npz data as a dictionary
      df (pandas.DataFrame): npz data as a dataframe
      outputs (dict): temporary and final variables used (e.g. timeseries and config yamls) 
    """
    return data['t'][-1]

def total_cases(data, df, outputs):
    """ Returns the total number of cases
    
    Args:
      data (dict): npz data as a dictionary
      df (pandas.DataFrame): npz data as a dataframe
      outputs (dict): temporary and final variables used (e.g. timeseries and config yamls) 
    """
    return df[list(utils.cumulative_keys)].iloc[-1].sum()

def total_deaths(data, df, outputs):
    """ Returns the total number of deaths
    
    Args:
      data (dict): npz data as a dictionary
      df (pandas.DataFrame): npz data as a dataframe
      outputs (dict): temporary and final variables used (e.g. timeseries and config yamls) 
    """
    return df[list(utils.dead_keys)].iloc[-1].sum()

def total_hospitalisations(data, df, outputs):
    """ Returns the total number of hospitalisations
    
    Args:
      data (dict): npz data as a dictionary
      df (pandas.DataFrame): npz data as a dataframe
      outputs (dict): temporary and final variables used (e.g. timeseries and config yamls) 
    """
    return df['eto'].eq('HQ').sum()

def total_icu(data, df, outputs):
    return df['eto'].eq('CQ').sum()

### TIMESERIES FUNCTIONS ###

# Typical pattern: 
#   1. extract the appropriate events
#   2. group by day and count/maximise
#   3. reindex to replace zero event days

def byday_new_infections(data, df, outputs):
    """ Returns a dataframe/timeseries of the number of new infections per day
    
    Args:
      data (dict): npz data as a dictionary
      df (pandas.DataFrame): npz data as a dataframe
      outputs (dict): temporary and final variables used (e.g. timeseries and config yamls) 
    """
    # an infection is moving from a safe key to a nonsafe key
    new_infections = df[df.efrom.isin(utils.safe_keys) & ~df.eto.isin(utils.safe_keys)]
    # group and count new infections by day
    numinf_byday = new_infections.groupby('day')['t'].count().rename('numinf')
    # fill in days without new infections
    return utils.byday_reindex(numinf_byday)

def byday_new_reseeds(data, df, outputs):
    """ Returns a dataframe/timeseries of the number of new reseeds per day
    
    Args:
      data (dict): npz data as a dictionary
      df (pandas.DataFrame): npz data as a dataframe
      outputs (dict): temporary and final variables used (e.g. timeseries and config yamls) 
    """
    # a reseed is moving from a safe key to a nonsafe key where the group is empty
    new_reseeds = df[(df.efrom.isin(utils.safe_keys) & ~df.eto.isin(utils.safe_keys)) & (df.group.values=='')]
    # group and count new reseeds by day
    numreseed_byday = new_reseeds.groupby('day')['t'].count().rename('numreseed')
    # fill in days without new reseeds
    return utils.byday_reindex(numreseed_byday)

def byday_new_conf(data, df, outputs):
    """ Returns a dataframe/timeseries of the number of new confirmed cases per day
    
    Args:
      data (dict): npz data as a dictionary
      df (pandas.DataFrame): npz data as a dataframe
      outputs (dict): temporary and final variables used (e.g. timeseries and config yamls) 
    """    
    # a confirmed cases is an infection is moving from an unconfirmed key to a confirmed key
    new_conf = df[~df.efrom.isin(utils.known_keys) & df.eto.isin(utils.known_keys)]
    # group and count new infections by day
    numconf_byday = new_conf.groupby('day')['t'].count().rename('numknown')
    # fill in days without new infections
    return utils.byday_reindex(numconf_byday)

def byday_new_hosp(data, df, outputs):
    """ Returns a dataframe/timeseries of the number of new hospitalisations per day
    
    Args:
      data (dict): npz data as a dictionary
      df (pandas.DataFrame): npz data as a dataframe
      outputs (dict): temporary and final variables used (e.g. timeseries and config yamls) 
    """
    # a hospitalisation is where an individual moves into HQ    
    new_hosp = df[df.eto.isin(['HQ'])]
    # group and count new hospitalisations by day
    numhosp_byday = new_hosp.groupby('day')['t'].count().rename('numhosp')
    # fill in days without new hospitalisations
    return utils.byday_reindex(numhosp_byday)

def byday_new_crit(data, df, outputs):
    """ Returns a dataframe/timeseries of the number of new ICU patients per day
    
    Args:
      data (dict): npz data as a dictionary
      df (pandas.DataFrame): npz data as a dataframe
      outputs (dict): temporary and final variables used (e.g. timeseries and config yamls) 
    """
    # a critical care patient is where an individual moves into CQ
    new_crit = df[df.eto.isin(['CQ'])]
    # group and count new critical care by day
    numcrit_byday = new_crit.groupby('day')['t'].count().rename('numcrit')
    # fill in days without new critical care
    return utils.byday_reindex(numcrit_byday)

def byday_new_dead(data, df, outputs):
    """ Returns a dataframe/timeseries of the number of new fatalities per day
    
    Args:
      data (dict): npz data as a dictionary
      df (pandas.DataFrame): npz data as a dataframe
      outputs (dict): temporary and final variables used (e.g. timeseries and config yamls) 
    """
    # a fatality is where an individual moves into DX
    new_dead = df[df.eto.isin(['DX'])]
    # group and count new fatalities by day
    numdead_byday = new_dead.groupby('day')['t'].count().rename('numdead')
    # fill in days without new fatalities
    return utils.byday_reindex(numdead_byday)

def byday_new_contact_trace(data, df, outputs):
    """ Returns a dataframe/timeseries of the number of new contact traces per day
    
    Args:
      data (dict): npz data as a dictionary
      df (pandas.DataFrame): npz data as a dataframe
      outputs (dict): temporary and final variables used (e.g. timeseries and config yamls) 
    """    
    # a trace is moving from a non traced key to a traced key
    new_contact_trace = df[~df.efrom.isin(utils.traced_keys) & df.eto.isin(utils.traced_keys)]
    # group and count new contact traces by day
    numtrace_byday = new_contact_trace.groupby('day')['t'].count().rename('numtrace')
    # fill in days without new contact traces
    return utils.byday_reindex(numtrace_byday)

def byday_active_infections(data, df, outputs):
    """ Returns a dataframe/timeseries of the number of active infections per day
    
    Args:
      data (dict): npz data as a dictionary
      df (pandas.DataFrame): npz data as a dataframe
      outputs (dict): temporary and final variables used (e.g. timeseries and config yamls) 
    """
    # active infection is an individual who has the contagion
    df = df.assign(active_inf = df[list(utils.active_keys)].sum(axis=1))
    # group and count active infections by day
    df_active_end_of_day = df.groupby('day').active_inf.agg(lambda x: x.iloc[-1])
    # fill in days without active infections
    return utils.byday_reindex(df_active_end_of_day, method='ffill')

def byday_confirmed_cases(data, df, outputs):
    """ Returns a dataframe/timeseries of the number of new confirmed cases per day
    
    Args:
      data (dict): npz data as a dictionary
      df (pandas.DataFrame): npz data as a dataframe
      outputs (dict): temporary and final variables used (e.g. timeseries and config yamls) 
    """    
    # confirmed infections are individuals who are confirmed to have the contagion
    df = df.assign(conf_cases = df[list(utils.confirmed_keys)].sum(axis=1))
    # group and count the new confirmed cases per day
    conf_byday = df.groupby('day')['conf_cases'].agg(lambda x: x.iloc[-1])
    # fill in days without new confirmed cases
    return utils.byday_reindex(conf_byday, method='ffill')

def byday_isolated_cases(data, df, outputs):
    """ Returns a dataframe/timeseries of the number of new isolations per day
    
    Args:
      data (dict): npz data as a dictionary
      df (pandas.DataFrame): npz data as a dataframe
      outputs (dict): temporary and final variables used (e.g. timeseries and config yamls) 
    """    
    # isolated individuals are those in quarantine (at home or at a facility)
    df = df.assign(selfisos = df[list(utils.selfiso_keys)].sum(axis=1))
    # group and count new isolations per day
    selfiso_byday = df.groupby('day')['selfisos'].agg(lambda x: x.iloc[-1])
    # fill in days with no new isolations
    return utils.byday_reindex(selfiso_byday, method='ffill')

def byday_current_hospitalised(data, df, outputs):
    """ Returns a dataframe/timeseries of the number of current hospitalisations per day
    
    Args:
      data (dict): npz data as a dictionary
      df (pandas.DataFrame): npz data as a dataframe
      outputs (dict): temporary and final variables used (e.g. timeseries and config yamls) 
    """    
    # hospitalised individuals are in state HQ
    hq_nums = df.groupby('day').HQ.agg(lambda x: x.iloc[-1])
    # fill in days with no hospitalisations
    return utils.byday_reindex(hq_nums, method='ffill')

def byday_current_icu(data, df, outputs):
    """ Returns a dataframe/timeseries of the number of current ICU patients per day
    
    Args:
      data (dict): npz data as a dictionary
      df (pandas.DataFrame): npz data as a dataframe
      outputs (dict): temporary and final variables used (e.g. timeseries and config yamls) 
    """
    # ICU patients are in state CQ
    cq_nums = df.groupby('day').CQ.agg(lambda x: x.iloc[-1])
    # fill in days with no ICU patients
    return utils.byday_reindex(cq_nums, method='ffill')

def byday_cumulative_infections(data, df, outputs):
    """ Returns a dataframe/timeseries of the number of cumulative infections per day
    
    Args:
      data (dict): npz data as a dictionary
      df (pandas.DataFrame): npz data as a dataframe
      outputs (dict): temporary and final variables used (e.g. timeseries and config yamls) 
    """    
    # an infection is moving from a safe key to a nonsafe key
    df = df.assign(cum_inf = df[list(utils.cumulative_keys)].sum(axis=1))
    # group by day and get cumulative count
    cum_byday = df.groupby('day')['cum_inf'].agg(lambda x: x.iloc[-1])
    # fill in days with no cumulative infections
    return utils.byday_reindex(cum_byday, method='ffill')

def byday_cumulative_reseeds(data, df, outputs):
    """ Returns a dataframe/timeseries of the number of cumulative reseeds per day
    
    Args:
      data (dict): npz data as a dictionary
      df (pandas.DataFrame): npz data as a dataframe
      outputs (dict): temporary and final variables used (e.g. timeseries and config yamls) 
    """        
    # a reseed is moving from a safe key to a nonsafe key where the group is empty
    new_reseeds = df[(df.efrom.isin(utils.safe_keys) & ~df.eto.isin(utils.safe_keys)) & (df.group.values=='')]
    # group and count new reseeds by day
    numreseed_byday = new_reseeds.groupby('day')['t'].count().rename('numreseed')
    # fill in days without new reseeds and get cumulative count
    return utils.byday_reindex(numreseed_byday).cumsum()

def byday_cumul_conf_cases(data, df, outputs):
    """ Returns a dataframe/timeseries of the number of cumulative confirmed cases per day
    
    Args:
      data (dict): npz data as a dictionary
      df (pandas.DataFrame): npz data as a dataframe
      outputs (dict): temporary and final variables used (e.g. timeseries and config yamls) 
    """            
    # confirmed cases are the number of known cases
    cumul_conf_sum = df[list(utils.known_keys)].sum(axis=1)
    df = df.assign(cumul_conf=cumul_conf_sum)
    # group by day and get cumulative count
    cumul_conf_byday = df.groupby('day').cumul_conf.agg(lambda x: x.iloc[-1])
    # fill in days without confirmed cases
    return utils.byday_reindex(cumul_conf_byday, method='ffill')

def byday_cumul_hosp(data, df, outputs):
    """ Returns a dataframe/timeseries of the number of cumulative hospitalisations per day
    
    Args:
      data (dict): npz data as a dictionary
      df (pandas.DataFrame): npz data as a dataframe
      outputs (dict): temporary and final variables used (e.g. timeseries and config yamls) 
    """            
    # cumulative hospitalised cases are in ICQ, HCQ, RT, or DX
    cumul_hosp_sum = df[list(utils.cumul_hosp_keys)].sum(axis=1)
    df = df.assign(cumul_hosp=cumul_hosp_sum)
    # group by day and get cumulative count
    cumul_hosp_byday = df.groupby('day').cumul_hosp.agg(lambda x: x.iloc[-1])
    # fill in days without hospitalisations
    return utils.byday_reindex(cumul_hosp_byday, method='ffill')

def byday_cumul_crit(data, df, outputs):
    """ Returns a dataframe/timeseries of the number of cumulative ICU patients per day
    
    Args:
      data (dict): npz data as a dictionary
      df (pandas.DataFrame): npz data as a dataframe
      outputs (dict): temporary and final variables used (e.g. timeseries and config yamls) 
    """            
    # cumulative CQ, RT, DX
    cumul_crit_sum = df[list(utils.cumul_crit_keys)].sum(axis=1)
    df = df.assign(cumul_crit=cumul_crit_sum)
    # group by day and get cumulative count
    cumul_crit_byday = df.groupby('day').cumul_crit.agg(lambda x: x.iloc[-1])
    # fill in days without critical patients
    return utils.byday_reindex(cumul_crit_byday, method='ffill')

def byday_cumul_dead(data, df, outputs):
    """ Returns a dataframe/timeseries of the number of cumulative fatalities per day
    
    Args:
      data (dict): npz data as a dictionary
      df (pandas.DataFrame): npz data as a dataframe
      outputs (dict): temporary and final variables used (e.g. timeseries and config yamls) 
    """            
    # get cumulative number of DX individuals
    cumul_dead_byday = df.groupby('day')['DX'].agg(lambda x: x.iloc[-1])
    # fill in days without fatalities
    return utils.byday_reindex(cumul_dead_byday, method='ffill')

def byday_inf_att(data, df, outputs, att_name, series):
    """ Inplace: changes the timeseries in the outputs dictionary to be aggregated by a certain attribute
    
    Args:
      data (dict): npz data as a dictionary
      df (pandas.DataFrame): npz data as a dataframe
      outputs (dict): temporary and final variables used (e.g. timeseries and config yamls) 
      att_name (str): attribute to group by (currently 'group' is only one supported)
      series: timeseries to aggregate over
    """
    # an infection is moving from a safe key to a nonsafe key      
    new_infections = df[df.efrom.isin(utils.safe_keys) & ~df.eto.isin(utils.safe_keys)]
    if att_name == 'group':
        # group by group type (e.g. DW, DZ, SC, ...)
        atts = new_infections.groupby('day')[att_name].aggregate(lambda s: Counter([x[:2] for x in s]))
    else:    
        atts = new_infections.groupby('day')[att_name].aggregate(lambda s: Counter(s))
    att_series = atts.apply(pd.Series)
    # change entry in outputs dict to have aggregation applied
    outputs[series] = att_series.fillna(0).cumsum()

def byday_attlike(data, df, outputs, att_name, series, att, type='Inf'):
    """ Function that uses byday_xxx_att functions and returns aggregated timeseries for certain att
    
    Args:
      data (dict): npz data as a dictionary
      df (pandas.DataFrame): npz data as a dataframe
      outputs (dict): temporary and final variables used (e.g. timeseries and config yamls) 
      att_name (str): attribute to group by (currently 'group' is only one supported)
      series: name of aggregated timeseries
      att (str): level of aggregated variable to return the timeseries of (e.g. DW for att_name=group)
      type (str): Type of event to aggregate over
    """
    if series not in outputs:
        if type=='Inf':
            byday_inf_att(data, df, outputs, att_name, series)
        else:    
            return pd.Series()
    att_series = outputs[series]
    if isinstance(att_series, pd.Series):
        # if no infections, return empty series
        return pd.Series()
    elif att in att_series:
        return utils.byday_reindex(att_series[att])
    else:
        fake_series = att_series.iloc[:,0].copy(deep=True).rename(att)
        fake_series.values[:] = 0
        return utils.byday_reindex(fake_series)

infection_groups = ['DW', 'DZ', 'DG', 'WP', 'WE', 'WZ', 'WG', 'SC', 'SG', 'SZ', 'PM', 'PG', 'PZ', 'EX', 'EV']
srs = '_GRP_TYPE_INF_'
byday_infection_groups = {f'Active Infections through {grp}': partial(byday_attlike, att_name = 'group', series = srs, att=grp) for grp in infection_groups}

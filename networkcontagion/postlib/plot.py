""" Functions related to plotting """

from matplotlib import pyplot as plt

_plot_styles_ = {
    'run': {'color': 'k', 'linestyle': '-', 'alpha': 0.1,},
    'med': {'color': '#377EB8', 'linestyle': '-',},
    'lq': {'color': '#4DAF4A', 'linestyle': '--',},
    'uq': {'color': '#E41A1C', 'linestyle': '--',},
}

_DEFAULT_DPI_ = 360

def plot_with_quantile(df, outname_root = None, plot_type = 'pdf', plot_suffix='plot_with_quantile', xlabel=None, ylabel=None, dpi=_DEFAULT_DPI_, **kwargs):
    """ Plots timeseries for each npz as well as pointwise median and IQR
        
    Args:
      df (pandas.DataFrame): dataframe containing timeseries to plot
      outname_root (str): path to root directory of output
      plot_suffix (str): prefix string describing what the plot is
      xlabel (str): label for the xaxis
      ylabel (str): label for the yaxis
      dpi (int): number of DPI to use when plotting
    """
    # Get timeseries list
    ts_list = df.index.unique('metric').tolist()

   # For each timeseries metric
    for i, ts in enumerate(ts_list):
        # Subset df for this metric
        df_ts = df.xs(ts, axis =0, level=1, drop_level=True)
        
        # Compute median, LQ, UQ
        med = df_ts.median()
        lq = df_ts.quantile(0.25)
        uq = df_ts.quantile(0.75)
        
        # Plot
        fig = plt.figure(dpi=dpi)
        ax = fig.add_subplot()
        
        df_ts.T.plot(**_plot_styles_['run'], legend=False, ax=ax, **kwargs)
        med.plot(**_plot_styles_['med'], ax=ax, **kwargs, label = 'median')
        lq.plot(**_plot_styles_['lq'], ax=ax, **kwargs, label = 'LQ')
        uq.plot(**_plot_styles_['uq'], ax=ax, **kwargs, label = 'UQ')
        
        plt.title(ts)
        plt.xlabel(xlabel)
        plt.ylabel(ylabel)
        plt.legend(bbox_to_anchor =(1, 0.5), ncol = 1, loc='center left')

        plt.xticks(fontsize="small")
        plt.yticks(fontsize="small")

        if outname_root is not None:
            plt.savefig((outname_root + plot_suffix + "_" + ts + "." + plot_type), dpi='figure', pad_inches=0.5,  bbox_inches='tight')
            plt.close(fig)
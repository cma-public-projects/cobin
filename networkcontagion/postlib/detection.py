""" Functions related to detection """

from . import utils
from . import parse_yaml
from functools import partial

do_nothing = utils.do_nothing

def get_detects(data, df, output):
    """ Returns the detection events as a pd.Series
    
    Args:
      data (dict): npz data as a dictionary
      df (pandas.DataFrame): npz data as a dataframe
      output (dict): temporary and final variables used (e.g. timeseries and config yamls) 
    """
    # if we have already got the detection events return the series
    if '_DETECTS_' in output:
        detects = output['_DETECTS_']
    # else find and return the detection events
    else:
        detects = df.eto.isin(utils.known_keys)
        output['_DETECTS_'] = detects

    return detects

def generic_sum_cases(data, df, output, keys):
    """ Generic function for summing all cases 

    Args:
      data (dict): npz data as a dictionary
      df (pandas.DataFrame): npz data as a dataframe
      output (dict): temporary and final variables used (e.g. timeseries and config yamls) 
      keys (set): states to sum over (e.g. CQ, UQ)
    """
    # get detection events
    detects = get_detects(data, df, output)
    # sum events by inputted keys if they exist else return none
    if not any(detects):
        return None
    return df[detects].iloc[0][list(keys)].sum()

def time_of(data, df, output):
    """ Returns the time of first detection
    
    Args:
      data (dict): npz data as a dictionary
      df (pandas.DataFrame): npz data as a dataframe
      output (dict): temporary and final variables used (e.g. timeseries and config yamls) 
    """    
    # get detection events
    detects = get_detects(data, df, output)
    # return time of first detection if there are detection events else return none
    if not any(detects):
        return None
    return df.t[detects].iloc[0]

# Returns the active cases at first detection
active_cases = partial(generic_sum_cases, keys=utils.active_keys)

# Returns the cumulative cases at first detection
cumulative_cases = partial(generic_sum_cases, keys=utils.cumulative_keys)
""" Parses the yaml config file for simulation runs """

from networkcontagion.lib import config_loader

def read_config(config_name, outputs):
    """ Adds the loaded config YAML if not in the outputs dictionary
    
    Args:
      config_name (str): Path to config YAML
      outputs (dict): temporary and final variables used (e.g. timeseries and config yamls)
    """
    if '_SIM_CONFIG_' not in outputs:
        outputs['_SIM_CONFIG_'] = config_loader.load(config_name)

def fixed_delay(outputs):
    """ Returns the delay between detection and activation of lockdown"""
    return outputs['_SIM_CONFIG_']['parameters']['lockdown_timeline']

def adap_delay(outputs):
    """ Returns the delay between detection and activation of alert level """
    return outputs['_SIM_CONFIG_']['parameters']['adaptive_lockdown']
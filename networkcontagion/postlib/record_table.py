import pandas as pd
import os.path
import re

def read_csv(filename):
    " Reads csv into a RecordTable and returns the RecordTable"
    rt = RecordTable()
    rt.read_csv(filename)
    return rt


class RecordTable(object):
    """ A class the retrieves a row from a table using the 'run' key"""

    def __init__(self):
        """ Initialise an empty RecordTable """
        self.df = pd.DataFrame()

    def read_csv(self, filename):
        """ Read a csv into df (a pandas.DataFrame) """
        if filename and os.path.exists(filename):
            # read the csv file and store the result as a data frame
            self.df = pd.read_csv(filename)

    def get_all_record_keys(self):
        """ Returns all the unique run values"""
        # retrieve all the 'run' values
        return self.df.get('run', []).unique()

    def get_record(self, key):
        """ Gets the first entry that matches the key
        
        Args:
          key (str): 'run' ID value to get entry of
        """
        # retrieve the first row matching the run key
        if 'run' in self.df:
            res = self.df[self.df['run'] == key].to_dict('records')
            if res:
                return res[0]
        return {}

    def get_timerecords(self, key, metric_name):
        """ Gets the first entry that matches the key and then the timeseries metric desired
        
        Args:
          key (str): 'run' ID value to get entry of
          metric_name (str): name of the timeseries metric to return
        """
        # retrieve the row matching the run key and the metric name and return a time series
        if ('run' in self.df) and ('metric' in self.df):
            rcdt = self.df[(self.df['run'] == key) & (self.df['metric'] == metric_name)].to_dict('records')
            if rcdt:
                rcdt = rcdt[0]
                timeseries_name = rcdt['metric']
                del rcdt['metric']
                del rcdt['run']
                try: 
                    del rcdt['Unnamed: 0']
                except:
                    pass
                srs = {int(k): v for k, v in rcdt.items()}
                srs = pd.Series(srs, index=srs.keys(), name = re.sub(r' ', '_', timeseries_name.lower()))
                srs.dropna(inplace=True)
                srs.index.name = 'day'
                # float -> int
                srs = pd.to_numeric(srs, downcast='integer')
                return srs
        return None
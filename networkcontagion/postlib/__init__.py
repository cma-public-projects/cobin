"""Postprocessing library for the cobin simulations

Contains an automated way for parsing a directory of .npz files into csv 
ouputs.

Major sublibraries
------------------
metrics
-------
    A mapping between metrics with human-readable names and the functions that
    compute them from the standard structure of (NpzFile, DataFrame, dict)
    
utils
-----
    Utilities and common definitions
    
processing
----------
    File, directory and command-line argument parsing
"""

__all__ = ['metrics', 'utils', 'processing']

from . import *
""" Functions related to metadata """

import os
import pathlib

def get_name(filepath):
    """ Returns the name of the file at a given filepath """
    return pathlib.Path(filepath).name

def clean_dir_path(dirpath):
    """ Returns a string that can be used as a valid output file name affix """
    return dirpath.strip(".").replace(os.sep, "_").strip("_")
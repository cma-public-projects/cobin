from scipy import stats
import numbers

class RandomFloat(numbers.Number):
    """ A random number that acts like a float, but can be redrawn from the underlying distribution
    
    Attributes:
        _dist: scipy.stats random variable distribution
        _value: current realisation of the random variable
        _name: name of the distribution (for repr purposes)

    Example:
        ```RandomFloat(stats.norm(loc=1.0, scale=0.3), name='norm')``` generates a random variable distributed as :math:`\\mathcal{N}(1, 0.09)`
    """
    __slots__ = ['_dist', '_value', '_name']

    def __init__(self, dist, name=None):
        if not hasattr(dist, 'rvs') and callable(dist.rvs):
            raise TypeError("Distribution must generate random variates via an rvs() method")
        self._dist = dist
        self._value = dist.rvs()
        if name is None:
            name = "Arbitrary Random Distribution"
        self._name = name

    def redraw(self):
        """Redraw the realisation value from the underlying distribution"""
        self._value = self._dist.rvs()

    def __add__(self, other):
        return float(self._value + other)

    def __radd__(self, other):
        return float(other + self._value)

    def __sub__(self, other):
        return float(self._value - other)

    def __rsub__(self, other):
        return float(other - self._value)

    def __mul__(self, other):
        return float(self._value * other)

    def __rmul__(self, other):
        return float(other * self._value)

    def __div__(self, other):
        return self.__truediv__(other)

    def __truediv__(self, other):
        return float(self._value / other)

    def __floordiv__(self, other):
        return int(self._value // other)

    def __rdiv__(self, other):
        return float(other / self._value)

    def __pow__(self, other):
        return float(self._value ** other)

    def __rpow__(self, other):
        return float(other ** self._value)

    def __eq__(self, other):
        return self._value == float(other)

    def __lt__(self, other):
        return self._value < float(other)

    def __gt__(self, other):
        return self._value > float(other)

    def __le__(self, other):
        return self._value <= float(other)

    def __ge__(self, other):
        return self._value >= float(other)

    def __str__(self):
        xstr = self._name
        if hasattr(self._dist, 'args'):
            xstr += str(self._dist.args)
        return xstr + ": " + str(self._value)

    def __repr__(self):
        return str(self)

    def __int__(self):
        return int(self._value)

    def __float__(self):
        return float(self._value)

random_tag = u'!random'

def construct_random_float(loader, node):
    dist_type, *info = loader.construct_sequence(node)
    dist = getattr(stats, dist_type)(*info)
    return RandomFloat(dist, name=dist_type)

def represent_random_float(representer, node):
    return representer.represent_sequence(random_tag, [node._name, *node._dist.args],
                                          flow_style=True)

def represent_frozen_random_float(representer, node):
    return representer.represent_float(node._value)
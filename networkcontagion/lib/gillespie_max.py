import random
from collections import OrderedDict, Counter
from enum import Enum
from numpy import savez_compressed as np_savez_compressed
from sortedcontainers import SortedList
from networkcontagion.lib.ratedict import ListDict
from warnings import warn

class ParameterMissingException(Exception):
    pass

def filter_parameters(simulation_module, parameters):
    """ Filters and sorts input parameters for a given simulation class

    Args:
      simulation_module : module or object
          Contains a _parameters field that is a dictionary of parameters
          and their descriptions
    parameters : dict-like
        Parameters to filter and sort

    Returns:
        filtered_parameters : OrderedDict
            Filtered and sorted parameter list

    Notes:
        Not in-place - returns a sorted dictionary (OrderedDict)
    """

    acceptable = simulation_module._parameters

    filtered_parameters = OrderedDict.fromkeys(acceptable)
    for par in acceptable:
        if par not in parameters:
            raise ParameterMissingException(f"Parameter {par} missing ({acceptable[par]})")
        else:
            filtered_parameters[par] = parameters[par]

    return filtered_parameters

def return_zero(*args, **kwargs):
    """Returns zero for early exit"""
    return 0

def new_queue():
    """Instantiates an empty queue that is compatible with the simulation algorithm"""
    return SortedList()

class BaseEvent(Enum):
    """Enumeration class that defines an ordering over the elements"""
    def __lt__(self, other):
        if isinstance(other, BaseEvent):
            return self.value < other.value
        else:
            raise TypeError(f"{other} is not a valid event type (not derived from BaseEvent)")

class NoEvent(BaseEvent, Enum):
    """A shadow event (no reaction) enum. Used as a sentinel object."""
    no_event = 0

class ContagionRecords():
    """Records of contagion history. 
    
    Each attribute is a list of values, with one element in each list for each event.
    An empty string is used as an element if the associated field/attribute is not applicable.
    
    Attributes:
        t : time of event
        enode: node of event that changes state, primary node
        anode: node(s) that are involved but do not change state, auxiliary nodes
        group: group/context through which event occurs
        efrom: primary node's initial state (before event)
        eto: primary node's final state (after event)
        astatus: states of the auxiliary nodes
        txncode: string representation of the states of the nodes involved in the event
    """
    __slots__ = ('t', 'enode', 'anode', 'group', 'efrom', 'eto', 'astatus', 'txncode', 'states')

    def __init__(self, return_statuses):
        """Initalise an empty contagion record
        
        Args:
            return_statuses (list): List of states to record a count of at each event
        """
        self.t = []
        self.enode = []
        self.anode = []
        self.group = []
        self.efrom = []
        self.eto = []
        self.astatus = []
        self.txncode = []
        # need to init this way to get distinct lists
        self.states = {state: [] for state in return_statuses}

    def set_initial_condition(self, t0, status):
        """ Sets the initial record in the record table

        Args:
            t0 (float) : Initial time
            status (dict) : Initial states (node (str) -> state (str))

        Constructs the first entry in each of the slots. By default, empty strings are used in
        place of Nones.
        """
        self.t.append(t0)
        # append in empty strings for initial condition
        self.enode.append('')
        self.anode.append('')
        self.group.append('')
        self.efrom.append('')
        self.eto.append('')
        self.astatus.append('')
        self.txncode.append('')
        status_counter = Counter(status.values())
        for state, state_record in self.states.items():
            # Counters have a default value of 0
            state_record.append(status_counter[state])

    def add(self, t, enode='', anode='', group='', efrom='', eto='', astatus=''):
        """ Add a record of an event to the record table

        Args:
            t (float) : Current time
            enode (str) : Node that is changing state in this event
            anode (str) : Other nodes that affect the event
            group (str) : Group that event occurs in/through (if applicable)
            efrom (str) : State of node prior to change/event
            eto (str) : State of node after change/event
            astatus (str) : status of other nodes that affect the event

        Also generates a txncode that represents the event category
        """
        # compute txn code
        pre_state = ",".join(filter(None, [efrom, astatus]))
        post_state = ",".join(filter(None, [eto, astatus]))
        txncode = f"({pre_state}) -> ({post_state})"

        self.t.append(t)
        self.enode.append(enode)
        self.anode.append(anode)
        self.group.append(group)
        self.efrom.append(efrom)
        self.eto.append(eto)
        self.astatus.append(astatus)
        self.txncode.append(txncode)
        for state_record in self.states.values():
            state_record.append(state_record[-1])
        self.states.get(efrom, [0])[-1] -= 1
        self.states.get(eto, [0])[-1] += 1

    def to_npz(self, filename, full_return=True):
        """ Output the record table to npz format

        Args:
            filename (str) : Path to file to output record table into (overwrites)
            full_return (bool) : if True, returns all information; if False, only returns time and state counts
        """
        if not full_return:
            np_savez_compressed(filename, t=self.t, **self.states)
        else:
            np_savez_compressed(filename,
                                t=self.t,
                                enode=self.enode,
                                anode=self.anode,
                                group=self.group,
                                efrom=self.efrom,
                                eto=self.eto,
                                astatus=self.astatus,
                                txncode=self.txncode,
                                **self.states)

def gillespie_max(G, rate_function, transition_choice, manage_event, IC, return_statuses, tmin=0, tmax=100, max_infections=float('Inf'), parameters=None, count_infections=None, event_queue=None, sim_objects=None):
    """ Simulates a contagion process on a network, with given model functions

    Args:
        G (nx.Graph) : Network on which the simulation runs on
        rate_function (Callable) : Gives the rate at which an event occurs for a given node
        transition_choice (Callable) : Gives the state transition (if any), given that an event is
                                       occurring to a given node
        manage_event (Callable) : Gives the changes to the graph and any nodes that have a change
                                  in rate, given that an given event has occurred
        IC (dict) : Dictionary {node (str) -> state (str)} of initial states
        return_statuses (list) : States to record and return counts of with respect to time
        tmin (float) : Initial (starting) time of the simulation
        tmax (float) : Final time of the simulation (if it has not finished by then)
        max_infections (float) : Maximum number of infections to simulate (terminates immediately after)
        parameters (dict) : Ordered dictionary of parameters that are passed to every function
        count_infections (Callable) : Gives the number of infections active in the simulation
        event_queue (SortedList) : The event queue at the start of the simulation
        sim_objects (dict) : Dictionary of dynamic simulation objects to be passed to every function

    Notes:
        Common argument descriptions:
            * G - the NetworkX Graph
            * node - name of the node
            * t - current time
            * status - dictionary node names to current status
            * event_queue - list of events that are scheduled to occur later
            * parameters - dictionary of parameters
            * sim_objects - dictionary of sim_objects

        rate_function(G, node, status, parameters, sim_objects)
            Should return a non-negative value.

        transition_choice(G, node, t, status, parameters, sim_objects)
            Can either return a single state, or a tuple of a state and a txn_info.

        manage_event(G, t, event_type, event_info, event_queue, parameters, sim_objects)
            event_type - an Enum value for a specified Event type
            event_info - auxiliary info about the event (e.g. a string describing type of event)

        count_infections(records, parameters, sim_objects)
            records - the data structure that retains the simulation history
    """
    if parameters is None:
        parameters = {}
    if sim_objects is None:
        sim_objects = {}

    # Deepcopy initialisation of status dict from IC
    status = {node: IC[node] for node in G.nodes()}

    # Initialise simulation records
    t = tmin
    records = ContagionRecords(return_statuses)
    records.set_initial_condition(t, status)

    # Initialise rate dict
    nodes_by_rate = ListDict()
    for nd in G.nodes:
        rate = rate_function(G, nd, status, parameters, sim_objects)
        if rate > 0:
            nodes_by_rate.insert(nd, weight=rate, cast=float)

    # Initialise event queue
    if event_queue is None:
        event_queue = new_queue()
    assert isinstance(event_queue, SortedList), "event_queue is not a SortedList"

    # Count infections
    if count_infections is None:
        count_infections = return_zero
    n_infections = count_infections(records, parameters, sim_objects)

    # Main direct Gillespie loop
    while (nodes_by_rate.is_active() or len(event_queue)) and n_infections < max_infections:

        # event queue handling
        if nodes_by_rate.total_weight > 0:
            cand_delay = random.expovariate(nodes_by_rate.total_weight)
        else:
            cand_delay = float('Inf')
        while len(event_queue) > 0 and event_queue[0][0] < (t + cand_delay):
            t_cand, event_type, *event_info = event_queue.pop(0)
            if t_cand < t:
                warn(f"Event Queue produced event in the past ({t_cand}) < ({t}")
                continue
            t = t_cand
            if t > tmax:
                break
            events, influence_set = manage_event(G, t, event_type, event_info, status, event_queue, parameters, sim_objects)
            for event in events:
                records.add(**event)
            for nbr in influence_set:
                weight = rate_function(G, nbr, status, parameters, sim_objects)
                nodes_by_rate.insert(nbr, weight=weight, cast=float)
            if nodes_by_rate.total_weight > 0:
                cand_delay = random.expovariate(nodes_by_rate.total_weight)
            else:
                cand_delay = float('Inf')

        t += cand_delay
        
        if t >= tmax:
            break

        # Determine type of random event
        node = nodes_by_rate.choose_random()
        event_type, *aux_info = transition_choice(G, node, t, status, parameters, sim_objects)
        # random events always have a node associated with them
        event_info = [node, *aux_info]

        # If this is an actual reaction
        if event_type is not NoEvent.no_event:
            # Determine nodes that are changed or influenced by this event
            events, influence_set = manage_event(G, t, event_type, event_info, status, event_queue, parameters, sim_objects)
            # Record any state changes of nodes
            for event in events:
                records.add(**event)
            # Update rates of any influenced nodes
            for nbr in influence_set:
                weight = rate_function(G, nbr, status, parameters, sim_objects)
                nodes_by_rate.insert(nbr, weight=weight, cast=float)

        # update and check the max_infections limit
        n_infections = count_infections(records, parameters, sim_objects)
        if n_infections >= max_infections:
            print(f"Hit {max_infections} infections at time {t}")

    return records, status

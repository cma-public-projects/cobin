""" Initial condition builder for cobin """

import random
from typing import Iterable, Mapping, Union

def is_valid(candidate : str, status : Mapping[str, str], allowed : Union[Iterable[str], None]) -> bool:
    """Checks if a candidate's status is in a list of allowed states
    
    Parameters
    ----------
    candidate: key of candidate to look up in status
    status: mapping from candidates to state that the candidate is in
    allowed: list of allowable states. If None, allows any state.
    """
    return allowed is None or status[candidate] in allowed

def select_seeds(G, status, groups, *, num_seeds=1, whole_groups=False, whole_expose_type=None, random_seeds=False, TA=None, SA2=None, seed_individual_id=None, group_lists=None, random_from_groups=False, seed_all=False, only_if=None, max_attempts=None, ):
    """Generates random individuals on a bipartite graph to seed, given some properties
    
    Parameters
    ----------
    G : networkx Graph
        Graph to select seed individuals from
    status : dict
        Mapping of nodes to their state in the simulation
    groups : iterable
        An iterable of the valid groups in the graph
    num_seeds : int
        Number of random seed individuals to generate
    whole_groups : bool
        Whether or not to seed entire groups (for random seeding or group_list seeding)
    whole_expose_type : iterable | None
        Types of groups to seed the entirety of, if seeding entire groups

    random_seeds : bool
        Whether or not to sample randomly. (If False, then one of ``seed_all``, ``seed_individual_id``, or ``group_lists`` must be given.)
    TA : int
        TA to restrict random seeding to.
    SA2 : int
        SA2 to restrict random seeding to. Ignored if TA is provided

    seed_individual_id : str | list[str]
        A single individual, or a list of individuals that are the seed cases

    group_lists : list[str]
        List of files that each contain a list of groups from which to select seeds from
    random_from_groups : bool
        Whether or not to, when using group_lists, to choose one random group from the list (True), or to seed in each listed group (False)

    seed_all : bool
        Whether or not to just seed all individuals

    only_if : None | list[str]
        If not None, represents a list of valid current statuses for seeds
    max_attempts : None | int
        Maximum number of attempts at randomly drawing valid individuals to seed before giving up

    Returns
    -------
    List of individuals' IDs that are to be exposed (Seeded)

    Notes
    -----

    Priority of different seeding modes:

        seed_individual_id > seed_all > random > group_lists
    """
    exposed = []

    if seed_individual_id is not None:
        if isinstance(seed_individual_id, str) and is_valid(seed_individual_id, status, only_if):
            exposed.append(seed_individual_id)
        elif isinstance(seed_individual_id, list):
            for indlist in seed_individual_id:
                with open(indlist, 'r', encoding='utf-8-sig') as fp:
                    ilist = fp.read().strip().split('\n')
                for indv in ilist:
                    if is_valid(indv, status, only_if):
                        exposed.append(indv)
        else:
            raise TypeError("given seed_individual_id is not a string or a list (of strings)")
    elif seed_all:
        exposed = set()
        for group in groups:
            for indv in G[group]:
                if is_valid(indv, status, only_if):
                    exposed.add(indv)
        exposed = list(exposed)
    elif random_seeds:
        if TA is not None:
            valid_groups = [group for group in groups if G.nodes[group].get('TA', None) == TA]
        elif SA2 is not None:
            valid_groups = [group for group in groups if G.nodes[group].get('SA2', None) == SA2]
        else:
            valid_groups = list(groups)
        if max_attempts is None:
            max_attempts = num_seeds
        for _ in range(max_attempts):
            group = random.choice(valid_groups)
            if len(G[group]) < 1:
                # empty group
                continue
            if group[:2] in whole_expose_type and whole_groups:
                for indv in G[group]:
                    if is_valid(indv, status, only_if):
                        exposed.append(indv)
            else:
                indv = random.choice(list(G[group]))
                if is_valid(indv, status, only_if):
                    exposed.append(indv)
            if len(exposed) >= num_seeds:
                break
    elif group_lists:
        for group_list in group_lists:
            with open(group_list, 'r', encoding='utf-8-sig') as fp:
                glist = fp.read().strip().split('\n')
            if random_from_groups:
                group = random.choice(glist)
                glist = [group]
            for group in glist:
                if whole_groups and group[:2] in whole_expose_type:
                    for indv in G[group]:
                        if is_valid(indv, status, only_if):
                            exposed.append(indv)
                else:
                    for _ in range(max_attempts if max_attempts is not None else 1):
                        indv = random.choice(list(G[group]))
                        if is_valid(indv, status, only_if):
                            exposed.append(indv)
                            break
    
    return exposed

def build_init(init_config: Mapping, G, groups):

    def init():
        """Sets initial condition, by selecting individuals to be seed cases.

        Parameters
        ----------
        init_config
            dictionary with options as described by the keyword arguments of  `select_seeds`
        G : networkx.Graph
            Graph to select seed individuals from
        groups: Iterable
            Group nodes of the graph

        Returns
        -------
        IC : A dictionary that maps node names to their initial state.
         IC contains a key for every node in the network object, individual nodes are given a disease state (SU etc), group/event nodes are given a value of 00.

        Note
        ----
        Currently, seed cases are assumed to be in the EU (exposed and unrestricted/undetected) state; all other individuals are SU (susceptible and unrestricted); all groups are in the 00 (open) state. This assumes a base lockdown level of 0, which can be immediately changed at t=0 of the simulation, which could set some groups to 0x (closed)
        """

        IC = {nd: 'SU' for nd in G.nodes}

        exposed = select_seeds(G, IC, groups, 
            num_seeds=init_config.get('num_seeds', None),
            whole_groups=init_config.get('whole_groups', False), 
            whole_expose_type=init_config.get('whole_expose_type', ['DW']),
            random_seeds=init_config.get('random', False), 
            TA=init_config.get('TA', None), 
            SA2=init_config.get('SA2', None),
            seed_individual_id=init_config.get('seed_individual_id', None), 
            seed_all=init_config.get('seed_all', False),
            group_lists=init_config.get('group_lists', []), 
            random_from_groups=init_config.get('random_from_groups', False),
            max_attempts=init_config.get('max_attempts', None),
            only_if=None)

        ##############################################
        # Set IC of individuals
        print("number of initial seeds:", len(set(exposed)))
        for node in exposed:
            IC[node] = 'EU'

        # no-event for group nodes
        for node in G.nodes:
            if G.nodes[node]['bipartite'] == 0:
                IC[node] = '00'

        return IC

    return init

def build_reseed():

    def reseed(G, status, properties):
        """Add additional seed cases during simulation

        Parameters
        ----------
        G : networkx Graph
            graph to select individuals to seed in
        status: dict
            mapping of individuals and groups to their current state
        properties: dict
            options for the reseeding, as described by the keyword arguments in `select_seeds`
            Also contains an additional keywords specific to reseeding:
              - ```time```: time at which the reseeding is scheduled to occur

        Returns
        -------
        List of mutations, which is a dictionary of events that occurred (individuals transitioning to Exposed)

        Notes
        -----
        This function is intended to be called as part of a manage_event routine, so it performs the state changes in this function, and then reports the changes back.
        """
        groups = (nd for nd in G if G.nodes[nd]['bipartite'] == 0)

        exposed = select_seeds(G, status, groups, 
            num_seeds=properties.get('num_seeds', None),
            whole_groups=properties.get('whole_groups', False), 
            whole_expose_type=properties.get('whole_expose_type', ['DW']),
            random_seeds=properties.get('random', False), 
            TA=properties.get('TA', None), 
            SA2=properties.get('SA2', None),
            seed_individual_id=properties.get('seed_individual_id', None), 
            seed_all=properties.get('seed_all', False),
            group_lists=properties.get('group_lists', []), 
            random_from_groups=properties.get('random_from_groups', False),
            only_if=properties.get('only_if', None), 
            max_attempts=properties.get('max_attempts', None))

        mutations = []
        for node in exposed:
            old_status = status[node]
            status[node] = f"E{old_status[1]}"

            mutations.append({
                't': properties['time'],
                'enode': node,
                'group': '',
                'efrom': old_status,
                'eto': status[node],
                'anode': '',
                'astatus': '',
            })
        return mutations

    return reseed

def restart_init(previous_status):
    """Builds the function that sets the initial conditions in the case of a "restarted" simulation.
    
    Parameters
    ----------
    previous_status : dict
        The mapping of individuals and groups to their states extracted from the end of the previous simulation
        
    Returns
    -------
    The initialisation function
    """

    def init():
        """Sets the initial conditions in the case of a "restarted" simulation.
        
        Returns
        -------
        A copy of the status at the end of the previous simulation.
        """
        IC = {k:v for k,v in previous_status.items()}
        return IC

    return init

def build_aux_sim_objs(sim_objs):
    """Calls build_reseed on an existing sim_object
    """
    sim_objs['reseed'] = build_reseed()

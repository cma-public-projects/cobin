""" Handling of direct neighbour lookups subject to a filter """

import random

def weighted_draw(options, weights):
    """Weighted random draw using an empirical cdf approach
    
    Args:
        options (dict): mapping of categories/keys to outcomes/values
        weights (dict): mapping of categories/keys to their weights
    """
    roll = random.random() * sum(weights.values())
    cdf = 0
    for category, pdf in weights.items():
        cdf += pdf
        if roll < cdf:
            return options[category]

def is_of_type(types):
    """Generates a filtering function that accepts only if the input string starts with any of the given types
    
    Args:
        types (list[str]): list of 2-character strings that the input string must start with to accept
    """
    def accept(node):
        return node[:2] in types
    return accept

def uniform_draw(G, node, acceptance_filter):
    """Uniformly draws a neighbour of a given node, according to some acceptance filter
    
    Args:
        G (nx.Graph): graph object
        node (nx.Node): node of the graph to select neighbours of
        acceptance_filter (Callable): function that returns True if the neighbour is acceptable
        
    Returns: 
        random neighbour of ``node`` that is accepted by the acceptance filter, or None

    Notes:
        Returns None if there are no acceptable neighbours.
    """
    items = [nd for nd in G[node] if acceptance_filter(nd)]
    if items:
        res = random.choice(items)
        return res
    return None

def draw_neighbour(G, node, weights, group_mapping):
    """Randomly draws a neighbour of a given node through a random (weighted) group
    
    Args:
        G (nx.Graph): graph
        node (nx.Node): node of the graph to draw a neighbour of
        weights (dict): dictionary of weights for drawing a group
        group_mapping (dict): mapping of group categories to group types
        
    Returns: 
        a neighbour and the group, or None
    
    Notes:
        Returns None if there are no valid groups, or no valid neighbours through a valid group
        
        The group mapping will look something like:

    .. code-block:: python

        {
            'HOME': ['DW', 'DG'],
            'WORK': ['WP', 'WG'],
            ...
        }
    """
    group = draw_group(G, node, weights, group_mapping)
    if group is None:
        return None
    neighbour = uniform_draw(G, group, lambda x: x != node)
    if neighbour is None:
        return None
    return neighbour, group

def draw_group(G, node, weights, group_mapping):
    """Randomly draws a group that a node belongs to, where the group types are weighted
    
    Args:
        G (nx.Graph): graph
        node (nx.Node): node of the graph to draw a group of
        weights (dict): dictionary of weights for drawing a group
        group_mapping (dict): mapping of group categories to group types

    Notes:
        The group mapping will look something like:

    .. code-block:: python

        {
            'HOME': ['DW', 'DG'],
            'WORK': ['WP', 'WG'],
            ...
        }
"""
    accept_filter = is_of_type(weighted_draw(group_mapping, weights))
    group = uniform_draw(G, node, accept_filter)
    return group

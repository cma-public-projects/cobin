""" Contagion on a bipartite network model implementation on the EoN stack
contact tracing included: only contact individuals of their household
household quarantine: quarantine is aware of households, and quarantined individuals
"""
import random
import math
import numbers
from numpy import digitize
from scipy.stats import gamma as scipy_gamma
from scipy.special import gammaincc as upper_incomplete_gamma
from collections import OrderedDict, defaultdict
from collections import abc
from operator import itemgetter, mul as product
from functools import partial
from warnings import warn
from enum import Enum, auto
import networkx as nx
# from . import graph_handling as gh
from .gillespie_max import BaseEvent, NoEvent
from .config_loader import ParameterTable
from . import neighbour_handling as rndx

_all_states = OrderedDict([
    ('SU', 'Susceptible, not in lockdown'),
    ('EU', 'Exposed, tests negative, undetected, not in lockdown'),
    ('AU', 'Asymptomatic, tests positive, undetected, not in lockdown'),
    ('BU', 'Presymptomatic mild, tests positive, infectious, undetected, not in lockdown'),
    ('MU', 'Mildly symptomatic, tests positive, infectious, undetected, not in lockdown'),
    ('PU', 'Presymptomatic, tests positive, infectious, undetected, not in lockdown'),
    ('IU', 'Infectious, serious symptoms, tests positive, undetected, not in lockdown'),
    ('RU', 'Recovered, never detected'),
    ('DX', 'Dead'),
    #####################################################
    ('SW', 'Susceptible, tested and waiting on results, tests negative, in self-isolation'),
    ('EW', 'Exposed, tested and waiting on results, tests negative, in self-isolation'),
    ('AW', 'Asymptomatic, tested and waiting on results, tests positive, undetected, in self-isolation'),
    ('BW', 'Presymptomatic mild, tested and waiting on results, tests positive, undetected, in self-isolation'),
    ('MW', 'Mildly symptomatic, tested and waiting on results, tests positive, undetected, in self-isolation'),
    ('PW', 'Presymptomatic, tested and waiting on results, tests positive, undetected, in self-isolation'),
    ('IW', 'Infectious, serious symptoms, tested and waiting on results, tests positive, undetected, in self-isolation'),
    #####################################################
    ('SD', 'Susceptible, in induced self-isolation due to an unconfirmed case'),
    ('ED', 'Exposed, tests negative, undetected, in induced self-isolation due to an unconfirmed case'),
    ('AD', 'Asymptomatic, tests positive, undetected, in induced self-isolation due to an unconfirmed case'),
    ('BD', 'Presymptomatic mild, tests positive, infectious, undetected, in induced self-isolation due to an unconfirmed case'),
    ('MD', 'Mildly symptomatic, tests positive, infectious, undetected, in induced self-isolation due to an unconfirmed case'),
    ('PD', 'Presymptomatic, tests positive, infectious, undetected, in induced self-isolation due to an unconfirmed case'),
    ('ID', 'Infectious, serious symptoms, tests positive, undetected, in induced self-isolation due to an unconfirmed case'),
    ######################################################
    ('AC', 'Asymptomatic, tests positive, detected, in lockdown'),
    ('BC', 'Presymptomatic mild, infectious, detected, in lockdown'),
    ('MC', 'Mildly symptomatic, tests positive, infectious, detected, in lockdown'),
    ('PC', 'Presymptomatic, tested positive, infectious, detected, in lockdown'),
    ('IC', 'Infectious, tested positive, symptomatic, detected, in lockdown'),
    ('RT', 'Recovered, detected'),
    ######################################################
    ('HQ', 'Hospitalised (non-critical condition), in effective quarantine'),
    ('CQ', 'Hospitalised in ICU (critical condition), in effective quarantine'),
    #####################################################
    ('SQ', 'Susceptible, in induced lockdown due to a confirmed case'),
    ('EQ', 'Exposed, test negative, undetected, in induced lockdown due to a confirmed case'),
    ('AQ', 'Asymptomatic, tests positive, undetected, in induced lockdown due to a confirmed case'),
    ('BQ', 'Presymptomatic mild, infectious, undetected, in induced lockdown due to a confirmed case'),
    ('MQ', 'Mildly symptomatic, tests positive, infectious, undetected, in induced lockdown due to a confirmed case'),
    ('PQ', 'Presymptomatic, tests positive, infectious, undetected, in induced lockdown due to a confirmed case'),
    ('IQ', 'Infectious, symptomatic, tests positive, undetected, in induced lockdown due to a confirmed case'),
    ######################################################
    ('00', '[GROUP] group state that denotes that the group can carry transmission'),
    ('0X', '[GROUP] group state that denotes that the group cannot carry transmission'),
])

_return_states = OrderedDict([(k, v) for k,v in _all_states.items() if not v.startswith("[GROUP]")])

_parameters = OrderedDict([
    ('beta_0', 'Base Infectivity/Transmissivity (S -> E)'),
    ('beta_c', 'Base Infectivity of Close Contacts'),
    ('beta_h', 'Base Infectivity for Density-Dependent Groups'),
    ('density', 'Density-dependence coefficient'),
    ('density_groups', 'List of groups that use density-dependence instead of weighted frequency dependence'),
    ('beta_weights', 'Age structured relative susceptibility'),
    ('group_type_weights', 'Likelihood weights on transmission contexts (groups)'),
    ('omega', 'Relative contact between quarantine and non-quarantine'),
    ('eps_a', 'Relative infectivity of asymptomatic individuals'),
    ('eps_b', 'Relative infectivity of presymptomatic mild individuals'),
    ('eps_m', 'Relative infectivity of mild individuals'),
    ('eps_p', 'Relative infectivity of presymptomatic individuals'),
    ('eps_h', 'Relative infectivity of hospitalised individuals'),
    ('eps_c', 'Relative infectivity of critically hospitalised individuals'),
    ('p_asympt_list', 'Age structured proportions of infectious cases that are asymptomatic'),
    ('p_mild_list', 'Age structured proportions of infectious cases that are mild'),
    ('gamma', '1/Incubation Period (from noninfectious to infectious eg E -> P)'),
    ('delta_bm', 'Symptom Progression Rate of mild infections (B -> M)'),
    ('delta_pi', 'Symptom Progression Rate of serious infections (P -> I)'),
    ('delta_ih', 'Symptom Progression Rate for hospitalisation (I -> H)'),
    ('delta_hc', 'Symptom Progression Rate for critical hospitalisation (H -> C)'),
    ('alpha_ar', 'Recovery Rate for asymptomatic infections (A -> R)'),
    ('alpha_mr', 'Recovery Rate for mild infections (M -> R)'),
    ('alpha_md', 'Fatal Termination Rate for mild infections (M -> D)'),
    ('alpha_ir', 'Recovery Rate for serious infections (I -> R)'),
    ('alpha_id', 'Fatal Termination Rate for serious infections (I -> D)'),
    ('alpha_hr', 'Recovery Rate for hospitalisations (H -> R)'),
    ('alpha_cr', 'Recovery Rate for critical hospitalisations (C -> R)'),
    ('alpha_cd', 'Fatal Termination Rate for critical hospitalisations (C -> D)'),
    ('mu_m', 'mortality probability for mild infection'),
    ('mu_i', 'mortality probability for serious infection'),
    ('mu_c_list', 'age structured mortality probability of critically hospitalised individuals'),
    ('eta_h_list', 'age structured probability of hospitalisation'),
    ('eta_c_list', 'age structured probability of critical cases (of hospitalisations)'),
    ('p_test_0', ' probability that asymptomatics/presymptomatics get tested'),
    ('p_test_m', ' probability that mild cases get tested'),
    ('p_test_i', 'Age structured probability that serious cases get tested'),
    ('theta_0', 'rate at which asymptomatics/presymptomatics seek tests'),
    ('theta_m', 'rate at which mild cases seek tests'),
    ('theta_i', 'rate at which serious cases seek tests'),
    ('theta_lab', 'delay for lab results to be returned'),
    ('quarantine', 'testing rates and probabilities while in quarantine/induced isolation'),
    ('test_also_iso', 'group categories that also isolate if someone seeks a test',),
    ('kappa_0', 'Trace time distributions'),
    ('kappa_c', 'Re-trace time (followup trace) distribution'),
    ('close_contact_weights', 'Likelihood weights on close contact transmission contexts (groups)'),
    ('trace_weights', 'Priority weights for contact tracing'),
    ('escape_time', 'duration of minimum quarantine'),
    ('notrace', 'probability that an event cannot be traced (by group)'),
    ('trace_drop', 'chance that a contact attempt fails (by group)'),
    ('max_trace_attempts', 'maximum number of attempts for any given contact'),
    ('casual_test_prob', 'probabilities that casual contacts of a known case get tested'),
    ('casual_test_delay', 'distribution of delay for casual contact to get back a positive test'),
    ('adaptive_lockdown', 'dictionary of triggers for a single adaptive lockdown'),
    ('lockdown_weights', 'dictionary of lockdown weights per group on beta'),
    ('lockdown_testing', 'upweight on testing during lockdown'),
    ('lockdown_shutdown', 'dictionary that contains information about group shutdowns'),
    ('lockdown_state', 'level of lockdown'),
    ('group_size_reject', 'threshold group size which gets rejected for transmission'),
    ('transmission_event_parameters', 'parameters related to the size of a transmission event'),
    ('trace_policy', 'what to do for close and casual contacts at each alert level'),
    ('vaccination', 'parameters for vaccinated individuals'),
    ('reseed', 'parameters for seeding infections dynamically'),
    ('alpha_sr', 'rate at which recovered individuals become susceptible again (S -> R)'),
])

_sim_objects = dict([
    ('destiny', 'future outcome states for branching transitions'),
    ('nstate', 'number of confirmed, and are confirmed/actively infected'),
    ('reseed', 'reseeding function'),
    ('gamma_hazard', 'hazard function for the gamma distribution'),
    ('entry_time', 'time when an individual enters the E state'),
    ('count', 'Count of infections in the simulation'),
    ('prev_infections', 'Dictionary that maps nodes to a list of when they had previously recovered')
])

_param_order = _parameters.keys()


################################################################################
## HELPER STRUCTURES ###########################################################
################################################################################

_reduce_r = {
    'U': 'U',
    'Q': 'U',
    'W': 'U',
    'D': 'U',
    'C': 'T'
}

_groups = {
    'HOME': [
        {
            'name': 'household',
            'prefix': 'DW',
            'part': 'DG',
            'small': 'DZ',
        }
    ],
    'WORK': [
        {
            'name': 'workplace_old',
            'prefix': 'WE',
            'part': 'WG',
            'small': 'WZ',
        },
        {
            'name': 'workplace_new',
            'prefix': 'WP',
            'part': 'WG',
            'small': 'WZ',
        },
        {
            'name': 'school',
            'prefix': 'SC',
            'part': 'SG',
            'small': 'SZ',
        },
        {
            'name': 'primary_school',
            'prefix': 'PM',
            'part': 'PG',
            'small': 'PZ',
        },
    ],
    'COMM': [
        {
            'name': 'event',
            'prefix': 'EV',
            'pseudopart': 'EX',
        }
    ]
}

# Map prefix fields to part and small fields
_part_maps = {x['prefix']:x['part'] for maps in _groups.values() for x in maps if 'part' in x}
_small_maps = {x['prefix']:x['small'] for maps in _groups.values() for x in maps if 'small' in x}
_inverse_part_maps = {v:k for k,v in _part_maps.items()}
_inverse_small_maps = {v:k for k,v in _small_maps.items()}

def _parse_groups(struct, fields, backup='prefix'):
    """Extracts values of keys (fields) from a list of dictionaries (struct)
    If fields do not exist for a dictionary item, uses the backup key instead.

    Args:
      struct (list[dict]): list of dictionaries to search
      fields (list[str]): keys to find in each dictionary
      backup (str):  (Default value = 'prefix')

    Returns:
        list of values found in the struct

    Example:

        >>> fields = ['one', 'three']
        >>> struct = [{'name': 1, 'one': 'a', 'two': 'b', 'three': 'c'},  # finds one, three
                      {'name': 2, 'one': 'd', 'four': 'e'},               # finds one
                      {'name': 3, 'three': 'f'},                          # finds three
                      {'name': 4, 'five': 'g'}]                           # finds none -> uses name
        >>> print(_parse_groups(struct, fields, backup='name'))
        ['a', 'c', 'd', 'f', 4]
    """
    parsed = []
    for item in struct:
        empty = True
        for field in fields:
            if field in item:
                parsed.append(item[field])
                empty = False
        if empty:
            parsed.append(item[backup])
    return parsed

# Map contexts to fields dep. on their "closeness" definitions
_all_groups = {ctxt:_parse_groups(maps, ('prefix', 'part', 'pseudopart', 'small')) for ctxt, maps in _groups.items()}
_casual_groups = {ctxt:_parse_groups(maps, ('prefix', 'small')) for ctxt, maps in _groups.items()}
_close_groups = {ctxt:_parse_groups(maps, ('pseudopart', 'part', 'small')) for ctxt, maps in _groups.items()}

_inverse_all_groups = {vi:k for k,v in _all_groups.items() for vi in v}
_inverse_casual_groups = {vi:k for k,v in _casual_groups.items() for vi in v}
_inverse_close_groups = {vi:k for k,v in _close_groups.items() for vi in v}

_card_idx = 'card'
_default_prob_covid_card = 0.0

class Event(BaseEvent, Enum):
    """Enum of event types in the model
    
    Args:
      BaseEvent (Enum): Enumeration class that defines an ordering over the elements
      Enum (Enum): Dummy variable for Enum
    """

    spontaneous = auto()
    infect = auto()
    seek_test = auto()
    get_test_result = auto()
    become_detected = auto()
    trace = auto()
    lockdown = auto()
    escape = auto()
    reseed = auto()
    removal = auto()
    recovery_waning = auto()

# arbitrary truthy that can be hashed
_is_vacc = True

_miq_policy = {
    'home': {
        'at_miq': '',
        'at_home': 'CQWD'
    },
    'miq': {
        'at_miq': 'C',
        'at_home': 'QWD'
    },
}

################################################################################
## HELPER FUNCTIONS ############################################################
################################################################################

def fix_age(age):
    """Cleans non-grouped ages

    Args:
      age (int): An age value

    Returns: Age category (0, 1, 2, 3)

    """
    if age not in (0,1,2,3):
        return (age > 14) + (age > 29) + (age > 64)
    return age

def draw_num_infections(group, size, parameters):
    """Draws the number of infections in a single EV or EX infection event
    
    Roughly, the number is a log-normal distribution with the underlying
    exponential distribution having a mean of log(max(log10(N),1)) where N is the event size

    Args:
      group (str): name of the group
      size (int): number of individuals connected to the group
      parameters (dict): distribution parameters

    Returns: number of simultaneous infections

    """
    N = size - 1
    if N == 0:
        # empty groups
        return 0
    if group[:2] != 'EV' and group[:2] != 'EX':
        # not doing multi-infection for non-community groups
        return 1
    if group[:2] == 'EV':
        sigma = max(math.log(N**(parameters['exponent'])), parameters['sigma'])
        mu = math.log(max(math.log10(N), 1))
        cand = random.lognormvariate(mu, sigma)
    elif group[:2] == 'EX':
        cand = round(N*random.betavariate(parameters['alpha'], parameters['beta'])+1,0)
    return min(max(int(cand), 1), N)


def is_group_lockdown(group, group_info, shutdown_info, by_map):
    """Determines if a group is closed or not in a lockdown context

    Args:
      group (str): Name of the group 
      group_info (dict): Node attributes of the group
      shutdown_info (dict): Information on the way to determine whether the group is closed or not
      by_map (dict): Lookup map for attribute-based binary closures

    Returns: 
        The state code of the group

    Notes:
      Shutdown_info should be a dict containing entries of one of the following forms:
    .. code-block:: 

        shutdown_info = {
            GROUP_PREFIX:
                attribute: ATTRIBUTE_KEY
                DATA_KEY: NUMERIC_DATA_VALUE
        
            GROUP_PREFIX: NUMERIC_DATA_VALUE
        
            GROUP_PREFIX: ATTRIBUTE_KEY [str]
        }

    ``by_map`` is used by the third form, where the ATTRIBUTE_KEY can be directly mapped to a result

    """
    if group[:2].lower() not in shutdown_info:
        return '00'
    shutdown_group_info = shutdown_info[group[:2].lower()]
    # simple probability
    if isinstance(shutdown_group_info, numbers.Number):
        if random.random() < shutdown_group_info:
            return '0X'
    # binary flag attribute
    elif isinstance(shutdown_group_info, str):
        flag = group_info[shutdown_group_info]
        return by_map[flag]
    # attribute dependent probability
    elif 'attribute' in shutdown_group_info:
        group_attr = group_info[shutdown_group_info['attribute']]
        if random.random() < shutdown_group_info[group_attr]:
            return '0X'
    else:
        raise TypeError(f'Unknown group shutdown specification: \n---\n {shutdown_info} \n---')

    return '00'

def check_lockdown_trigger(lockdown_info, nstate, rechecking=False):
    """Returns whether a lockdown trigger is activated given nstate of system

    Args:
      lockdown_info: Trigger conditions dictionary of the given lockdown
      nstate: List of current state of known cases
      rechecking: whether or not an exhausted condition should be forcefully checked (Default value = False)

    Returns:
        whether or not the lockdown trigger has activated

    Notes:
        nstate is [number known cumulative, number known active]
        lockdown_info has fields: 

            * name: <str> unique identifier
            * level: <hashable> alert level to go to
            * delay: <float> delay time before alert level change
            * exhausted: <binary> whether or not the trigger is inactive
            * [ncumul]: <float +> lower required threshold for number of cumulative cases
            * [nactive]: <float> if positive, lower required threshold of number of active cases.
              if negative, upper required threshold of number of active cases
            * [recheck]: <bool> whether to recheck condition on activation
            * [reactivated_by]: <list <str>> list of names of trigger that remove exhaustion
    """
    if not rechecking and lockdown_info.get('exhausted', False):
        return False
    is_trigger = []
    if 'ncumul' in lockdown_info:
        is_trigger.append(nstate[0] >= lockdown_info['ncumul'])
    if 'nactive' in lockdown_info:
        if lockdown_info['nactive'] >= 0:
            is_trigger.append(nstate[1] >= lockdown_info['nactive'])
        else:
            is_trigger.append(nstate[1] < -lockdown_info['nactive'])
    return all(is_trigger)

def check_all_lockdown_triggers(lockdown_infos, nstate, queue, time):
    """Checks lockdown triggers. Exhausts and queues them if do
    
    Args:
        lockdown_infos (list[dict]): list of lockdown infos
        nstate (list[int]): current state of system [cumul known, active known]
        queue (list): queue of delayed events to add to
        time (float): current time

    Notes:
        Adds a lockdown event (`Event.lockdown`) for each lockdown that is triggered, and sets their ``exhausted`` value to ``True``
    """
    for lockdown_info in lockdown_infos:
        if check_lockdown_trigger(lockdown_info, nstate):
            lockdown_info['exhausted'] = True
            print(f"Queuing lockdown {lockdown_info['name']} at {time}, delay {lockdown_info['delay']}")
            queue.add((time + lockdown_info['delay'],
                       Event.lockdown,
                       lockdown_info['name'],
                      ))

def check_lockdown_triggers_reactivate(trigger_name, lockdown_infos):
    """Checks if any lockdown triggers are reactivated
    
    Reactivating a trigger means that it can potentially fire in the future
    """
    for lockdown_info in lockdown_infos:
        if trigger_name in lockdown_info.get('reactivated_by', []):
            lockdown_info['exhausted'] = False

def index_node_attributes(node_attributes, subfactor_dict, skip:list=None):
    """Computes the overall factor from a series of subfactors by indexing node attributes
    
    subfactor_dict is a dictionary structured as

    .. code-block::

        {
            FIELD:
                default: DEFAULT_VALUE (required)
                INDEX_0: VALUE
                INDEX_1: VALUE
                ...
            FIELD:
                ...
            baseline: BASELINE_VALUE (default 1)
        }
    
    It does not consider FIELDs listed in ``skip``.

    Args:
      node_attributes: 
      subfactor_dict: 
      skip:list:  (Default value = None)

    Returns:
        the product of subfactor_dict[FIELD][node_attributes[FIELD]] over all FIELDs * baseline

    """
    reserved = ['baseline']
    if skip is not None:
        reserved.extend(skip)
    factor_product = subfactor_dict.get('baseline', 1)
    for field, values in subfactor_dict.items():
        if field not in reserved:
            if field == 'age':
                factor_product *= values.get(fix_age(node_attributes['age']), values['default'])
            else:
                factor_product *= values.get(node_attributes[field], values['default'])
    return factor_product

def compute_vaccination_level(t, node_attrs, parameters, sim_objects):
    """Computes the number of doses of vaccine

    Args:
      t: Current time
      node_attrs: Attribute dictionary of the node
      parameters: simulation parameters
      sim_objects: dynamic simulation cache

    Returns:
        Number of vaccine doses the individual has at current time
    """
    t_relative = t + parameters['vaccination']['date_offset']
    vaccine_timings = node_attrs['vaccine_dates']
    doses = digitize(t_relative, vaccine_timings)
    return doses

def logistic_fn(t, L, k, t0):
    """Logistic (sigmoid) function in t

    Args:
        t (float): independent variable
        L (float): maximum value
        k (float): growth rate
        t0 (float): sigmoid midpoint

    Returns:
        Computes $f(t) = \frac{L}{1 + e^{-k(t-t_0)}}$, truncated above at $f(t) = 1$.
    """
    return min(L / (1 + math.exp(-k * (t - t0))),1)

def compute_waning_factor(t, node_name, doses, node_attrs, parameters, sim_objects,
                          aspect='default'):
    """Computes the waning factor associated with vaccination for an individual

    Args:
      t: current time
      node_name: nae of node
      doses: number of vaccine doses
      node_attrs: attributes of the node
      parameters: simulation parameters
      sim_objects: dynamic simulation caches
      aspect: property to compute waning vaccine efficacy for, defaults to 'default'

    Returns:
      relative waned effect of the vaccine
    """
    if doses > 0:
        last_vaccine = node_attrs['vaccine_dates'][doses - 1] - parameters['vaccination']['date_offset']
    else:
        last_vaccine = 0
    ref_time = last_vaccine
    num_prev_infs = len(sim_objects['prev_infections'].get(node_name, []))
    if num_prev_infs:
        last_infection = sim_objects['prev_infections'][node_name][-1]
        ref_time = max(last_vaccine, last_infection)
    param_list = parameters['vaccination'][(doses, num_prev_infs>0)]['waning'][aspect]
    waning_effect = logistic_fn(t - ref_time, *param_list)
    return waning_effect

def neighbouring_grp_szs(G, node, parameters, sim_objects):
    """
    Enumerates over all nodes an individual node is connected to. 
    If any are of a group type specified in the density_groups input parameter, 
    adds the size of that group to the group_size dictionary (cached in `sim_objects`).
    If a group is not of a type specified in density_groups then yields the default value if function was called with one, else does not yield anything. 
    
    Args:
      G: graph object
      node: node for which to compute size of groups it is connected to
      parameters: simulation parameters
      sim_objects: dynamic simulation objects (simulation memory)    
    """
    for g in G[node]:
        if g[:2] in parameters['density_groups']:
            if g not in sim_objects['group_size']:
                sim_objects['group_size'][g] = len(G[g])
            yield sim_objects['group_size'][g]

def get_max_grp_sz(G, node, parameters, sim_objects):
    """
    Returns the size of the largest group a node is connected to.
    
    Args:
      G: graph object
      node: node for which to compute size of groups it is connected to
      parameters: simulation parameters
      sim_objects: dynamic simulation objects (simulation memory)    
    """
    if node in sim_objects['max_group_size']:
        return sim_objects['max_group_size'][node]
    sim_objects['max_group_size'][node] = max(neighbouring_grp_szs(G, node, parameters, sim_objects), default=0)
    return sim_objects['max_group_size'][node]

def ptable_lookup(table, node_attributes):
    """Looks up a table (`ParameterTable`) based on the node attribute dictionary
    
    Args:
        table (`ParameterTable` or list-like): Data to lookup.
        node_attributes (dict): attribute dictionary of a node
    
    Returns:
        value in the table given the node attributes

    Notes:
        If the table is not a ParameterTable, it is assumed it is an age-based lookup list (list or dict with integer keys)
    """
    if isinstance(table, ParameterTable):
        return table.lookup(node_attributes)
    else:
        return table[fix_age(node_attributes['age'])]

def default_lookup(instance, key):
    """Default lookup using as default the value associated with the key 'default'
    
    Args:
        instance: object to lookup
        key: key value

    Returns:
        The looked-up value if the instance is a dictionary, else returns the instance itself
    """
    if isinstance(instance, dict):
        return instance.get(key, instance['default'])
    else:
        return instance


################################################################################
## CORE SIMULATION FUNCTIONS ###################################################
################################################################################

def rate_function(G, node, status, parameters, sim_objects):
    """Defines (maximal) rate of reactions for nodes in particular statuses

    Args:
      G: graph object
      node: node to compute sum of rates of all associated reactions of
      status: mapping from nodes to their statuses
      parameters: simulation parameters
      sim_objects: dynamic simulation objects (simulation memory)

    Returns:
      sum of rates of reactions associated with node
    """
    
    # get disease state and quarantine state of node
    infected, lockdown = status[node]

    # if node is susceptible, dead, or an event node, no rate of reaction - does not self-evolve states
    if infected in 'SD0': 
        return 0
    
    # if there is a valid disease progression/reaction for this state, compute the rates for them
    (beta_0s, beta_cs, beta_h, density, density_groups, beta_weights, group_type_weights, omega,
     eps_a, eps_b, eps_m, eps_p, eps_h, eps_c, p_asympt_list, p_mild_list, gamma, delta_bm, delta_pi,
     delta_ih, delta_hc, alpha_ar, alpha_mr, alpha_md, alpha_ir, alpha_id, alpha_hr, alpha_cr,
     alpha_cd, mu_m, mu_i, mu_c_list, eta_h_list, eta_c_list, p_test_0, p_test_m, p_test_i,
     theta_0, theta_m, theta_i, theta_lab, quarantine, test_also_iso, kappa_0, kappa_c,
     close_contact_weights, trace_weights, escape_time, notrace, trace_drop, max_trace_attempts,
     casual_test_prob, casual_test_delay, adaptive_lockdown, lockdown_weights, lockdown_testing,
     lockdown_shutdown, base_lockdown_state, group_size_reject, transmission_event_parameters,
     trace_policy, vaccination, reseed, alpha_sr) = parameters.values()

    node_attributes = G.nodes[node]

    lockdown_state = sim_objects['lockdown_state']

    # lockdown structure
    if lockdown_state != 0:
        p_test_0 = lockdown_testing[lockdown_state]['p_test_0']
        p_test_m = lockdown_testing[lockdown_state]['p_test_m']
        p_test_i = ptable_lookup(lockdown_testing[lockdown_state]['p_test_i'], node_attributes)
        theta_0 = lockdown_testing[lockdown_state]['theta_0']
        theta_m = lockdown_testing[lockdown_state]['theta_m']
        theta_i = lockdown_testing[lockdown_state]['theta_i']
    else:
        p_test_i = ptable_lookup(p_test_i, node_attributes)    

    # age structure
    # if infected in 'ABMPIHC':
    if infected in 'ABMPIHC':
        beta_0 = ptable_lookup(beta_0s, node_attributes)
        beta_c = ptable_lookup(beta_cs, node_attributes)
        beta_h = ptable_lookup(beta_h, node_attributes)
        lambda_h = beta_h * get_max_grp_sz(G, node, parameters, sim_objects) ** parameters['density']
        beta_sum = beta_0 + beta_c + lambda_h

    # state based adjustments
    # overwrite lockdown settings
    if lockdown == 'Q' and infected not in 'HC':
        theta_0 = quarantine['theta_0']
        theta_m = quarantine['theta_m']
        theta_i = quarantine['theta_i']

    # roll destiny values
    if node not in sim_objects['destiny']:
        sim_objects['destiny'][node] = {
            'T': random.random(),
            'D': random.random(),
            'H': random.random(),
            'C': random.random(),
        }

    if infected == 'E':
        # transition
        return 1./gamma['scale']
    if infected == 'A':
        # infection + recovery + testing
        return eps_a*(beta_sum) + alpha_ar + theta_0
    if infected == 'B':
        # infection + transition to M + testing
        return eps_b*(beta_sum) + delta_bm + theta_0
    if infected == 'M':
        # infection + (death OR recovery) + testing
        return (eps_m*(beta_sum) + alpha_md + alpha_mr + theta_m)
    if infected == 'P':
        # infection + transition to I + testing
        return eps_p*(beta_sum) + delta_pi + theta_0
    if infected == 'I':
        # infection + death + hospitalisation + recovery + testing
        return ((beta_sum) + alpha_id + delta_ih + alpha_ir + theta_i)
    if infected == 'H':
        # infection + recovery + ICU
        return eps_h*(beta_sum) + delta_hc + alpha_hr
    if infected == 'C':
        # in ICU
        # infection + death + recovery
        return eps_c*(beta_sum) + alpha_cd + alpha_cr
    if infected == 'R':
        return alpha_sr

    warn(RuntimeWarning(f'Unknown state: {infected}{lockdown}'))
    return 0

def transition_choice(G, node, t, status, parameters, sim_objects):
    """Determines the subtype of event that will occur, given a reaction involves a particular node

    Args:
      G: graph
      node: node that is reacting
      t: current time (time that reaction is initiated)
      status: mapping from node to status of node
      parameters: simulation parameters
      sim_objects: dynamic simulation caches

    Returns:
      tuple of the event type and auxiliary information (typically state to transition to)
    """
    infected, lockdown = status[node]

    (beta_0s, beta_cs, beta_h, density, density_groups, beta_weights, group_type_weights, omega,
     eps_a, eps_b, eps_m, eps_p, eps_h, eps_c, p_asympt_list, p_mild_list, gamma, delta_bm, delta_pi,
     delta_ih, delta_hc, alpha_ar, alpha_mr, alpha_md, alpha_ir, alpha_id, alpha_hr, alpha_cr,
     alpha_cd, mu_m, mu_i, mu_c_list, eta_h_list, eta_c_list, p_test_0, p_test_m, p_test_i,
     theta_0, theta_m, theta_i, theta_lab, quarantine, test_also_iso, kappa_0, kappa_c,
     close_contact_weights, trace_weights, escape_time, notrace, trace_drop, max_trace_attempts,
     casual_test_prob, casual_test_delay, adaptive_lockdown, lockdown_weights, lockdown_testing,
     lockdown_shutdown, base_lockdown_state, group_size_reject, transmission_event_parameters,
     trace_policy, vaccination, reseed, alpha_sr) = parameters.values()
    node_attributes = G.nodes[node]

    lockdown_state = sim_objects['lockdown_state']

    # lockdown structure
    if lockdown_state != 0:
        p_test_0 = lockdown_testing[lockdown_state]['p_test_0']
        p_test_m = lockdown_testing[lockdown_state]['p_test_m']
        p_test_i = ptable_lookup(lockdown_testing[lockdown_state]['p_test_i'], node_attributes)
        theta_0 = lockdown_testing[lockdown_state]['theta_0']
        theta_m = lockdown_testing[lockdown_state]['theta_m']
        theta_i = lockdown_testing[lockdown_state]['theta_i']
    else:
        p_test_i = ptable_lookup(p_test_i, node_attributes)

    # age structure
    # if infected in 'EIHC':
    p_mild = ptable_lookup(p_mild_list, node_attributes)
    p_asympt = ptable_lookup(p_asympt_list, node_attributes)
    eta_h = ptable_lookup(eta_h_list, node_attributes)
    eta_icu = ptable_lookup(eta_c_list, node_attributes)
    mu_icu = ptable_lookup(mu_c_list, node_attributes)

    doses = compute_vaccination_level(t, node_attributes, parameters, sim_objects)
    prev_infs = len(sim_objects['prev_infections'].get(node, []))

    # draw/update vaccination-related behaviour
    if doses > 0 or prev_infs > 0:
        vacc_ps = vaccination[doses, prev_infs>0]
        
        # calculate the conditional probabilities given state, vaccination level, and time since last vaccination. 
        waning_sympt_given_infected = compute_waning_factor(t, node, doses, node_attributes, parameters, sim_objects, aspect='symptoms')        
        
        # adjust symptom presentation                                    
        p_mild *= ptable_lookup(vacc_ps['p_mild'], node_attributes) * waning_sympt_given_infected
        p_asympt = 1 - ((1 - p_asympt) * (ptable_lookup(vacc_ps['p_sympt'], node_attributes) * waning_sympt_given_infected))    
        # update testing "propensity"
        p_test_0 *= vacc_ps['testing'].get('p_test_0', 1)
        p_test_m *= vacc_ps['testing'].get('p_test_m', 1)
        p_test_i *= vacc_ps['testing'].get('p_test_i', 1)
        # update hospitalisation
        eta_h *= index_node_attributes(node_attributes, vacc_ps['hospitalisation']['eta_h']) *  compute_waning_factor(t, node, doses, node_attributes, parameters, sim_objects, aspect='hospitalisation')

        eta_icu *= index_node_attributes(node_attributes, vacc_ps['hospitalisation']['eta_c']) * compute_waning_factor(t, node, doses, node_attributes, parameters, sim_objects, aspect='critical')
        # update death
        mu_m *= vacc_ps['mortality']['mu_m']
        mu_i *= vacc_ps['mortality']['mu_i']
        mu_icu *= index_node_attributes(node_attributes, vacc_ps['mortality']['mu_c']) * compute_waning_factor(t, node, doses, node_attributes, parameters, sim_objects, aspect='mortality')

    # reduced infectivity in each state
    eps = {'A': eps_a, 'B': eps_b, 'M': eps_m, 'P': eps_p, 'I': 1, 'H': eps_h, 'C': eps_c}
    if infected in eps:
        beta_0 = ptable_lookup(beta_0s, node_attributes)
        beta_c = ptable_lookup(beta_cs, node_attributes)
        beta_h = ptable_lookup(beta_h, node_attributes)
        lambda_h = beta_h * get_max_grp_sz(G, node, parameters, sim_objects) ** parameters['density']
        b = (eps[infected] if infected in eps else 0) * (beta_0 + beta_c + lambda_h)

    # state based adjustments
    # lockdown overwrites
    if lockdown == 'Q' and infected not in 'HC':
        p_test_0 = quarantine['p_test_0']
        p_test_m = quarantine['p_test_m']
        p_test_i = ptable_lookup(quarantine['p_test_i'], node_attributes)
        theta_0 = quarantine['theta_0']
        theta_m = quarantine['theta_m']
        theta_i = quarantine['theta_i']

    # destiny structure
    is_test, is_die, is_hosp, is_crit = False, False, False, False
    if infected in 'ABP':
        is_test = sim_objects['destiny'][node]['T'] < p_test_0
    elif infected in 'M':
        is_die = sim_objects['destiny'][node]['D'] < mu_m
        is_test = sim_objects['destiny'][node]['T'] < p_test_m
    elif infected in 'I':
        is_die = sim_objects['destiny'][node]['D'] < mu_i
        is_test = sim_objects['destiny'][node]['T'] < p_test_i
        is_hosp = sim_objects['destiny'][node]['H'] < eta_h
    elif infected in 'H':
        is_crit = sim_objects['destiny'][node]['C'] < eta_icu
    elif infected in 'C':
        is_die = sim_objects['destiny'][node]['D'] < mu_icu

    # waiting on test or confirmed individuals cannot re-test
    if lockdown in 'CW':
        is_test = False

    if infected == 'E':
        # infectivity progression
        roll = random.random()*(1./gamma['scale'])
        if roll < sim_objects['gamma_hazard'](t - sim_objects['entry_time'][node]):
            roll = random.random()
            if roll < p_asympt:
                return Event.spontaneous, 'A' + lockdown
            elif roll < p_asympt + p_mild:
                return Event.spontaneous, 'B' + lockdown
            else:
                return Event.spontaneous, 'P' + lockdown
        else:
            return NoEvent.no_event, 
    if infected == 'A':
        roll = random.random()*(b + alpha_ar + theta_0)
        # recovers
        if roll < alpha_ar:
            return Event.removal, 'R' + _reduce_r[lockdown]
        # seek test
        if roll < (theta_0 + alpha_ar):
            if is_test:
                return Event.seek_test, # comma is important
            else:
                return NoEvent.no_event, # comma is important
        # infects others
        if roll < (theta_0 + alpha_ar + b):
            return Event.infect, # comma is important
    if infected == 'B':
        roll = random.random()*(delta_bm + theta_0 + b)
        # progression
        if roll < delta_bm:
            return Event.spontaneous, 'M' + lockdown
        # testing
        if roll < (theta_0 + delta_bm):
            if is_test:
                return Event.seek_test, # comma is important
            else:
                return NoEvent.no_event, # comma is important
        # infect other
        if roll < (theta_0 + delta_bm + b):
            return Event.infect, # comma is important
    if infected == 'P':
        roll = random.random()*(delta_pi + theta_0 + b)
        # symptomatic progression
        if roll < delta_pi:
            return Event.spontaneous, 'I'+ lockdown
        # testing
        if roll < (delta_pi + theta_0):
            if is_test:
                return Event.seek_test, # comma is important
            else:
                return NoEvent.no_event, # comma is important
        # infect other
        if roll < (delta_pi + theta_0 + b):
            return Event.infect, # comma is important
    if infected == 'M':
        roll = random.random()*(alpha_md + alpha_mr + theta_m + b)
        # death
        if roll < alpha_md:
            if is_die:
                return Event.become_detected, 'DX'
            else:
                return NoEvent.no_event, # comma is important
        # recovery:
        if roll < (alpha_md + alpha_mr):
            if not is_die:
                return Event.removal, 'R' + _reduce_r[lockdown]
            else:
                return NoEvent.no_event, # comma is important
        # testing
        if roll < (alpha_md + alpha_mr + theta_m):
            if is_test:
                return Event.seek_test, # comma is important
            else:
                return NoEvent.no_event, # comma is important
        # infect other
        if roll < (b + alpha_md + alpha_mr + theta_m):
            return Event.infect, # comma is important
    # IU and IQ
    if infected == 'I':
        roll = random.random() * (alpha_id + delta_ih + alpha_ir + theta_i + b)
        # death
        if roll < (alpha_id):
            if is_die:
                return Event.removal, 'DX'
            else:
                return NoEvent.no_event, # comma is important
        # hospitalisation
        if roll < (alpha_id + delta_ih):
            if is_hosp:
                return Event.become_detected, 'HQ'
            else:
                return NoEvent.no_event, # comma is important
        # recovery
        if roll < (alpha_id + delta_ih + alpha_ir):
            if not (is_die or is_hosp):
                return Event.removal, 'R' + _reduce_r[lockdown]
            else:
                return NoEvent.no_event, # comma is important
        # testing
        if roll < (alpha_id + delta_ih + alpha_ir + theta_i):
            if is_test:
                return Event.seek_test, # comma is important
            else:
                return NoEvent.no_event, # comma is important
        # infect others
        if roll < (alpha_id + delta_ih + alpha_ir + theta_i + b):
            return Event.infect, # comma is important
    if infected == 'H':
        roll = random.random()*(delta_hc + alpha_hr + b)
        # critical condition
        if roll < delta_hc:
            if is_crit:
                return Event.spontaneous, 'CQ'
            else:
                return NoEvent.no_event, # comma is important
        # infect others
        if roll < (delta_hc + b):
            return Event.infect, # comma is important
        # recovery
        if not is_crit:
            return Event.removal, 'RT'
        else:
            return NoEvent.no_event, # comma is important
    if infected == 'C':
        roll = random.random()*(alpha_cd + alpha_cr + b)
        # death
        if roll < (alpha_cd):
            if is_die:
                return Event.removal, 'DX'
            else:
                return NoEvent.no_event, # comma is important
        # infect others
        if roll < (alpha_cd + b):
            return Event.infect, # comma is important
        # recovery
        if not is_die:
            return Event.removal, 'RT'
        else:
            return NoEvent.no_event, # comma is important
    if infected == 'R':
        return Event.recovery_waning, 'SU'

    # default
    warn(f"Unparsed state {status[node]} of {node}", category=RuntimeWarning)
    return NoEvent.no_event, # comma is important

def manage_trace_event(G, t, event_info, status, event_queue, parameters, sim_objects):
    """Executes a single attempt of contact tracing to place a contact into isolation

    Args:
      G: graph
      t: current time
      event_info: auxiliary information of the event

        - node to trace
        - number of attempts made so far
        - delay parameter or attempt if not successful
        - probability of attempt failure
        - node that caused the contact tracing
        - group through which contact tracing is occurring
      status: mapping of statuses of nodes
      event_queue: queue of events to occur in the future
      parameters: simulation parameters
      sim_objects: dynamic simulation caches

    Returns:
      list of transition events and list of nodes to have their rates updated
    """
    events, influence_set = [], []
    node, attempts, delay, p_fail, trc_nd, trc_grp = event_info
    # Test tracing attempt successful
    if random.random() > p_fail:
        # successful contact, check status of individual
        if status[node][1] in 'UD' and status[node][0] not in 'RD0':
            old_status = status[node]
            status[node] = status[node][0] + 'Q'
            influence_set.append(node)
            events.append({
                't': t,
                'enode': node,
                'anode': trc_nd,
                'group': trc_grp,
                'efrom': old_status,
                'eto': status[node],
                'astatus': status[trc_nd]
            })
            default_quarantine = parameters['escape_time']['default']
            dose = compute_vaccination_level(t, G.nodes[node], parameters, sim_objects)
            quarantine_period = parameters['escape_time'].get(dose, default_quarantine)
            # define a quarantine escape event in the event queue
            event_queue.add((t+quarantine_period, Event.escape, node, 0))
        elif status[node] == 'RU':
            # RU gets a test after some time
            p_test = float(parameters['quarantine']['p_test_0'])
            if random.random() < p_test:
                dt = random.expovariate(float(parameters['quarantine']['theta_0']))
                event_queue.add((t+dt, Event.seek_test, node, trc_nd, trc_grp))
    # retrace on attempt failure
    elif attempts < parameters['max_trace_attempts']:
        tr_delay = random.expovariate(delay)
        # add a retrace event in the event queue
        event_queue.add((t+tr_delay, Event.trace, node, attempts+1, delay, p_fail, trc_nd, trc_grp))
    return events, influence_set

def manage_seek_test_event(G, t, event_info, status, event_queue, parameters, sim_objects):
    """Executes the test seeking event

    Args:
      G: graph
      t: current time
      event_info: auxiliary information for event

        - node that is seeking a test
        - node (if any) that is causing the test seeking
        - group (if any) that test seeking is being caused through
      status: mapping of nodes to states
      event_queue: queue of events to happen in future
      parameters: simulation parameters
      sim_objects: dynamic simulation caches

    Returns:
      list of transition events that occurred and nodes to update the rates of

    """
    events, influence_set = [], []
    node, *cause = event_info
    old_status = status[node]

    # already known positive ->  do nothing
    if old_status[1] not in 'UQD':
        return [], []

    if old_status[0] in 'SEABMPI':
        new_status = old_status[0] + 'W'
        status[node] = new_status
        if len(cause):
            anode, group = cause
            astatus = status[anode]
        else:
            anode, group, astatus = '', '', ''
        events.append({
            't': t,
            'enode': node,
            'anode': anode,
            'group': group,
            'efrom': old_status,
            'eto': new_status,
            'astatus': astatus,
        })

    # compute test result
    is_positive_test = old_status[0] not in 'SE'

    # add test return event
    theta_lab = parameters['theta_lab']
    if sim_objects['lockdown_state'] != 0:
        theta_lab = parameters['lockdown_testing'][sim_objects['lockdown_state']]['theta_lab']
    theta_lab = float(theta_lab)
    lab_return_delay = random.expovariate(theta_lab)
    event_queue.add((t+lab_return_delay, Event.get_test_result, node, is_positive_test, *cause))

    # dwelling (or otherwise spec'd) goes into a distanced mode
    groups_also_iso = [grp_prefix for grptype in parameters['test_also_iso']
                       for grp_prefix in _close_groups[grptype]]
    dwelling_grp = [grp for grp in G.neighbors(node) if grp[:2] in groups_also_iso]
    if len(dwelling_grp) == 0:
        dwelling_nodes = {}
    else:
        dwelling_nodes = {nd: grp for grp in dwelling_grp for nd in G.neighbors(grp)}
    for dw_nd, dw_grp in dwelling_nodes.items():
        if dw_nd != node and status[dw_nd][1] == 'U' and status[dw_nd][0] not in 'RD0':
            old_status = status[dw_nd]
            status[dw_nd] = status[dw_nd][0] + 'D'
            influence_set.append(dw_nd)
            events.append({
                't': t,
                'enode': dw_nd,
                'anode': node,
                'group': dw_grp,
                'efrom': old_status,
                'eto': status[dw_nd],
                'astatus': status[node],
            })
            event_queue.add((t+lab_return_delay+1e-15*random.random(), Event.escape, dw_nd, 1))

    return events, influence_set

def manage_get_test_result_event(G, t, event_info, status, event_queue, parameters, sim_objects):
    """Executes the event of returning a test result

    Args:
      G: graph
      t: current time
      event_info: auxiliary event information
    
        * node that is getting a test back
        * whether that test is positive
        * node (if any) that caused the test seeking
        * group (if any) through which the test seeking was triggered
      status: mapping of nodes to states
      event_queue: queue of events that happen in the future
      parameters: simulation parameters
      sim_objects: dynamic simulation caches

    Returns:
      list of transition events and list of nodes that need their rates to be updated

    """
    node, is_positive_test, *_ = event_info
    old_status = status[node]
    # already known positive ->  do nothing
    if old_status[1] in 'CT':
        return [], []
    # did not test positive, can go free
    if not is_positive_test:
        new_status = old_status[0] + 'U'
        # no-op on confirmed_event
        event_info = (node, new_status)
        return manage_spontaneous_event(G, t, event_info, status, event_queue, parameters, sim_objects)
    # infectious confirmed goes to XC
    elif old_status[0] in 'ABMPI' and old_status[1] != 'C':
        new_status = old_status[0] + 'C'
    # recovered goes to RT
    elif old_status[0] == 'R' and old_status[1] != 'T':
        new_status = 'RT'
    else:
        # non-event (not infected, or in hospital, or dead)
        new_status = None
    # replace test result to conform to confirmed_event style
    event_info[1] = new_status

    return manage_confirmed_event(G, t, event_info, status, event_queue, parameters, sim_objects)

def manage_confirmed_event(G, t, event_info, status, event_queue, parameters, sim_objects):
    """Executes the transitions where a positive case is confirmed.
    Queues future contact tracing and induced testing events in contacts, and potentially triggers lockdowns.

    Args:
      G: graph
      t: current time
      event_info: auxiliary event information

        * node that is being confirmed
        * the new status of the node
        * node (if any) that caused the confirmation
        * group (if any) through which confirmation was triggered
      status: mapping fo nodes to their states
      event_queue: queue of events to occur in the future
      parameters: simulation parameters
      sim_objects: dynamic simulation caches

    Returns:
      list of transition events and list of nodes that need to have rates updated

    """
    events, influence_set = [], []

    node, new_status, *cause = event_info
    if new_status is None:
        return [], []
    old_status = status[node]

    # update confirmed node's status and record event
    status[node] = new_status
    if len(cause):
        anode, group = cause
        astatus = status[anode]
    else:
        anode, group, astatus = '', '', ''
    events.append({
        't': t,
        'enode': node,
        'efrom': old_status,
        'eto': new_status,
        'anode': anode,
        'group': group,
        'astatus': astatus,
    })
    influence_set.append(node)

    # check and activate lockdown
    if not all([x['activated'] for x in parameters['adaptive_lockdown']]):
        sim_objects['nstate'][0] += 1
        if status[node] not in ['RT', 'DX']:
            sim_objects['nstate'][1] += 1
        check_all_lockdown_triggers(parameters['adaptive_lockdown'], sim_objects['nstate'], event_queue, t)


    # Compute neighbours to trace, and trace delays
    nd_list, w_list, delay_list = [], [], []
    # pulling out close groups that have trace weights defined in parameters
    trace_policy = parameters['trace_policy'].get(sim_objects['lockdown_state'],
                                                    parameters['trace_policy']['default'])
    for group in G.neighbors(node):
        gx = group[:2]
        p_notrace = default_lookup(parameters['notrace'][gx.lower()], sim_objects['lockdown_state'])
        trace_drop = default_lookup(parameters['trace_drop'][gx.lower()], sim_objects['lockdown_state'])
        casual_test_prob = default_lookup(parameters['casual_test_prob'].get(gx.lower(), parameters['casual_test_prob']['default']), sim_objects['lockdown_state'])
        grp_type = 'close' if gx in _inverse_close_groups else 'casual'
        policy_key = gx if gx in trace_policy else grp_type
        if trace_policy[policy_key] == 'immediate':
            # special case: contacts at home immediately traced
            for nbr in G.neighbors(group):
                if nbr == node:
                    continue
                event_queue.add((t, Event.trace, nbr, 0, parameters['kappa_c'], trace_drop, node, group))
        elif trace_policy[policy_key] == 'trace':
            # reverse map to context of group
            priority = parameters['trace_weights'][_inverse_all_groups[gx]][grp_type]
            if priority == 0:
                continue
            
            for nbr in G.neighbors(group):
                if nbr != node and random.random() > p_notrace and status[nbr][1] not in 'QCT' and status[nbr][0] not in 'D0':
                    # temp list for storing tracer and retrace info
                    nd_list.append([nbr, 0, parameters['kappa_c'], trace_drop, node, group])
                    # priority list for tracing
                    w_list.append(priority)
                    # generate a random delay time
                    delay_list.append(random.weibullvariate(
                                        alpha=parameters['kappa_0']['scale'],
                                        beta=parameters['kappa_0']['shape']
                                        ))
        elif trace_policy[policy_key] == 'test':
            for nbr in G.neighbors(group):
                if random.random() > casual_test_prob:
                    continue
                # schedule a test
                test_delay = random.betavariate(**parameters['casual_test_delay']['core']) * parameters['casual_test_delay']['scale'] + parameters['casual_test_delay']['loc']
                event_queue.add((t+test_delay, Event.seek_test, nbr, node, group))
        elif trace_policy[policy_key] == 'skip':
            pass
        else:
            warn(f"Unknown trace policy {trace_policy[grp_type]}", RuntimeWarning)

    # Sort neighbours to trace by priority (exponential random vars)
    # Assign delays based on sort order
    # High priority/weight = earlier trace
    order = sorted(range(len(nd_list)), key=lambda i: random.expovariate(w_list[i]))
    delay_list.sort()
    for dt, i in zip(delay_list, order):
        event_queue.add((t+dt, Event.trace, *nd_list[i]))

    return events, influence_set

def manage_infection_event(G, t, event_info, status, event_queue, parameters, sim_objects):
    """Executes the transitions where an individual infects another individual.

    Args:
      G: graph
      t: current time
      event_info: auxiliary event information 

        * infector node
      status: mapping of nodes to their states
      event_queue: queue of events to occur in the future
      parameters: simulation parameters
      sim_objects: dynamic simulation caches

    Returns:
      list of transition events and list of nodes that need to have rates updated
    """
    events, influence_set = [], []

    node, = event_info
    infection, lockdown = status[node]

    # vaccination effects of infector node
    infector_attrs = G.nodes[node]

    inf_dose = compute_vaccination_level(t, infector_attrs, parameters, sim_objects)
    prev_inf = len(sim_objects['prev_infections'][node])
    if inf_dose > 0 or prev_inf > 0:
        # if the infector is vaccinated or previously infected, reject onward transmission with some probability
        vacc_ps = parameters['vaccination'][inf_dose, prev_inf > 0]
        waned_no_txn_p = 1 - ptable_lookup(vacc_ps['txn_weight'], infector_attrs) * compute_waning_factor(t, node, inf_dose, infector_attrs, parameters, sim_objects, aspect='onwards') 
        if random.random() < waned_no_txn_p:
            return [], []
    # Draw a group for event to occur in
    beta_0 = ptable_lookup(parameters['beta_0'], infector_attrs)
    beta_c = ptable_lookup(parameters['beta_c'], infector_attrs)
    beta_h = ptable_lookup(parameters['beta_h'], infector_attrs)
    lambda_h = beta_h * get_max_grp_sz(G, node, parameters, sim_objects)**parameters['density']
    roll = (random.random() * (beta_0 + beta_c + lambda_h))
    if roll < beta_0:
        group = rndx.draw_group(G, node, parameters['group_type_weights'],
                                group_mapping=_casual_groups)
    elif roll < (beta_0 + beta_c):
        group = rndx.draw_group(G, node, parameters['close_contact_weights'],
                                group_mapping=_close_groups)
    else:
        group = rndx.uniform_draw(G, node, acceptance_filter=lambda x: x[:2] in parameters['density_groups'])
        if random.random() > sim_objects['group_size'][group] / sim_objects['max_group_size'][node]:
            group = None

    # reject failed group draws
    if group is None:
        return [], []
    # reject groups that are too large
    default_reject = parameters['group_size_reject']['default']
    group_reject_size = parameters['group_size_reject'].get(sim_objects['lockdown_state'], default_reject)
    if group[:2] in _all_groups['COMM'] and len(G[group]) > group_reject_size:
        return [], []
    # reject groups that are closed
    if status[group] == '0X':
        return [], []

    # Draw transmission attempts in the group
    N = draw_num_infections(group, size=len(G[group]),
                            parameters=parameters['transmission_event_parameters'])
    draws = random.sample(list(G.neighbors(group)), N+1)
    if node in draws:
        draws.remove(node)
    draws = draws[:N]

    # Accept/reject for each attempt
    for neighbour in draws:
        rn_inf, rn_lck = status[neighbour]
        # Check infection conditions
        if rn_inf == 'S':
            txn_grp = group[:2].lower()
            at_miq = _miq_policy[parameters['omega']['policy']]['at_miq']
            at_home = _miq_policy[parameters['omega']['policy']]['at_home']
            omega_accept = min(parameters['omega'].get(rn_lck, 1), parameters['omega'].get(lockdown, 1))
            # If either in MIQ/C state, deny transmission
            if rn_lck in at_miq or lockdown in at_miq:
                if rn_lck in at_home or lockdown in at_home:
                    continue
                if random.random() > omega_accept:
                    continue
            # If both are in isolation, deny transmission outside household
            elif rn_lck in at_home and lockdown in at_home:
                if txn_grp.upper() not in _close_groups['HOME']:
                    continue
            # If only one in isolation, roll for cross-iso infection
            elif rn_lck in at_home or lockdown in at_home:
                if txn_grp.upper() not in _close_groups['HOME'] and random.random() > omega_accept:
                    continue
            # Determine age and lockdown weightings on transmission
            neighbour_attributes = G.nodes[neighbour]
            baw = ptable_lookup(parameters['beta_weights'], neighbour_attributes)
            lckw = parameters['lockdown_weights'][sim_objects['lockdown_state']][txn_grp] if sim_objects['lockdown_state'] else 1
            rcv_dose = compute_vaccination_level(t, neighbour_attributes, parameters, sim_objects)
            vacc_status = (rcv_dose, len(sim_objects['prev_infections'][neighbour]) > 0)
            vacc_weight = 1
            if any(vacc_status):
                # the waning factor gives the probability of acquiring the disease given some vaccination status
                rcv_wane_factor = compute_waning_factor(t, neighbour, rcv_dose, neighbour_attributes, parameters, sim_objects, aspect='acquisition')
                vacc_ps = parameters['vaccination'][vacc_status]
                # vacc_weight gives the protection from acquisition
                vacc_demographic_effect = ptable_lookup(vacc_ps['rcv_weight'], neighbour_attributes)
                vacc_weight = rcv_wane_factor * vacc_demographic_effect
            rcv_weight = baw * lckw * vacc_weight
            # Roll to accept/reject transmission based on weights
            if random.random() < rcv_weight:
                status[neighbour] = 'E' + rn_lck
                sim_objects['count'] += 1
                events.append({
                    't': t,
                    'enode': neighbour,
                    'group': group,
                    'efrom': rn_inf+rn_lck,
                    'eto': status[neighbour],
                    'anode': node,
                    'astatus': status[node],
                })
                influence_set.append(neighbour)

    return events, influence_set

_escape_strength = {'Q': 0, 'D': 1}
def manage_escape_event(G, t, event_info, status, event_queue, parameters, sim_objects):
    """Executes the transitions where an individual leaves self-isolation.

    Args:
      G: graph
      t: current time
      event_info: auxiliary event information 

        * node leaving self-isolation
        * 'strength' of the self-isolation
      status: mapping of nodes to their states
      event_queue: queue of events to occur in the future
      parameters: simulation parameters
      sim_objects: dynamic simulation caches

    Returns:
      list of transition events and list of nodes that need to have rates updated
    """
    events, influence_set = [], []

    node, iso_strength = event_info
    sufficient_strength = _escape_strength.get(status[node][1], -1) >= iso_strength
    if status[node][0] not in 'RHC' and sufficient_strength:
        old_status = status[node]
        status[node] = status[node][0] + 'U'
        influence_set.append(node)
        events.append({
            't': t,
            'enode': node,
            'efrom': old_status,
            'eto': status[node]
        })
    
    return events, influence_set

def manage_spontaneous_event(G, t, event_info, status, event_queue, parameters, sim_objects):
    """Executes the transitions where an individual changes state with no side effects.

    Args:
      G: graph
      t: current time
      event_info: auxiliary event information 

        * node changing state
        * state they are changing to
      status: mapping of nodes to their states
      event_queue: queue of events to occur in the future
      parameters: simulation parameters
      sim_objects: dynamic simulation caches

    Returns:
      list of transition events and list of nodes that need to have rates updated
    """
    events, influence_set = [], []

    node, new_status = event_info
    old_status = status[node]
    status[node] = new_status
    events.append({
        't': t,
        'enode': node,
        'efrom': old_status,
        'eto': new_status,
    })
    influence_set.append(node)

    return events, influence_set

def manage_removal_event(G, t, event_info, status, event_queue, parameters, sim_objects):
    """Executes the transitions where an individual is removed from the population 
    (e.g. death or recovery)

    Args:
      G: graph
      t: current time
      event_info: auxiliary event information 

        * node being removed (from the contagion dynamics)
        * state they are changing to
      status: mapping of nodes to their states
      event_queue: queue of events to occur in the future
      parameters: simulation parameters
      sim_objects: dynamic simulation caches

    Returns:
      list of transition events and list of nodes that need to have rates updated
    """

    node, new_status = event_info
    old_status = status[node]
    # was previously confirmed and is now removed or dead
    if (old_status[1] == 'C' or old_status in ['HQ', 'CQ']) and new_status[0] in 'RD':
        sim_objects['nstate'][1] -= 1
        check_all_lockdown_triggers(parameters['adaptive_lockdown'], sim_objects['nstate'], event_queue, t)
    # was previously never confirmed and is now dead
    elif new_status == 'DX':
        return manage_confirmed_event(G, t, event_info, status, event_queue, parameters, sim_objects)
    return manage_spontaneous_event(G, t, event_info, status, event_queue, parameters, sim_objects)

def manage_recovery_waning_event(G, t, event_info, status, event_queue, parameters, sim_objects):
    """Executes the transitions where a recovered individual becomes susceptible to infection again

    Args:
      G: graph
      t: current time
      event_info: auxiliary event information 

        * name of the node becoming susceptible
      status: mapping of nodes to their states
      event_queue: queue of events to occur in the future
      parameters: simulation parameters
      sim_objects: dynamic simulation caches

    Returns:
      list of transition events and list of nodes that need to have rates updated
    """
    node, _ = event_info
    sim_objects['prev_infections'][node].append(t)
    return manage_spontaneous_event(G, t, event_info, status, event_queue, parameters, sim_objects)

def manage_reseed_event(G, t, event_info, status, event_queue, parameters, sim_objects):
    """Executes the transitions where individuals are infected by external sources
    e.g. by an overseas returnee

    Args:
      G: graph
      t: current time
      event_info: auxiliary event information 

        * parameters to pass to cached reseeding function
      status: mapping of nodes to their states
      event_queue: queue of events to occur in the future
      parameters: simulation parameters
      sim_objects: dynamic simulation caches

    Returns:
      list of transition events and list of nodes that need to have rates updated

    Notes:
      reseeding is controlled by an auxiliary function that is created by 
      `cobin_build.build_reseed` in the call of `cobin_build.build_aux_sim_objs`
    """
    events, influence_set = [], []
    _, reseed_info, = event_info
    mutations = sim_objects['reseed'](G, status, reseed_info)
    events.extend(mutations)
    influence_set = [mutation['enode'] for mutation in mutations]

    if 'replay' in reseed_info:
        reseed_info['time'] += reseed_info['replay']
        event_queue.add((reseed_info['time'], Event.reseed, reseed_info['priority'], reseed_info))

    return events, influence_set

def manage_lockdown_event(G, t, event_info, status, event_queue, parameters, sim_objects):
    """Executes the transitions where a lockdown (NPI) is enacted

    Args:
      G: graph
      t: current time
      event_info: auxiliary event information 

        * name of the lockdown that has been triggered
      status: mapping of nodes to their states
      event_queue: queue of events to occur in the future
      parameters: simulation parameters
      sim_objects: dynamic simulation caches

    Returns:
      list of transition events and list of nodes that need to have rates updated
    """
    events, influence_set = [], []
    lockdown_name, = event_info
    lockdown_info = next(x for x in parameters['adaptive_lockdown'] if x['name'] == lockdown_name)
    if lockdown_info.get('recheck', False) and not check_lockdown_trigger(lockdown_info, sim_objects['nstate'], rechecking=True):
        # skip if trigger is no longer valid
        # allow it to check again
        lockdown_info['exhausted'] = False
        return [], []
    check_lockdown_triggers_reactivate(lockdown_name, parameters['adaptive_lockdown'])
    sim_objects['lockdown_state'] = lockdown_info['level']
    shutdown_info = parameters['lockdown_shutdown'].get(sim_objects['lockdown_state'], {})
    by_attr_map = parameters['lockdown_shutdown'].get('_by_map_', {})
    for grp, grpinfo in G.nodes(data=True):
        if grpinfo['bipartite'] == 0:
            status[grp] = is_group_lockdown(grp, grpinfo, shutdown_info, by_map=by_attr_map)
    influence_set = (nd for nd in G.nodes if status[nd][0] in 'ABMPI' and status[nd][1] != 'C')
    events.append({
        't': t,
        'efrom': '_LOCKDOWN',
        'eto': f'>>{lockdown_name}>>',
    })
    return events, influence_set

_manage_event_map = {
    Event.trace: manage_trace_event,
    Event.seek_test: manage_seek_test_event,
    Event.get_test_result: manage_get_test_result_event,
    Event.become_detected: manage_confirmed_event,
    Event.infect: manage_infection_event,
    Event.escape: manage_escape_event,
    Event.spontaneous: manage_spontaneous_event,
    Event.reseed: manage_reseed_event,
    Event.lockdown: manage_lockdown_event,
    Event.removal: manage_removal_event,
    Event.recovery_waning: manage_recovery_waning_event,
}

def manage_event(G, t, event_type, event_info, status, event_queue, parameters, sim_objects):
    """Makes state changes, queues delayed effects, and computes influence set for an event
    
    Args:
        G (nx.Graph): network with adjacency and attribute info
        t (float): current time
        event_type (Event.Enum) : type of event
        event_info (object) : object that contains (auxiliary) information about the event
        status (dict) : mapping of nodes to statuses
        event_queue (SortedList) : list of delayed events
        parameters (dict): simulation parameters
        sim_objects (dict): dynamic simulation cache

    Returns:
      events (list) : list of dictionaries that contain info about all instantaneous state changes
      influence_set (list) : list of nodes that need to have their rates recomputed

    """
    if event_type not in _manage_event_map:
        warn(f"Unknown event type: {event_type}. Available event types are: {list(Event)}.")
        return [], []
    event_list, influence_set = _manage_event_map[event_type](G, t, event_info, 
                                                              status, event_queue, 
                                                              parameters, sim_objects)
    # record all state change times for semi-Markovian dynamics
    for event in event_list:
        if 'enode' in event:
            sim_objects['entry_time'][event['enode']] = t
    return event_list, influence_set

def initialise_parameters(parameters, sim_objects):
    """Create sim object type parameters, and do any further parameter manipulation

    Args:
      parameters: simulation parameters (static)
      sim_objects: dynamic simulation caches
    """
    # store initial exhaust state for repeats
    for trigger in parameters['adaptive_lockdown']:
        if '_exhausted' not in trigger:
            trigger['_exhausted'] = trigger['exhausted']
        trigger['exhausted'] = trigger['_exhausted']
    # parameter insertion
    sim_objects['count'] = sim_objects.get('count', 0)
    sim_objects['destiny'] = sim_objects.get('destiny', dict())
    sim_objects['nstate'] = sim_objects.get('nstate', [0, 0])
    sim_objects['lockdown_state'] = sim_objects.get('lockdown_state', parameters['lockdown_state'])
    # modification of the adaptive_lockdown to flag it per run
    for cond in parameters['adaptive_lockdown']:
        cond['activated'] = False
    # convert the restriction to a set for easy check of membership
    for prio, event in enumerate(parameters['reseed']):
        if 'only_if' in event:
            event['only_if'] = set(event['only_if'])
        event['priority'] = prio
    # construct hazard for gamma
    gscale, gshape = parameters['gamma']['scale'], parameters['gamma']['shape']
    gconst = 1./(math.gamma(gshape)*gscale**gshape)
    def gamma_haz(x):
        return gconst * x**(gshape-1) * math.exp(-x/gscale) / upper_incomplete_gamma(gshape, x/gscale)
    sim_objects['gamma_hazard'] = gamma_haz
    # default entry time is zero
    sim_objects['entry_time'] = sim_objects.get('entry_time', defaultdict(int))
    # default infections is zero
    sim_objects['prev_infections'] = sim_objects.get('prev_infections', defaultdict(list))
    sim_objects['max_group_size'] = dict()
    sim_objects['group_size'] = dict()

def initial_delays(time, event_queue, parameters, sim_objects):
    """Insert known delay events into the event queue

    Args:
      event_queue: queue of events to fire in future
      parameters: simulation parameters
      sim_objects: dynamic simuation caches
    """
    check_all_lockdown_triggers(parameters['adaptive_lockdown'], sim_objects['nstate'],
                                event_queue, time)
    for reseed in parameters['reseed']:
        event_queue.add((reseed['time'], Event.reseed, reseed['priority'], reseed))

def cleanup(G):
    """Undoes changes to the graph that were done during simulation time

    Args:
      G: graph

    Notes:
      Currently does nothing, as we assume that close contact structures are created offline
    """
    pass

def count_infections(records, parameters, sim_objects):
    """Counts the number of infections for stopping

    Args:
      records: history of events
      parameters: simulation parameters
      sim_objects: dynamic simulation caches

    Returns:
        count of number of infections that have occured so far in the simulation 
        for early termination
    """
    return sim_objects['count']

def select_dumpable_sim_objects(sim_objects):
    """Returns the cached dynamic simulation objects that can be pickled and stored"""
    return [
        "destiny",
        "nstate",
        "lockdown_state",
        "count",
        "entry_time",
        "prev_infections",
    ]

################################################################################

sim_functions = {
    'rate_function': rate_function,
    'transition_choice': transition_choice,
    'manage_event': manage_event,
    'initial_delays': initial_delays,
    'cleanup': cleanup,
    'count_infections': count_infections,
}
